/*
 * valet_uniqpid_parse
 *
 * Extract the fields from a unique process identifier.
 *
 * The uniqpid is created using the following process metadata:
 *
 *   - 32-bit hash of the command name
 *   - uid, gid of the process
 *   - pid of the process
 *   - start time (in jiffies since boot)
 *
 * All are concatenated as hexadecimal without leading zeroes as:
 *
 *  [hash]-[pid]-[uid]-[gid]-[start time]
 *
 * $Id: valet_uniqpid_parse.c 531 2014-10-31 16:18:05Z frey $
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>

//

static struct option cliOptions[] = {
                          { "help",       no_argument,      NULL,   'h' },
                          { "cmd-hash",   no_argument,      NULL,   'c' },
                          { "pid",        no_argument,      NULL,   'p' },
                          { "uid",        no_argument,      NULL,   'u' },
                          { "gid",        no_argument,      NULL,   'g' },
                          { "start-time", no_argument,      NULL,   't' },
                          { "hex",        no_argument,      NULL,   'x' },
                          { "prefix",     required_argument,NULL,   'P' },
                          { NULL,         0,                NULL,   0 }
                        };

//

void
usage(
  const char      *exeName
)
{
  printf(
      "usage:\n\n"
      "  %s {options} <uniqpid>\n\n"
      " options:\n\n"
      "  -h/--help                display this information\n"
      "  -c/--cmd-hash            display the command hash field\n"
      "  -p/--pid                 display the pid field\n"
      "  -u/--uid                 display the uid field\n"
      "  -g/--gid                 display the gid field\n"
      "  -t/--start-time          display the start time field\n"
      "  -x/--hex                 display fields in hexadecimal format\n"
      "  -P/--prefix              prefix text to ignore\n"
      "\n"
      "$Id: valet_uniqpid_parse.c 531 2014-10-31 16:18:05Z frey $\n",
      exeName
    );
  exit(0);
}

//

enum {
  kValetUniqPidFieldCommandHash = 1 << 0,
  kValetUniqPidFieldPid         = 1 << 1,
  kValetUniqPidFieldUid         = 1 << 2,
  kValetUniqPidFieldGid         = 1 << 3,
  kValetUniqPidFieldStartTime   = 1 << 4,
  //
  kValetUniqPidFieldMin         = kValetUniqPidFieldCommandHash,
  kValetUniqPidFieldMax         = kValetUniqPidFieldStartTime,
  //
  kValetUniqPidFieldIsDefault   = 1 << 16,
  //
  kValetUniqPidFieldDefault     = kValetUniqPidFieldIsDefault | kValetUniqPidFieldPid,
  //
  kValetUniqPidFieldRootProcess = kValetUniqPidFieldCommandHash | kValetUniqPidFieldPid | kValetUniqPidFieldStartTime
};

//

const char* fieldNames[] = {
                "hash",
                "pid",
                "uid",
                "gid",
                "starttime"
              };

//

int
main(
  int           argc,
  char* const * argv
)
{
  const char    *exeName = argv[0];
  int           optCh, fieldCount;
  unsigned      tmp_u_i, fields = kValetUniqPidFieldDefault;
  bool          shouldDisplayHex = false;
  const char    *prefixString = NULL;
  
  while ( (optCh = getopt_long(argc, argv, "hcpugtxP:", cliOptions, NULL)) != -1 ) {
    switch ( optCh ) {
      case 'h':
        usage(exeName);
        break;
      case 'c':
        if ( (fields & kValetUniqPidFieldIsDefault) != 0 ) fields = 0;
        fields |= kValetUniqPidFieldCommandHash;
        break;
      case 'p':
        if ( (fields & kValetUniqPidFieldIsDefault) != 0 ) fields = 0;
        fields |= kValetUniqPidFieldPid;
        break;
      case 'u':
        if ( (fields & kValetUniqPidFieldIsDefault) != 0 ) fields = 0;
        fields |= kValetUniqPidFieldUid;
        break;
      case 'g':
        if ( (fields & kValetUniqPidFieldIsDefault) != 0 ) fields = 0;
        fields |= kValetUniqPidFieldGid;
        break;
      case 't':
        if ( (fields & kValetUniqPidFieldIsDefault) != 0 ) fields = 0;
        fields |= kValetUniqPidFieldStartTime;
        break;
      case 'x':
        shouldDisplayHex = true;
        break;
      case 'P':
        if ( optarg && *optarg ) prefixString = optarg;
        break;
    }
  }
  argc -= optind;
  argv += optind;
  
  if ( argc > 0 ) {
    unsigned long long      fieldValues[5];
    const char              *uniqpidString = argv[0];
    
    // Prefix?
    if ( prefixString ) {
      size_t                prefixStringLen = strlen(prefixString);
      
      if ( strlen(uniqpidString) <= prefixStringLen ) return 0;
      if ( strncmp(uniqpidString, prefixString, prefixStringLen) == 0 ) {
        uniqpidString += prefixStringLen;
      }
    } 
    
    // Count the number of items being printed:
    fieldCount = 0;
    tmp_u_i = kValetUniqPidFieldMin;
    while ( tmp_u_i <= kValetUniqPidFieldMax ) {
      if ( (fields & tmp_u_i) != 0 ) fieldCount++;
      tmp_u_i <<= 1;
    }
    
    if ( sscanf(uniqpidString, "%llx-%llx-%llx-%llx-%llx", &fieldValues[0], &fieldValues[1], &fieldValues[2], &fieldValues[3], &fieldValues[4]) != 5 ) {
      if (  sscanf(uniqpidString, "%llx-%llx-%llx", &fieldValues[0], &fieldValues[1], &fieldValues[4]) != 3 ) {
        fprintf(stderr, "ERROR:  invalid uniqpid:  %s\n", uniqpidString);
        return EINVAL;
      } else {
        fields &= kValetUniqPidFieldRootProcess;
      }
    }
    if ( fields ) {
      int                   fieldIndex = 0;
      const char*           fieldFormat;
      
      if ( shouldDisplayHex ) {
        fieldFormat = ( fieldCount > 1 ) ? "%1$s: %2$llx\n" : "%2$llx\n";
      } else {
        fieldFormat = ( fieldCount > 1 ) ? "%1$s: %2$lld\n" : "%2$lld\n";
      }
      
      tmp_u_i = kValetUniqPidFieldMin;
      while ( tmp_u_i <= kValetUniqPidFieldMax ) {
        if ( (fields & tmp_u_i) != 0 ) printf(fieldFormat, fieldNames[fieldIndex], fieldValues[fieldIndex]);
        fieldIndex++;
        tmp_u_i <<= 1;
      }
    }
  } else {
    usage(exeName);
    return EINVAL;
  }
  return 0;
}
