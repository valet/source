/*
 * VALET - VALET Automates Linux Environment Tasks
 * vpkg_count
 *
 * Post a simple form containing a uid, package id,
 * and version id to the cluster-local VALET package-
 * counting system.  Increments that user's per-diem
 * use count for the given versioned package.
 *
 * Normally I'd use e.g. libcurl to make a HTTP request,
 * but we want to keep this program as simple, quick,
 * failsafe, and unobtrusive as possible.  So the HTTP
 * request is implemented explicitly.
 *
 * $Id: vpkg_count.c 531 2014-10-31 16:18:05Z frey $ 
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <openssl/sha.h>
#include <openssl/md5.h>

//

static char       *VPkgServerName = "farber.hpc.udel.edu";
static char       *VPkgServerIP = "10.0.0.1";

#ifndef VPKGCOUNT_SEND_TIMEOUT
#define VPKGCOUNT_SEND_TIMEOUT    10
#endif

#ifndef VPKGCOUNT_RECV_TIMEOUT
#define VPKGCOUNT_RECV_TIMEOUT    20
#endif

#ifndef VPKGCOUNT_CONNECT_TIMEOUT
#define VPKGCOUNT_CONNECT_TIMEOUT    2
#endif

//

typedef enum {
  kVPkgDigestAlgorithmMD5       = 0,
  kVPkgDigestAlgorithmSHA1,
  kVPkgDigestAlgorithmSHA256,
  //
  kVPkgDigestAlgorithmDefault	= kVPkgDigestAlgorithmSHA1
} VPkgDigestAlgorithm;

#define VPKGCOUNT_DIGEST_MD5_LENGTH MD5_DIGEST_LENGTH
#define VPKGCOUNT_DIGEST_MD5(D,U,P,V) {\
                  MD5_CTX md5Context; \
                  MD5_Init(&md5Context); \
                  MD5_Update(&md5Context, (U), strlen((U))); \
                  MD5_Update(&md5Context, (P), strlen((P))); \
                  MD5_Update(&md5Context, (V), strlen((V))); \
                  MD5_Update(&md5Context, secret, secretLen); \
                  MD5_Final((D), &md5Context); \
                }

#define VPKGCOUNT_DIGEST_SHA1_LENGTH SHA_DIGEST_LENGTH
#define VPKGCOUNT_DIGEST_SHA1(D,U,P,V) {\
                  SHA_CTX shaContext; \
                  SHA1_Init(&shaContext); \
                  SHA1_Update(&shaContext, (U), strlen((U))); \
                  SHA1_Update(&shaContext, (P), strlen((P))); \
                  SHA1_Update(&shaContext, (V), strlen((V))); \
                  SHA1_Update(&shaContext, secret, secretLen); \
                  SHA1_Final((D), &shaContext); \
                }

#define VPKGCOUNT_DIGEST_SHA256_LENGTH SHA256_DIGEST_LENGTH
#define VPKGCOUNT_DIGEST_SHA256(D,U,P,V) {\
                  SHA256_CTX shaContext; \
                  SHA256_Init(&shaContext); \
                  SHA256_Update(&shaContext, (U), strlen((U))); \
                  SHA256_Update(&shaContext, (P), strlen((P))); \
                  SHA256_Update(&shaContext, (V), strlen((V))); \
                  SHA256_Update(&shaContext, secret, secretLen); \
                  SHA256_Final((D), &shaContext); \
                }

//

void
byteToHexStr(
  unsigned char   byte,
  char            *str
)
{
  static char     *hexTable = "0123456789abcdef";
  
  *str++ = hexTable[ (byte & 0xF0) >> 4 ];
  *str = hexTable[ (byte & 0x0F) ];
}

//

char*
VPkgCountCreateMessage(
  VPkgDigestAlgorithm   algorithm,
  uid_t                 uidNumber,
  const char*           pkgId,
  const char*           versionId
)
{
  char            secret[256];
  size_t          secretLen = 0;
  size_t          digestLength;
  char            uidStr[32];
  char*           msg = NULL;
  int             rc;
  
  secret[secretLen++] = 'Y';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 'u';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'h';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'v';
  secret[secretLen++] = 'e';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'v';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'y';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'g';
  secret[secretLen++] = 'e';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'n';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 's';
  secret[secretLen++] = 't';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'i';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 's';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'f';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 'r';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 's';
  secret[secretLen++] = 'u';
  secret[secretLen++] = 'c';
  secret[secretLen++] = 'h';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'a';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 's';
  secret[secretLen++] = 'm';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'l';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'g';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'n';
  secret[secretLen++] = 't';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'm';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'n';
  secret[secretLen++] = '.';
  
  switch ( algorithm ) {
    case kVPkgDigestAlgorithmMD5:
      digestLength = VPKGCOUNT_DIGEST_MD5_LENGTH;
      break;
    case kVPkgDigestAlgorithmSHA1:
      digestLength = VPKGCOUNT_DIGEST_SHA1_LENGTH;
      break;
    case kVPkgDigestAlgorithmSHA256:
      digestLength = VPKGCOUNT_DIGEST_SHA256_LENGTH;
      break;
    default:
      return NULL;
  }
  
  // Stringify the uid number:
  snprintf(uidStr, sizeof(uidStr), "%ld", (long int)uidNumber);
  
  // Produce the digest string:
  unsigned char   *digestStr = alloca(digestLength * 2 + 1);
  
  switch ( algorithm ) {
    case kVPkgDigestAlgorithmMD5: {
      VPKGCOUNT_DIGEST_MD5(digestStr, uidStr, pkgId, versionId);
      break;
    }
    case kVPkgDigestAlgorithmSHA1: {
      VPKGCOUNT_DIGEST_SHA1(digestStr, uidStr, pkgId, versionId);
      break;
    }
    case kVPkgDigestAlgorithmSHA256: {
      VPKGCOUNT_DIGEST_SHA256(digestStr, uidStr, pkgId, versionId);
      break;
    }
    default:
      break;
  }

  // Convert the digest bytes to a string:
  int             src = digestLength;
  int             dst = 2 * src;
  
  digestStr[dst] = '\0';
  dst -= 2;
  while ( src-- ) {
    byteToHexStr(digestStr[src], &digestStr[dst]);
    dst -= 2;
  }
  
  // Create the message body:
  rc = asprintf(
          &msg,
          "u=%d&"
          "p=%s&"
          "v=%s&"
          "d=%s",
          (int)uidNumber,
          pkgId,
          versionId,
          digestStr
        );
  return msg;
}

//

int
VPkgSendMessage(
  const char    *message,
  size_t        messageLen
)
{
  int           s;
  int           rc;
  char          *header = NULL;
  size_t        headerLen = 0;
      
  headerLen = asprintf(
            &header,
            "POST /valet-counts/count.php HTTP/1.1\r\n"
            "Host: %s\r\n"
            "Content-type: application/x-www-form-urlencoded\r\n"
            "Content-length: %ld\r\n"
            "\r\n",
            VPkgServerName,
            messageLen
          );
  if ( headerLen <= 0 ) {
    return ENOMEM;
  }
  
  s = socket(AF_INET, SOCK_STREAM, 0);
  if ( s >= 0 ) {
    fd_set                fds;
    struct timeval        timeout;
    struct sockaddr_in    destAddr;
    
    timeout.tv_sec = VPKGCOUNT_SEND_TIMEOUT;
    timeout.tv_usec = 0;
    setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));
    
    timeout.tv_sec = VPKGCOUNT_RECV_TIMEOUT;
    timeout.tv_usec = 0;
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));
    
    destAddr.sin_family = AF_INET;
    destAddr.sin_port = htons(80);
    destAddr.sin_addr.s_addr = inet_addr(VPkgServerIP);
    memset(destAddr.sin_zero, 0, sizeof(destAddr.sin_zero));
    
    fcntl(s, F_SETFL, O_NONBLOCK);
    rc = connect(s, (struct sockaddr*)&destAddr, sizeof(destAddr));
    if ( (rc != -1) || (errno != EINPROGRESS) ) {
      close(s);
      return errno;
    }
    
    // Wait for it to finish connecting:
    FD_ZERO(&fds);
    FD_SET(s, &fds);
    timeout.tv_sec = VPKGCOUNT_CONNECT_TIMEOUT;
    timeout.tv_usec = 0;
    if ( select(s + 1, NULL, &fds, NULL, &timeout) == 1 ) {
      rc = write(s, header, headerLen);
      rc = write(s, message, messageLen);
      
      //
      // Wait 10 seconds for output before we close the socket:
      //
      FD_ZERO(&fds);
      FD_SET(s, &fds);
      timeout.tv_sec = 10;
      timeout.tv_usec = 0;
      select(s + 1, &fds, NULL, NULL, &timeout);
      rc = 0;
    } else {
      rc = errno;
    }
    close(s);
  } else {
    rc = errno;
  }
  free(header);
  return rc;
}

//

#include <getopt.h>

const struct option VPkgCliOptions[] = {
          { "use-md5",          no_argument,        NULL,         '1' },
          { "use-sha1",         no_argument,        NULL,         '2' },
          { "use-sha256",       no_argument,        NULL,         '3' },
          { "no-fork",          no_argument,        NULL,         'f' },
          { "help",             no_argument,        NULL,         'h' },
          { NULL,               0,                  NULL,         0   }
        };

//

void
VPkgCountUsage(
  const char*   exeName
)
{
  printf(
      "usage:\n\n"
      "  %s {options} <pkg-id> <version-id>\n\n"
      " options:\n\n"
      "  -1/--use-md5             Use MD5 digests\n"
      "  -2/--use-sha1            Use SHA digests\n"
      "  -3/--use-sha256          Use SHA-256 digests\n"
      "  -f/--no-fork             Do not fork!\n"
      "\n"
      "$Id: vpkg_count.c 531 2014-10-31 16:18:05Z frey $\n",
      exeName
    );
  exit(0);
}

//

int
main(
  int           argc,
  char* const   *argv
)
{
  VPkgDigestAlgorithm   algorithm = kVPkgDigestAlgorithmDefault;
  int                   ch;
  bool                  noFork = false;
  const char            *exeName = argv[0];
  
  while ( (ch = getopt_long(argc, argv, "123hf", VPkgCliOptions, NULL)) != -1 ) {
    switch ( ch ) {
      case '1':
        algorithm = kVPkgDigestAlgorithmMD5;
        break;
      case '2':
        algorithm = kVPkgDigestAlgorithmSHA1;
        break;
      case '3':
        algorithm = kVPkgDigestAlgorithmSHA256;
        break;
      case 'h':
        VPkgCountUsage(exeName);
        break;
      case 'f':
        noFork = true;
        break;
      default:
        break;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if ( argc >= 2 ) {
    char                *message = VPkgCountCreateMessage(algorithm, getuid(), argv[0], argv[1]);
    
    if ( message ) {
      if ( ! noFork ) {
        if ( daemon(1, 0) == -1 ) {
          exit(ENOMEM);
        }
      }
      ch = VPkgSendMessage(message, strlen(message));
      free(message);
      if ( ch != 0 ) return ch;
    }
    return 0;
  }
  return EINVAL;
}
