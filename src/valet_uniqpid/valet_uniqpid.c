/*
 * valet_uniqpid
 *
 * Generate the unique process identifier for a running process (by pid).
 * The uniqpid is created using the following process metadata:
 *
 *   - 32-bit hash of the command name
 *   - uid, gid of the process
 *   - pid of the process
 *   - start time (in jiffies since boot)
 *
 * All are concatenated as hexadecimal without leading zeroes as:
 *
 *  [hash]-[pid]-[uid]-[gid]-[start time]
 *
 * For example:
 *
 *    command name:     hash("bash") => 0x4a3f274a
 *    pid:              43443
 *    uid:              1001
 *    gid:              900
 *    start time:       198112790
 *
 *    uniqpid:          4a3f274a-a9b3-3e9-384-bcef616
 *
 * One special case is processes owned by root:root (0:0).  Such
 * processes will have the [uid]-[gid] omitted, e.g.
 *
 *    uniqpid:          4ae83f3e-3476-bd9f759
 *
 * $Id: valet_uniqpid.c 531 2014-10-31 16:18:05Z frey $
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

//

/*
 * http://en.wikipedia.org/wiki/Dr._Dobbs_Journal
 *
 * Inlined below in ESnapshotIdGenerate.
 *
uint32_t
jenkins_one_at_a_time_hash(
  char      *key,
  size_t    len
)
{
    uint32_t hash, i;
    for(hash = i = 0; i < len; ++i)
    {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}
 */

//

int
ESnapshotSessionIdGenerate(
  long int    shellPid,
  bool        usePPId,
  long int    ppidIndex
)
{
  int         rc = 0;
  char        *statFilePath = NULL;
  
  if ( usePPId && (ppidIndex > 0) && (shellPid == 1) ) return EINVAL;
  
  asprintf(&statFilePath, "/proc/%ld/stat", (long int)shellPid);
  
  if ( statFilePath ) {
    struct stat     statFileMetaData;
    
    if ( stat(statFilePath, &statFileMetaData) == 0 ) {
      FILE      *statFilePtr = fopen(statFilePath, "r");
      
      if ( statFilePtr ) {
        uint32_t          commHash = 0;
        bool              inParen, done;
        
        /* Skip the pid: */
        fscanf(statFilePtr, "%*d");
        
        /* Skip the executable name: */
        inParen = done = false;
        while ( ! done ) {
          int   c = fgetc(statFilePtr);
          
          switch ( c ) {
            case '(':
              inParen = true;
              break;
            case ')':
              if ( inParen ) {
                done = true;
              }
              break;
            case EOF:
              done = true;
              break;
            default:
              if ( inParen ) {
                commHash += c;
                commHash += (commHash << 10);
                commHash ^= (commHash >> 6);
              }
              break;
          }
        }
        if ( done && inParen ) {
          int                 count;
          int                 shellPPId;
          unsigned long long  shellStartTime;
          
          commHash += (commHash << 3);
          commHash ^= (commHash >> 11);
          commHash += (commHash << 15);
          count = fscanf(
                      statFilePtr,
                      " %*c %d %*d %*d %*d %*d %*u %*lu %*lu %*lu %*lu %*lu %*lu %*ld %*ld %*ld %*ld %*ld %*ld %llu",
                      &shellPPId,
                      &shellStartTime
                    );
          if ( count == 2 ) {
            if ( usePPId && (ppidIndex > 0)) {
              fclose(statFilePtr);
              return ESnapshotSessionIdGenerate(shellPPId, true, ppidIndex - 1);
            }
            if ( (statFileMetaData.st_uid > 0) || (statFileMetaData.st_gid > 0) ) {
              printf("%x-%x-%x-%x-%llx\n",
                  commHash,
                  shellPid,
                  statFileMetaData.st_uid,
                  statFileMetaData.st_gid,
                  shellStartTime
                );
            } else {
              printf("%x-%x-%llx\n",
                  commHash,
                  shellPid,
                  shellStartTime
                );
            }
          } else {
            rc = EINVAL;
          }
        } else {
          rc = EINVAL;
        }
              
        fclose(statFilePtr);
      } else {
        rc = errno;
      }
    } else {
      rc = errno;
    }
    free(statFilePath);
  } else {
    rc = ENOMEM;
  }
  return rc;
}

//

int
main(
  int         argc,
  const char* argv[]
)
{
  int         argn = 1;
  bool        usePPId = false;
  long int    ppidIndex = 1;
  
  if ( argc < 2 ) {
    printf(
        "usage:\n\n"
        "  %s {-p{=#}} <pid>\n\n"
        " options:\n\n"
        "  -p           use parent process of <pid>\n"
        "  -p=#         use the n-th parent process of <pid>\n"
        "               e.g. -p=1 and -p produce the same result\n"
        "\n"
        "$Id: valet_uniqpid.c 531 2014-10-31 16:18:05Z frey $\n",
        argv[0]
      );
    return EINVAL;
  }
  
  if ( strncmp(argv[1], "-p", 2) == 0 ) {
    usePPId = true;
    if ( *(argv[1] + 2) == '=' ) {
      char        *endPtr = NULL;
      long int    tmpInt = strtol(argv[1] + 3, &endPtr, 10);
      
      if ( endPtr > argv[1] + 3 ) {
        if ( tmpInt > 0 ) {
          ppidIndex = tmpInt;
        } else {
          ppidIndex = 0;
          usePPId = false;
        }
      } else {
        printf("ERROR:  invalid parent index: %s\n", argv[1] + 3);
        return EINVAL;
      }
    }
    argn++;
  }
  
  long int    scannedPid = -1;
  char        *endPtr = NULL;
  
  if ( (scannedPid = strtol(argv[argn], &endPtr, 10)) && (endPtr > argv[1]) ) {
    return ESnapshotSessionIdGenerate(scannedPid, usePPId, ppidIndex);
  }
  printf("ERROR:  invalid pid:  %s\n", argv[1]);
  return EINVAL;
}
