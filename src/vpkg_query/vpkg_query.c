/*
 * VALET - VALET Automates Linux Environment Tasks
 * vpkg_query
 *
 * Query the cluster-local VALET package-counting system.  Queries
 * can be made in per-day or aggregated fashion.  By default, only
 * records for the effective UID are requested; when run as root
 * records for all users or an arbitrary UID are available.
 *
 * $Id: vpkg_query.c 531 2014-10-31 16:18:05Z frey $ 
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <curl/curl.h>

#include <openssl/sha.h>
#include <openssl/md5.h>

//

static char       *VPkgQueryURL = "http://10.0.0.1/valet-counts/query.php";

#ifndef VPkgQuery_CONNECT_TIMEOUT
#define VPkgQuery_CONNECT_TIMEOUT    30
#endif

#ifndef VPkgQuery_REQUEST_TIMEOUT
#define VPkgQuery_REQUEST_TIMEOUT    120
#endif

#ifndef VPkgQuery_RECV_RETRYCOUNT
#define VPkgQuery_RECV_RETRYCOUNT    4
#endif

#ifndef VPkgQuery_RECV_BUFFERSIZE
#define VPkgQuery_RECV_BUFFERSIZE    1024
#endif

//

typedef enum {
  kVPkgDigestAlgorithmMD5       = 0,
  kVPkgDigestAlgorithmSHA1,
  kVPkgDigestAlgorithmSHA256,
  //
  kVPkgDigestAlgorithmDefault	= kVPkgDigestAlgorithmSHA1
} VPkgDigestAlgorithm;

#define VPkgQuery_DIGEST_MD5_LENGTH MD5_DIGEST_LENGTH
#define VPkgQuery_DIGEST_MD5(D,U,P,V,S,E,AG,AGBC,AU,Q) {\
                  MD5_CTX md5Context; \
                  MD5_Init(&md5Context); \
                  if ( (U) ) { MD5_Update(&md5Context, (U), strlen((U))); } \
                  if ( (P) ) { MD5_Update(&md5Context, (P), strlen((P))); } \
                  if ( (V) ) { MD5_Update(&md5Context, (V), strlen((V))); } \
                  if ( (S) ) { MD5_Update(&md5Context, (S), strlen((S))); } \
                  if ( (E) ) { MD5_Update(&md5Context, (E), strlen((E))); } \
                  if ( (AG) ) { MD5_Update(&md5Context, "aggregate=1", 11); } \
                  if ( (AG && AGBC) ) { MD5_Update(&md5Context, "aggregateByCount=1", 18); } \
                  if ( (AU) ) { MD5_Update(&md5Context, "everyone=1", 10); } \
                  if ( (Q) ) { MD5_Update(&md5Context, "quiet=1", 7); } \
                  MD5_Update(&md5Context, secret, secretLen); \
                  MD5_Final((D), &md5Context); \
                }

#define VPkgQuery_DIGEST_SHA1_LENGTH SHA_DIGEST_LENGTH
#define VPkgQuery_DIGEST_SHA1(D,U,P,V,S,E,AG,AGBC,AU,Q) {\
                  SHA_CTX shaContext; \
                  SHA1_Init(&shaContext); \
                  if ( (U) ) { SHA1_Update(&shaContext, (U), strlen((U))); } \
                  if ( (P) ) { SHA1_Update(&shaContext, (P), strlen((P))); } \
                  if ( (V) ) { SHA1_Update(&shaContext, (V), strlen((V))); } \
                  if ( (S) ) { SHA1_Update(&shaContext, (S), strlen((S))); } \
                  if ( (E) ) { SHA1_Update(&shaContext, (E), strlen((E))); } \
                  if ( (AG) ) { SHA1_Update(&shaContext, "aggregate=1", 11); } \
                  if ( (AG && AGBC) ) { SHA1_Update(&shaContext, "aggregateByCount=1", 18); } \
                  if ( (AU) ) { SHA1_Update(&shaContext, "everyone=1", 10); } \
                  if ( (Q) ) { SHA1_Update(&shaContext, "quiet=1", 7); } \
                  SHA1_Update(&shaContext, secret, secretLen); \
                  SHA1_Final((D), &shaContext); \
                }

#define VPkgQuery_DIGEST_SHA256_LENGTH SHA256_DIGEST_LENGTH
#define VPkgQuery_DIGEST_SHA256(D,U,P,V,S,E,AG,AGBC,AU,Q) {\
                  SHA256_CTX shaContext; \
                  SHA256_Init(&shaContext); \
                  if ( (U) ) { SHA256_Update(&shaContext, (U), strlen((U))); } \
                  if ( (P) ) { SHA256_Update(&shaContext, (P), strlen((P))); } \
                  if ( (V) ) { SHA256_Update(&shaContext, (V), strlen((V))); } \
                  if ( (S) ) { SHA256_Update(&shaContext, (S), strlen((S))); } \
                  if ( (E) ) { SHA256_Update(&shaContext, (E), strlen((E))); } \
                  if ( (AG) ) { SHA256_Update(&shaContext, "aggregate=1", 11); } \
                  if ( (AG && AGBC) ) { SHA256_Update(&shaContext, "aggregateByCount=1", 18); } \
                  if ( (AU) ) { SHA256_Update(&shaContext, "everyone=1", 10); } \
                  if ( (Q) ) { SHA256_Update(&shaContext, "quiet=1", 7); } \
                  SHA256_Update(&shaContext, secret, secretLen); \
                  SHA256_Final((D), &shaContext); \
                }

//

void
byteToHexStr(
  unsigned char   byte,
  char            *str
)
{
  static char     *hexTable = "0123456789abcdef";
  
  *str++ = hexTable[ (byte & 0xF0) >> 4 ];
  *str = hexTable[ (byte & 0x0F) ];
}

//

char*
VPkgQueryCreateDigest(
  VPkgDigestAlgorithm   algorithm,
  uid_t                 uidNumber,
  const char*           pkgId,
  const char*           versionId,
  const char*           startDate,
  const char*           endDate,
  bool                  aggregate,
  bool                  aggregateByCount,
  bool                  everyone,
  bool                  quiet
)
{
  char            secret[256];
  size_t          secretLen = 0;
  size_t          digestLength;
  char            uidStr[32], *uidStrPtr = NULL;
  char*           msg = NULL;
  int             rc;
  
  secret[secretLen++] = 'Y';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 'u';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'h';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'v';
  secret[secretLen++] = 'e';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'v';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'y';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'g';
  secret[secretLen++] = 'e';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'n';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 's';
  secret[secretLen++] = 't';
  secret[secretLen++] = 'r';
  secret[secretLen++] = 'i';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 's';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'f';
  secret[secretLen++] = 'o';
  secret[secretLen++] = 'r';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 's';
  secret[secretLen++] = 'u';
  secret[secretLen++] = 'c';
  secret[secretLen++] = 'h';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'a';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 's';
  secret[secretLen++] = 'm';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'l';
  secret[secretLen++] = ' ';
  secret[secretLen++] = 'g';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'n';
  secret[secretLen++] = 't';
  secret[secretLen++] = 'l';
  secret[secretLen++] = 'e';
  secret[secretLen++] = 'm';
  secret[secretLen++] = 'a';
  secret[secretLen++] = 'n';
  secret[secretLen++] = '.';
  
  switch ( algorithm ) {
    case kVPkgDigestAlgorithmMD5:
      digestLength = VPkgQuery_DIGEST_MD5_LENGTH;
      break;
    case kVPkgDigestAlgorithmSHA1:
      digestLength = VPkgQuery_DIGEST_SHA1_LENGTH;
      break;
    case kVPkgDigestAlgorithmSHA256:
      digestLength = VPkgQuery_DIGEST_SHA256_LENGTH;
      break;
    default:
      return NULL;
  }
  
  // Stringify the uid number:
  if ( uidNumber > 0 ) {
    if ( uidNumber == (uid_t)-1 ) {
      strcpy(uidStr, "*");
    } else {
      snprintf(uidStr, sizeof(uidStr), "%u", (unsigned int)uidNumber);
    }
    uidStrPtr = uidStr;
  }
  
  // Produce the digest string:
  unsigned char   *digestStr = malloc(digestLength * 2 + 1);
  
  switch ( algorithm ) {
    case kVPkgDigestAlgorithmMD5: {
      VPkgQuery_DIGEST_MD5(digestStr, uidStrPtr, pkgId, versionId, startDate, endDate, aggregate, aggregateByCount, everyone, quiet);
      break;
    }
    case kVPkgDigestAlgorithmSHA1: {
      VPkgQuery_DIGEST_SHA1(digestStr, uidStrPtr, pkgId, versionId, startDate, endDate, aggregate, aggregateByCount, everyone, quiet);
      break;
    }
    case kVPkgDigestAlgorithmSHA256: {
      VPkgQuery_DIGEST_SHA256(digestStr, uidStrPtr, pkgId, versionId, startDate, endDate, aggregate, aggregateByCount, everyone, quiet);
      break;
    }
    default:
      break;
  }

  // Convert the digest bytes to a string:
  int             src = digestLength;
  int             dst = 2 * src;
  
  digestStr[dst] = '\0';
  dst -= 2;
  while ( src-- ) {
    byteToHexStr(digestStr[src], &digestStr[dst]);
    dst -= 2;
  }
  
  return digestStr;
}

//

typedef struct __VPkgQueryDataBuffer {
  size_t        capacity, length;
  unsigned char *pointer;
} VPkgQueryDataBuffer;

VPkgQueryDataBuffer* VPkgQueryDataBufferCreate(
  size_t        capacity
)
{
  VPkgQueryDataBuffer*  newBuffer = NULL;
  
  if ( (newBuffer = malloc(sizeof(VPkgQueryDataBuffer))) ) {
    newBuffer->capacity = ( capacity > 0 ? capacity : 0 );
    newBuffer->length = 0;
    newBuffer->pointer = ( capacity > 0 ? malloc(capacity) : NULL );
  }
  return newBuffer;
}

void VPkgQueryDataBufferRelease(
  VPkgQueryDataBuffer   *aBuffer
)
{
  free(aBuffer);
}

bool VPkgQueryDataBufferAppend(
  VPkgQueryDataBuffer   *aBuffer,
  void                  *bytePtr,
  size_t                byteLength
)
{
  size_t                newLength = aBuffer->length + byteLength;
  
  if ( newLength > aBuffer->capacity ) {
    unsigned char       *newPointer = NULL;
    size_t              newCapacity = aBuffer->capacity;
    
    while ( newCapacity < newLength ) newCapacity += 4096;
    
    newPointer = realloc(aBuffer->pointer, newCapacity);
    if ( ! newPointer ) { return false; }
    aBuffer->pointer = newPointer;
    aBuffer->capacity = newCapacity;
  }
  
  memcpy(aBuffer->pointer + aBuffer->length, bytePtr, byteLength);
  aBuffer->length = newLength;
  
  return true;
}

//

static size_t __VPkgSendMessageWriteCallback(
  void    *ptr,
  size_t  size,
  size_t  nmemb,
  void    *inBuffer
)
{
  VPkgQueryDataBuffer   *theBuffer = (VPkgQueryDataBuffer*)inBuffer;
  
  if ( VPkgQueryDataBufferAppend(theBuffer, ptr, size * nmemb) ) {
    return size * nmemb;
  }
  return 0;
}

int
VPkgQuerySendMessage(
  uid_t                 uidNumber,
  const char*           pkgId,
  const char*           versionId,
  const char*           startDate,
  const char*           endDate,
  bool                  aggregate,
  bool                  aggregateByCount,
  bool                  everyone,
  bool                  quiet,
  const char*           digestStr
)
{
  CURL                  *curlContext = NULL;
  CURLcode              rc;
  
  struct curl_httppost  *formData = NULL;
  struct curl_httppost  *lastField = NULL;
  
  curl_global_init(CURL_GLOBAL_ALL);
  
  // Build the form data:
  if ( uidNumber > 0 ) {
    char                uidStr[32];
    
    if ( uidNumber == (uid_t)-1 ) {
      strcpy(uidStr, "*");
    } else {
      snprintf(uidStr, sizeof(uidStr), "%u", (unsigned int)uidNumber);
    }
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "userId",
                 CURLFORM_COPYCONTENTS, uidStr,
                 CURLFORM_END);
  }
  if ( pkgId ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "pkgIdPattern",
                 CURLFORM_COPYCONTENTS, pkgId,
                 CURLFORM_END);
  }
  if ( versionId ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "versionIdPattern",
                 CURLFORM_COPYCONTENTS, versionId,
                 CURLFORM_END);
  }
  if ( startDate ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "startDate",
                 CURLFORM_COPYCONTENTS, startDate,
                 CURLFORM_END);
  }
  if ( endDate ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "endDate",
                 CURLFORM_COPYCONTENTS, endDate,
                 CURLFORM_END);
  }
  if ( aggregate ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "aggregate",
                 CURLFORM_COPYCONTENTS, "1",
                 CURLFORM_END);
    if ( aggregateByCount ) {
      curl_formadd(&formData,
                   &lastField,
                   CURLFORM_COPYNAME, "aggregateByCount",
                   CURLFORM_COPYCONTENTS, "1",
                   CURLFORM_END);
    }
  }
  if ( everyone ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "everyone",
                 CURLFORM_COPYCONTENTS, "1",
                 CURLFORM_END);
  }
  if ( quiet ) {
    curl_formadd(&formData,
                 &lastField,
                 CURLFORM_COPYNAME, "quiet",
                 CURLFORM_COPYCONTENTS, "1",
                 CURLFORM_END);
  }
  curl_formadd(&formData,
               &lastField,
               CURLFORM_COPYNAME, "digestStr",
               CURLFORM_COPYCONTENTS, digestStr,
               CURLFORM_END);
  
  // Ready to send:
  if ( (curlContext = curl_easy_init()) ) {
    long                  requestTimeout = VPkgQuery_REQUEST_TIMEOUT;
    long                  connectTimeout = VPkgQuery_CONNECT_TIMEOUT;
    struct curl_slist     *headerList = curl_slist_append(NULL, "Expect: ");
    VPkgQueryDataBuffer   *readBuffer = VPkgQueryDataBufferCreate(VPkgQuery_RECV_BUFFERSIZE);
    
    curl_easy_setopt(curlContext, CURLOPT_URL, VPkgQueryURL);
    curl_easy_setopt(curlContext, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curlContext, CURLOPT_HTTPPOST, formData);
    curl_easy_setopt(curlContext, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curlContext, CURLOPT_TIMEOUT, requestTimeout);
    curl_easy_setopt(curlContext, CURLOPT_CONNECTTIMEOUT, connectTimeout);
    curl_easy_setopt(curlContext, CURLOPT_WRITEFUNCTION, __VPkgSendMessageWriteCallback);
    curl_easy_setopt(curlContext, CURLOPT_WRITEDATA, readBuffer);
    
    // Perform the request:
    rc = curl_easy_perform(curlContext);
    if ( rc == CURLE_OK ) {
      long      httpResponse = 0;
      
      curl_easy_getinfo(curlContext, CURLINFO_RESPONSE_CODE, &httpResponse);
      if ( httpResponse == 200 ) {
        if ( readBuffer->length > 0 ) {
          fwrite(readBuffer->pointer, readBuffer->length, 1, stdout);
        }
      } else {
        fprintf(stderr, "ERROR:  query failed with HTTP code %ld", httpResponse);
        if ( readBuffer->length > 0 ) {
          fprintf(stderr, ":\n  ");
          fwrite(readBuffer->pointer, readBuffer->length, 1, stderr);
        }
        fprintf(stderr, "\n");
        rc = -1;
      }
    }
    
    // Cleanup context and header list:
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curlContext);
  }
  
  curl_formfree(formData);
  
  return ( rc == CURLE_OK ? 0 : EIO );
}

//

#include <getopt.h>

const struct option VPkgCliOptions[] = {
          { "use-md5",          no_argument,        NULL,         '1' },
          { "use-sha1",         no_argument,        NULL,         '2' },
          { "use-sha256",       no_argument,        NULL,         '3' },
          { "package-id",       required_argument,  NULL,         'p' },
          { "version-id",       required_argument,  NULL,         'v' },
          { "start-date",       required_argument,  NULL,         's' },
          { "end-date",         required_argument,  NULL,         'e' },
          { "aggregate",        no_argument,        NULL,         'A' },
          { "by-count",         no_argument,        NULL,         'c' },
          { "uid-number",       required_argument,  NULL,         'u' },
          { "everyone",         no_argument,        NULL,         'a' },
          { "quiet",            no_argument,        NULL,         'q' },
          { "help",             no_argument,        NULL,         'h' },
          { NULL,               0,                  NULL,         0   }
        };

//

void
VPkgQueryUsage(
  const char*   exeName
)
{
  printf(
      "usage:\n\n"
      "  %s {options}\n\n"
      " options:\n\n"
      "  -p/--package-id <pattern>    Restrict output to packages matching this id\n"
      "  -v/--version-id <pattern>    Restrict output to versions matching this id\n"
      "  -s/--start-date YYYY-MM-DD   Restrict output to this date and later\n"
      "  -e/--end-date YYYY-MM-DD     Restrict output to this date and earlier\n"
      "  -A/--aggregate               Sum across all resulting dates\n"
      "    -c/--by-count              Order aggregated results by count rather than\n"
      "                               by package name\n"
      "  -q/--quiet                   Do not show the query summary\n",
      exeName
    );
  if ( geteuid() == 0 ) {
    printf(
        "  -u/--uid-number #|*          Restrict output to a uid number or display\n"
        "                               per-user info if '*' is used\n"
        "  -a/--everyone                Aggregate across all users\n"
        "\n"
        "  -1/--use-md5                 Use MD5 digests\n"
        "  -2/--use-sha1                Use SHA digests\n"
        "  -3/--use-sha256              Use SHA-256 digests\n"
      );
  }
  printf(
      "\n"
      "  Package and version patterns may use the wildcard character '*' but are\n"
      "  otherwise restricted by the same syntax rules as in VALET.  E.g.\n"
      "\n"
      "    %s -p '*mpi*'\n"
      "\n"
      "  selects all package ids containing the text 'mpi'.\n"
      "\n"
      "  Dates can be specified without the '-MM-DD' or '-DD' component to imply\n"
      "  all months of the given year or all days of the given month, respectively.\n"
      "  Some special values are also understood -- see\n"
      "\n"
      "    http://us3.php.net/manual/en/datetime.formats.relative.php\n"
      "\n"
      "  for a discussion of the recognized values.  If no start- or end-date are\n"
      "  specified then the program defaults to querying the last week's worth of\n"
      "  records.\n"
      "\n"
      "$Id: vpkg_query.c 531 2014-10-31 16:18:05Z frey $\n",
      exeName
    );
  exit(0);
}

//

int
main(
  int           argc,
  char* const   *argv
)
{
  VPkgDigestAlgorithm   algorithm = kVPkgDigestAlgorithmDefault;
  int                   ch;
  const char            *exeName = argv[0];
  
  const char            *pkgId = NULL;
  const char            *versionId = NULL;
  const char            *startDate = NULL;
  const char            *endDate = NULL;
  bool                  aggregate = false;
  bool                  aggregateByCount = false;
  bool                  everyone = false;
  bool                  quiet = false;
  uid_t                 specificUid = 0;
  char                  *digestStr = NULL;
  
  while ( (ch = getopt_long(argc, argv, "123p:v:s:e:Acu:aqh", VPkgCliOptions, NULL)) != -1 ) {
    switch ( ch ) {
      case '1':
        algorithm = kVPkgDigestAlgorithmMD5;
        break;
      case '2':
        algorithm = kVPkgDigestAlgorithmSHA1;
        break;
      case '3':
        algorithm = kVPkgDigestAlgorithmSHA256;
        break;
      case 'h':
        VPkgQueryUsage(exeName);
        break;
      case 'p':
        pkgId = (optarg && *optarg) ? optarg : NULL;
        break;
      case 'v':
        versionId = (optarg && *optarg) ? optarg : NULL;
        break;
      case 's':
        startDate = (optarg && *optarg) ? optarg : NULL;
        break;
      case 'e':
        endDate = (optarg && *optarg) ? optarg : NULL;
        break;
      case 'A':
        aggregate = true;
        break;
      case 'c':
        aggregateByCount = true;
        break;
      case 'u':
        if ( geteuid() == 0 ) {
          if ( optarg && *optarg ) {
            if ( strcmp(optarg, "*") == 0 ) {
              specificUid = -1;
              everyone = false;
            } else {
              char*       endPtr = NULL;
              long        uidNumber = strtol(optarg, &endPtr, 10);
              
              if ( endPtr && uidNumber > 0 ) {
                everyone = false;
                specificUid = uidNumber;
              }
            }
          }
        } else {
          fprintf(stderr, "WARNING:  that option is only available to root\n");
        }
        break;
      case 'a':
        if ( geteuid() == 0 ) {
          everyone = true;
          specificUid = 0;
        } else {
          fprintf(stderr, "WARNING:  that option is only available to root\n");
        }
        break;
      case 'q':
        quiet = true;
        break;
      default:
        break;
    }
  }
  
  // If no date range was provided, then by default let's use the last 7 days:
  if ( ! startDate && ! endDate ) {
    startDate = "-1 week";
    endDate = "today";
  }
  
  // If not everyone and no specific uid, then currect user id:
  if ( ! everyone && (specificUid == 0) ) specificUid = getuid();
  
  // Validate our request with a digest string:
  digestStr = VPkgQueryCreateDigest(algorithm, specificUid, pkgId, versionId, startDate, endDate, aggregate, aggregateByCount, everyone, quiet);
    
  if ( digestStr ) {
    ch = VPkgQuerySendMessage(specificUid, pkgId, versionId, startDate, endDate, aggregate, aggregateByCount, everyone, quiet, digestStr);
    free(digestStr);
    return ch;
  }
  return ENOMEM;
}
