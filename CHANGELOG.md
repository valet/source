# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [2.1.0]
This is the first version of VALET to be made available to the public via gitlab.

### Changed
- Full rewrite of code base
- Merge of "envsnapshot" Python code into "valet" package

### Added
- Contextual factoring of packages/versions/config entities added (absorbs previous "development environment" behavior)
- Generalization of package/version "flags" (absorbs development-env and standard-paths flags)
- Decomposition of version id into recognizable numeric versioning components and strings for the sake of sorting
- Version id predicates augmenting extant version id regex pattern syntax
- Adaptive sub-path checking (e.g. detect local architecture, add hw-specific sub-paths to PATH)
- YAML and Python-native package definition file formats

## [2.0.0] 2014-10-30
Released on the University of Delaware "Farber" community cluster.

### Changed
- Environment snapshots are now written to a file and not kept in the environment itself

### Added
- Feature tags as a part of the versioned package identifier

### Fixed
- Environment snapshots now include aliases, functions, and all variables (not just those exported)

## [1.0.0] 2011-12-20
Initial release of the software.  Used on the University of Delaware "Mills" community cluster and a few private Engineering clusters.
