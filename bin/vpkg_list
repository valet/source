#!/usr/bin/python -E
# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

#
# Setup the path to the VALET install and import it.  The assumption
# here is that if VALET_PREFIX is _not_ set in the environment, the
# system has VALET installed in one of its default Python module
# containers.
#
import os
import sys

valetPrefix = os.getenv('VALET_PREFIX')
if valetPrefix:
  valetLibPath = os.path.join(valetPrefix, 'lib', 'python')
  if valetLibPath not in sys.path:
    sys.path.insert(1, valetLibPath)

import valet


#
# Process CLI arguments:
#
import argparse

parser = argparse.ArgumentParser(
        description = 'List the available packages (by their package id).',
        epilog="If no package ids are provided, a list of available packages is produced. By default the list appears in sections, with each section showing packages within one of the available configuration directories.  The -u/--unify flag will collapse the display into a single list across all configuration directories. If any package ids are provided, the output is limited to packages matching those ids.")
parser.add_argument(
        '--verbose', '-v',
        dest='isVerbose',
        action='store_true',
        help='increase the amount of output the program generates')
parser.add_argument(
        '--context', '-c',
        dest='context',
        metavar='<context-list>',
        help='choose alternate environment context(s) (e.g. development)')
parser.add_argument(
        '--unify', '-u',
        dest='shouldUnify',
        action='store_true',
        help='display a single summary of package names across all configuration directories')
parser.add_argument(
        'pkgId',
        metavar='<pkgId>',
        nargs='*',
        help='optional package ids to lookup')
args = parser.parse_args()


#
# Process the options we were presented:
#
if args.isVerbose:
  os.environ['VALET_VERBOSE'] = 'yes'
if args.context:
  targetContext = set(args.context.split(','))
else:
  targetContext = valet.contexts.standard
unifyAcrossDirs = args.shouldUnify
isIdList = False

#
# Replace "args" with the remaining CLI arguments after options:
#
args = args.pkgId

#
# Main program:
#

import textwrap

def subEmptyStrForNone(obj):
  if obj is None:
    return ''
  return obj

#
# Make a packageManager to facilitate finding and loading package definitions:
#
vmgr = valet.packageManager.sharedPackageManager()

if len(args) == 0:
  try:
    pkgList = vmgr.packageIdList(unifyAcrossDirs, targetContext)
    isIdList = True
  except Exception as e:
    print 'ERROR:  ' + str(e)
    sys.exit(22)
else:
  pkgList = []
  for pkgStr in args:
    try:
      morePkgs = vmgr.packagesWithId(pkgStr, targetContext)
      if morePkgs:
        pkgList.extend(morePkgs)
      else:
        print 'ERROR:  No package with id "{0}"'.format(pkgStr)
        sys.exit(22)
    except Exception as e:
      print 'ERROR:  ' + str(e)
      sys.exit(22)
      
if pkgList is not None:
  print
  print 'Available packages:'
  if unifyAcrossDirs:
    #
    # Show a single list, irrespective of source directories:
    #
    if isIdList:
      #
      # No pkgId arguments were present, so the list of valet.id
      # instances is what we received:
      #
      for pkgId in pkgList:
        print '    ' + str(pkgId)
    else:
      #
      # We had pkgId arguments present, so the list of valet.package
      # instances is what we received:
      #
      for pkg in pkgList:
        print '    ' + str(pkg.id())
  else:
    #
    # Split the list out into source directories:
    #
    if isIdList:
      #
      # No pkgId arguments were present, so we got back a dictionary or
      # path = [valet.id, ..] pairs:
      #
      for path, pkgs in pkgList.iteritems():
        print '  in ' + path
        for pkg in pkgs:
          print '    ' + str(pkg)
    else:
      #
      # We got back a flat list of valet.package instances, so we have
      # to split them out ourself...
      #
      splitPkgList = {}
      for pkg in pkgList:
        pkgPath = pkg.sourceFile()
        if pkgPath:
          pkgPath = os.path.dirname(pkgPath)
        else:
          pkgPath = '<n/a>'
        if pkgPath not in splitPkgList:
          splitPkgList[pkgPath] = [pkg]
        else:
          splitPkgList[pkgPath].append(pkgs)
      #
      # ...and then sort and print each per-directory list:
      #
      for path, pkgs in splitPkgList.iteritems():
        print '  in ' + path
        for pkg in sorted(pkgs):
          print '    ' + str(pkg.id())

  #
  # Print any accumulated error messages:
  #
  header = False
  wrapper = textwrap.TextWrapper(initial_indent = '  * ', subsequent_indent = '    ')
  if valet.config.isVerbose():
    for warnStr in vmgr.warningStrings():
      if not header:
        print os.linesep + 'Warnings:' + os.linesep
        header = True
      print wrapper.fill(warnStr)
      print
  if header:
    print
    header = False
  for errStr in vmgr.errorStrings():
    if not header:
      print os.linesep + 'Errors:' + os.linesep
      header = True
    print wrapper.fill(errStr)
    print
  if header:
    print
