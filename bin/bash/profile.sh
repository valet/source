#
# bash-specific VALET environment setup for profile.d
#
# $Id$
#

if [ -f /etc/sysconfig/valet ]; then
  . /etc/sysconfig/valet
fi

if [ -f ~/.valetrc ]; then
  . ~/.valetrc
fi

# Set the VALET prefix path:
if [ -n "${VALET_PREFIX}" -a "${VALET_PREFIX}" != '/opt/shared/valet/2.1' ]; then
  source "${VALET_PREFIX}/bin/bash/profile.sh"
else
  if [ -z "${VALET_PREFIX}" ]; then
    export VALET_PREFIX='/opt/shared/valet/2.1'
  fi

  # Configure PATH and MANPATH only if we're missing our pieces:
  case ":${PATH}:" in
    *:"${VALET_PREFIX}/bin":*)
      ;;
    *)
      export PATH="${VALET_PREFIX}/bin${PATH:+:}${PATH}"
      ;;
  esac
  #
  case ":${PATH}:" in
    *:"${VALET_PREFIX}/bin/bash":*)
      ;;
    *)
      export PATH="${VALET_PREFIX}/bin/bash${PATH:+:}${PATH}"
      ;;
  esac
  #
  case ":${MANPATH}:" in
    *:"${VALET_PREFIX}/man":*)
      ;;
    *)
      export MANPATH=":${VALET_PREFIX}/man${MANPATH:+:}${MANPATH}"
      ;;
  esac

  #
  # @function vpkg_require
  # @discussion
  #   Given a list of package/versioned-package strings as arguments,
  #   use the in-scope vpkg_emitconf script (should be in bin/bash)
  #   to generate the commands that will modify the environment.
  #
  #   If successful (return code 0) then evaluate the commands.
  #
  #   Otherwise, the output from vpkg_emitconf is considered to be
  #   error/warning text that is echoed to stdout.
  #
  #   The return code from vpkg_emitconf is returned to the caller
  #   and can be examined in $?.
  #
  vpkg_require() {
    local vpkg_conf_quiet=0
    local vpkg_conf_verbose=0
    local vpkg_conf_cmd=''
    local vpkg_conf_rc=0
    local vpkg_args=()

    while [ $# -gt 0 ]; do
      case "$1" in
        -h|--help)
          vpkg_emitconf --help | sed 's/vpkg_emitconf/vpkg_require/g'
          return $?
          ;;
        -v|--verbose)
          vpkg_conf_quiet=0
          vpkg_conf_verbose=1
          ;;
        -q|--quiet)
          vpkg_conf_quiet=1
          vpkg_conf_verbose=0
          ;;
        *)
          vpkg_args+=("$1")
          ;;
      esac
      shift
    done

    vpkg_conf_rc=0
    if [ $vpkg_conf_verbose -ne 0 ]; then
      vpkg_conf_cmd=$(vpkg_emitconf --verbose "${vpkg_args[@]}")
    else
      vpkg_conf_cmd=$(vpkg_emitconf "${vpkg_args[@]}")
    fi
    if [ $? -eq 0 ]; then
      if [ $vpkg_conf_quiet -eq 0 ]; then
        eval "$vpkg_conf_cmd"
      else
        eval "$vpkg_conf_cmd" > /dev/null 2>&1
      fi
      vpkg_conf_rc=$?
    else
      vpkg_conf_rc=$?
      if [ $vpkg_conf_quiet -eq 0 ]; then
        printf "%s\n\n" "$vpkg_conf_cmd" | sed 's:'"${VALET_PREFIX}/bin/bash/vpkg_emitconf"':vpkg_require:'
      fi
    fi
    unset vpkg_conf_cmd vpkg_conf_quiet vpkg_conf_rc
    return $vpkg_conf_rc
  }
  export -f vpkg_require

  #
  # @function vpkg_devrequire
  # @discussion
  #   Similar to vpkg_require but enables the emission of additional
  #   development environment variables, etc., by always using the
  #   '--context=development' flag when invoking vpkg_emitconf.
  #
  vpkg_devrequire() {
    local vpkg_conf_quiet=0
    local vpkg_conf_verbose=0
    local vpkg_conf_cmd=''
    local vpkg_conf_rc=0
    local vpkg_args=()

    while [ $# -gt 0 ]; do
      case "$1" in
        -c|--context)
          # Discard any context specifications:
          #vpkg_args+="$1"
          #vpkg_args+="$2"
          shift
          ;;
        -h|--help)
          vpkg_emitconf --help | sed 's/vpkg_emitconf/vpkg_devrequire/g'
          return $?
          ;;
        -v|--verbose)
          vpkg_conf_quiet=0
          vpkg_conf_verbose=1
          ;;
        -q|--quiet)
          vpkg_conf_quiet=1
          vpkg_conf_verbose=0
          ;;
        *)
          vpkg_args+=("$1")
          ;;
      esac
      shift
    done

    vpkg_conf_rc=0
    if [ $vpkg_conf_verbose -ne 0 ]; then
      vpkg_conf_cmd=$(vpkg_emitconf --verbose --context=development "${vpkg_args[@]}")
    else
      vpkg_conf_cmd=$(vpkg_emitconf --context=development "${vpkg_args[@]}")
    fi
    if [ $? -eq 0 ]; then
      if [ $vpkg_conf_quiet -eq 0 ]; then
        eval "$vpkg_conf_cmd"
      else
        eval "$vpkg_conf_cmd" > /dev/null 2>&1
      fi
      vpkg_conf_rc=$?
    else
      vpkg_conf_rc=$?
      if [ $vpkg_conf_quiet -eq 0 ]; then
        printf "%s\n\nUsing this variant, a '--context=development' is implied.\n\n" "$vpkg_conf_cmd" | sed 's:'"${VALET_PREFIX}/bin/bash/vpkg_emitconf"':vpkg_devrequire:'
      fi
    fi
    unset vpkg_conf_cmd vpkg_conf_quiet vpkg_conf_rc
    return $vpkg_conf_rc
  }
  export -f vpkg_devrequire

  #
  # @function vpkg_rollback
  # @discussion
  #   Use the __vpkg_emitrollback_driver function (source'd from the in-scope
  #   vpkg_emitrollback script) to generate the statements necessary to
  #   remove the effects of one or more vpkg_require/vpkg_devrequire
  #   invocations.
  #
  #   The commands are written to a temporary file.  If __vpkg_emitrollback_driver
  #   has a non-zero return code, it is assumed some aspect of its function
  #   failed and an error message was already written to stdout.  If zero, then
  #   the temporary file is source'd to apply the necessary changes.
  #
  #   When all rollbacks specified have completed, the temporary file is removed
  #   and this function exits.
  #
  vpkg_rollback() {
    local vpkg_rollback_all=0
    local vpkg_rollback_count=0
    local vpkg_rollback_cmds

    source ${VALET_PREFIX}/bin/bash/vpkg_emitrollback

    if [ -n "$VALET_SESSION_ID" ]; then
      vpkg_rollback_cmds="${TMPDIR:-/tmp}/VALET-`${VALET_PREFIX}/bin/valet_uniqpid $$`-${VALET_SESSION_ID}.rollback"
      if [ "x$1" = "xall" ]; then
        touch "$vpkg_rollback_cmds"
        chmod 600 "$vpkg_rollback_cmds"
        while [ -n "$VALET_SESSION_ID" ]; do
          __vpkg_emitrollback_driver "$vpkg_rollback_cmds"
          if [ $? -eq 0 ]; then
            source "$vpkg_rollback_cmds"
          else
            break
          fi
        done
        rm -f "$vpkg_rollback_cmds"
      else
        if [ "x$1" = "x" ]; then
          vpkg_rollback_count=1
        else
          if [[ $1 =~ ^[0-9][0-9]*$ ]]; then
            vpkg_rollback_count=$1
          else
            echo "ERROR:  invalid argument to vpkg_rollback:  $1"
            return 1
          fi
        fi
        touch "$vpkg_rollback_cmds"
        chmod 600 "$vpkg_rollback_cmds"
        while [ $vpkg_rollback_count -gt 0 ]; do
          __vpkg_emitrollback_driver "$vpkg_rollback_cmds"
          if [ $? -eq 0 ]; then
            source "$vpkg_rollback_cmds"
          else
            break
          fi
          vpkg_rollback_count=$((vpkg_rollback_count - 1))
        done
        rm -f "$vpkg_rollback_cmds"
      fi
    else
      echo 'ERROR:  no previous session on record, unable to roll back'
    fi
    unset -f __vpkg_emitrollback_driver
    unset vpkg_rollback_cmds
    unset vpkg_rollback_count
    unset vpkg_rollback_all
  }
  export -f vpkg_rollback

  #
  # @function vpkg_replicate_parent
  # @discussion
  #   Since environment state is kept in files under a directory whose name
  #   maps from the current shell, spawing another shell will produce a
  #   completely new VALET context lacking any environment state.  Thus,
  #   this would fail:
  #
  #     $ vpkg_require intel/2015
  #     $ bash
  #     $ vpkg_rollback
  #
  #   The newly-spawned shell can have its parent shells' environment state
  #   files copied into its state directory; this allows rollbacks to proceed
  #   as expected within the current shell.
  #
  vpkg_replicate_parent() {
    local vpkg_my_uniqpid=`${VALET_PREFIX}/bin/valet_uniqpid $$`
    local vpkg_parent_index=1
    local vpkg_parent_uniqpid=`${VALET_PREFIX}/bin/valet_uniqpid -p $$`

    while [ $? -eq 0 ]; do
      if [ -d "${TMPDIR:-/tmp}/VALET-${vpkg_parent_uniqpid}" ]; then
        if [ ! -d "${TMPDIR:-/tmp}/VALET-${vpkg_my_uniqpid}" ]; then
          mkdir -p --mode=0700 "${TMPDIR:-/tmp}/VALET-${vpkg_my_uniqpid}"
          if [ $? -ne 0 ]; then
            echo "ERROR:  could not create this shell's VALET rollback snapshot directory"
            unset vpkg_parent_uniqpid vpkg_my_uniqpid
            return 1
          fi
        fi
        cp "${TMPDIR:-/tmp}/VALET-${vpkg_parent_uniqpid}/"* "${TMPDIR:-/tmp}/VALET-${vpkg_my_uniqpid}"
        echo "DONE:  VALET rollback snapshots from ${vpkg_parent_uniqpid} populated in this shell"
      fi
      vpkg_parent_index=$((vpkg_parent_index+1))
      vpkg_parent_uniqpid=`${VALET_PREFIX}/bin/valet_uniqpid -p=${vpkg_parent_index} $$`
    done
    unset vpkg_parent_uniqpid
    unset vpkg_my_uniqpid
  }
  export -f vpkg_replicate_parent

  #
  # @function vpkg_help
  # @discussion
  #   Display a VALET command summary on stdout.
  #
  vpkg_help() {
    if [ -r "${VALET_PREFIX}/doc/vpkg_help_bash.txt" ]; then
      fmt --width=${COLUMNS:-$(tput cols)} "${VALET_PREFIX}/doc/vpkg_help_bash.txt" | less
    else
      man valet
    fi
  }
  export -f vpkg_help

  #
  # @function vpkg_history
  # @discussion
  #   Break the VALET_HISTORY environment variable into distinct versioned-
  #   package strings and display them one per line.
  #
  vpkg_history() {
    echo $VALET_HISTORY | /bin/sed -r -e 's/\|/\n/g' -e 's/\]/]\n  /g' -e 's/;/\n  /g'
  }
  export -f vpkg_history

  #
  # @function __valetcompl_pkgs
  # @discussion
  #   Generate bash command completion for package ids.
  #
  __valetcompl_pkgs()
  {
    cur=${COMP_WORDS[COMP_CWORD]}
    packages=`vpkg_list_completionHelper`
    COMPREPLY=( $(compgen -W "$packages" -- $cur) )
    return 0
  }
  export -f __valetcompl_pkgs

  #
  # @function __valetcompl_vers
  # @discussion
  #   Generate bash command completion for versioned-package ids.
  #
  __valetcompl_vers()
  {
    cur=${COMP_WORDS[COMP_CWORD]}
    colonprefixes=${cur%"${cur##*:}"}
    # Are we completing the package name or a version name?
    hasSlash=`echo "$cur" | grep / | wc -l`
    if [ $hasSlash -eq 0 ]; then
      packages=`vpkg_list_completionHelper`
      COMPREPLY=( $( compgen -W "$packages" -- $cur ) )
    else
      versions=`vpkg_versions_completionHelper "$cur"`
      COMPREPLY=( $( compgen -W "$versions" -- $cur ) )
    fi
    local i=${#COMPREPLY[*]}
    while [ $((--i)) -ge 0 ]; do
      COMPREPLY[$i]=${COMPREPLY[$i]#"$colonprefixes"}
    done
    return 0
  }
  export -f __valetcompl_vers

  #
  # Setup the completion behavior for VALET commands:
  #
  complete -o bashdefault -o nospace -F __valetcompl_vers vpkg_info vpkg_require vpkg_devrequire
  complete -o bashdefault -F __valetcompl_pkgs vpkg_versions
fi
