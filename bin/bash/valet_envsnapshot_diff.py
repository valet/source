#!/usr/bin/python -E
# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

#
# valet_envdiff.py
#
# Generate a BASH environment snapshot from the data passed on stdin.  The
# data coming on stdin is expected to be:
#
#   printf "%s\n" "`(typeset -p; typeset -f; alias -p)`"
#
# Compare this data against a snapshot contained in the filename passed to
# this utility and emit BASH commands that would transform the current
# environment into the other (from the snapshot file).
#

#
# Setup the path to the VALET install and import it.  The assumption
# here is that if VALET_PREFIX is _not_ set in the environment, the
# system has VALET installed in one of its default Python module
# containers.
#
import os
import sys

valetPrefix = os.getenv('VALET_PREFIX')
if valetPrefix:
  valetLibPath = os.path.join(valetPrefix, 'lib', 'python')
  if valetLibPath not in sys.path:
    sys.path.insert(1, valetLibPath)

import valet.envsnapshot
import re
import fileinput

skipVariables = ('_', 'PIPESTATUS', 'FUNCNAME', 'vpkg_rollback_all', 'vpkg_rollback_count', 'vpkg_rollback_cmds', 'VALET_NEXT_SESSION_ID', 'VALET_PREVIOUS_ENV_DUMP', 'BASH_LINENO', 'BASH_SOURCE', 'BASH_FUNCNAME', 'PWD', 'OLDPWD')

if len(sys.argv) < 2:
  sys.stderr.write("usage:\n\n  %s {-v} <snapshot-file>\n\n" % (sys.argv[0]))
  exit(1)
if sys.argv[1] == '-v':
  isVerbose = True
  prevShellObjects = valet.envsnapshot.ShellObjects.importFromFile(sys.argv[2])
else:
  isVerbose = False
  prevShellObjects = valet.envsnapshot.ShellObjects.importFromFile(sys.argv[1])

if prevShellObjects is None:
  if isVerbose:
    sys.stderr.write("ERROR:  invalid shell named-entity snapshot file: %s\n" % (sys.argv[1]))
  exit(1)

inFunction = False
inMultilineVar = False
shellObjects = valet.envsnapshot.ShellObjects()

for nextLine in fileinput.input(("-")):
  if inFunction:
    matchObj = re.match(r'^}\s*$', nextLine)
    if matchObj is None:
      functionBody = functionBody + nextLine
    else:
      inFunction = False
      if re.match(r'^__vpkg_[a-z0-9_]+_driver', functionName) is None:
        shellObjects.addObject(valet.envsnapshot.ShellFunction(functionName, functionBody + nextLine))
  elif inMultilineVar:
    if nextLine.endswith("\"\n") and not nextLine.endswith("\\\"\n"):
      inMultilineVar = False
      if not varName in skipVariables:
        varValue = varValue + nextLine.rstrip("\n")
        shellObjects.addObject(valet.envsnapshot.ShellVariable(varName, varValue, varFlags))
    else:
      varValue = varValue + nextLine
  else:
    matchObj = re.match(r'^alias ([^=]+)=(.*)$', nextLine)
    if matchObj is not None:
      shellObjects.addObject(valet.envsnapshot.ShellAlias(matchObj.group(1), matchObj.group(2)))
      continue

    matchObj = re.match(r'^declare (-([A-Za-z-]+) )?([^=]+)=(.*)$', nextLine)
    if matchObj is not None:
      flags = matchObj.group(2)
      if flags == '-':
        flags = None

      # Is it multiline?
      if matchObj.group(4).startswith('"') and not matchObj.group(4).endswith('"'):
        inMultilineVar = True
        varName = matchObj.group(3)
        varValue = matchObj.group(4) + "\n"
        varFlags = flags
        continue

      if not matchObj.group(3) in skipVariables:
        shellObjects.addObject(valet.envsnapshot.ShellVariable(matchObj.group(3), matchObj.group(4), flags))
      continue

    matchObj = re.match(r'^([^ ]+) \(\)\s*$', nextLine)
    if matchObj is not None:
      inFunction = True
      functionName = matchObj.group(1)
      functionBody = ''
      continue

    matchObj = re.match(r'^declare -([a-z]*f[a-z]*) (.*)$', nextLine)
    if matchObj is not None:
      if re.match(r'^__vpkg_[a-z0-9_]+_driver', matchObj.group(2)) is None:
        targetFn = shellObjects.objectMatching(valet.envsnapshot.ShellFunction(matchObj.group(2), ''))
        if targetFn is not None and not matchObj.group(1) == 'f' :
          targetFn.setFlags(matchObj.group(1))
      continue

# Compute the difference:
objectDiff = prevShellObjects.objectDiff(shellObjects)
for diff in objectDiff:
  # Named-entity not present in old environment:
  if diff[0] is None:
    if isinstance(diff[1], valet.envsnapshot.ShellVariable):
      flags = diff[1].flags()
      # Can't unset read-only variables, sorry!
      if flags is None or not 'r' in flags:
        print "unset %s" % (diff[1].name())
      elif isVerbose:
        sys.stderr.write("WARNING:  cannot remove read-only variable %s\n" % (diff[0].name()))
    elif isinstance(diff[1], valet.envsnapshot.ShellAlias):
      print "unalias %s" % (diff[1].name())
    elif isinstance(diff[1], valet.envsnapshot.ShellFunction):
      print "unset -f %s" % (diff[1].name())
  # Named-entity not present in current environment:
  elif diff[1] is None:
    if isinstance(diff[0], valet.envsnapshot.ShellVariable):
      print "%s=%s" % (diff[0].name(), diff[0].value())
      flags = diff[0].flags()
      if flags is not None:
        if 'x' in flags:
          print "export %s" % (diff[0].name())
          flags = flags.replace('x', '')
        if len(flags) > 0:
          print "declare -%s %s" % (flags, diff[0].name())
    elif isinstance(diff[0], valet.envsnapshot.ShellAlias):
      print "alias %s=%s" % (diff[0].name(), diff[0].value())
    elif isinstance(diff[0], valet.envsnapshot.ShellFunction):
      print "%s ()\n%s" % (diff[0].name(), diff[0].value())
      flags = diff[0].flags()
      if flags is not None:
        print "declare -%s %s" % (flags, diff[0].name())
  # Named-entity modified between environments:
  else:
    if isinstance(diff[0], valet.envsnapshot.ShellVariable):
      # If the new flags are read-only, we can't modify the variable, period:
      flags = diff[1].flags()
      if flags is not None and 'r' in flags:
        if isVerbose:
          sys.stderr.write("WARNING:  cannot modify read-only variable %s\n" % (diff[0].name()))
        continue;

      # Set the variable:
      print "%s=%s" % (diff[0].name(), diff[0].value())
      flags = diff[0].flags()
      if flags is not None:
        if 'x' in flags:
          print "export %s" % (diff[0].name())
          flags = flags.replace('x', '')
        if len(flags) > 0:
          print "declare -%s %s" % (flags, diff[0].name())
    elif isinstance(diff[0], valet.envsnapshot.ShellAlias):
      print "alias %s=%s" % (diff[0].name(), diff[0].value())
    elif isinstance(diff[0], valet.envsnapshot.ShellFunction):
      # If the new flags are read-only, we can't modify the variable, period:
      flags = diff[1].flags()
      if flags and 'r' in flags:
        if isVerbose:
          sys.stderr.write("WARNING:  cannot modify read-only function %s\n" % (diff[0].name()))
        continue;

      # Set the function:
      print "%s ()\n%s" % (diff[0].name(), diff[0].value())
      flags = diff[0].flags()
      if flags is not None:
        print "declare -%s %s" % (flags, diff[0].name())



