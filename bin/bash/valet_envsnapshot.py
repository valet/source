#!/usr/bin/python -E
# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

#
# valet_envsnapshot.py
#
# Generate a BASH environment snapshot from the data passed on stdin.  The
# data coming on stdin is expected to be:
#
#   printf "%s\n" "`(typeset -p; typeset -f; alias -p)`"
#

#
# Setup the path to the VALET install and import it.  The assumption
# here is that if VALET_PREFIX is _not_ set in the environment, the
# system has VALET installed in one of its default Python module
# containers.
#
import os
import sys

valetPrefix = os.getenv('VALET_PREFIX')
if valetPrefix:
  valetLibPath = os.path.join(valetPrefix, 'lib', 'python')
  if valetLibPath not in sys.path:
    sys.path.insert(1, valetLibPath)

import valet.envsnapshot
import re
import fileinput

skipVariables = ( '_', 'PIPESTATUS', 'FUNCNAME', 'vpkg_conf_quiet', 'vpkg_conf_rc',
                  'vpkg_conf_cmd', 'VALET_NEXT_SESSION_ID', 'VALET_PREVIOUS_ENV_DUMP',
                  'BASH_LINENO', 'BASH_SOURCE', 'BASH_FUNCNAME', 'PWD', 'OLDPWD'
                )

inFunction = False
inMultilineVar = False
shellObjects = valet.envsnapshot.ShellObjects()

for nextLine in fileinput.input(("-")):
  if inFunction:
    matchObj = re.match(r'^}\s*$', nextLine)
    if matchObj is None:
      functionBody = functionBody + nextLine
    else:
      inFunction = False
      if re.match(r'^__vpkg_[a-z0-9_]+_driver', functionName) is None:
        shellObjects.addObject(valet.envsnapshot.ShellFunction(functionName, functionBody + nextLine))
  elif inMultilineVar:
    if nextLine.endswith("\"\n") and not nextLine.endswith("\\\"\n"):
      inMultilineVar = False
      if not varName in skipVariables:
        varValue = varValue + nextLine.rstrip("\n")
        shellObjects.addObject(valet.envsnapshot.ShellVariable(varName, varValue, varFlags))
    else:
      varValue = varValue + nextLine
  else:
    matchObj = re.match(r'^alias ([^=]+)=(.*)$', nextLine)
    if matchObj is not None:
      shellObjects.addObject(valet.envsnapshot.ShellAlias(matchObj.group(1), matchObj.group(2)))
      continue

    matchObj = re.match(r'^declare (-([A-Za-z-]+) )?([^=]+)=(.*)$', nextLine)
    if matchObj is not None:
      flags = matchObj.group(2)
      if flags == '-':
        flags = None

      # Is it multiline?
      if matchObj.group(4).startswith('"') and not matchObj.group(4).endswith('"'):
        inMultilineVar = True
        varName = matchObj.group(3)
        varValue = matchObj.group(4) + "\n"
        varFlags = flags
        continue

      if not matchObj.group(3) in skipVariables:
        shellObjects.addObject(valet.envsnapshot.ShellVariable(matchObj.group(3), matchObj.group(4), flags))
      continue

    matchObj = re.match(r'^([^ ]+) \(\)\s*$', nextLine)
    if matchObj is not None:
      inFunction = True
      functionName = matchObj.group(1)
      functionBody = ''
      continue

    matchObj = re.match(r'^declare -([a-z]*f[a-z]*) (.*)$', nextLine)
    if matchObj is not None:
      if re.match(r'^__vpkg_[a-z0-9_]+_driver', matchObj.group(2)) is None:
        targetFn = shellObjects.objectMatching(valet.envsnapshot.ShellFunction(matchObj.group(2), ''))
        if targetFn is not None and not matchObj.group(1) == 'f' :
          targetFn.setFlags(matchObj.group(1))
      continue

if len(sys.argv) < 2:
  if len(shellObjects.objects()) == 0:
    print "No shell named-entities found."
  else:
    print str(shellObjects)
else:
  if len(shellObjects.objects()) > 0:
    shellObjects.exportToFile(sys.argv[1])
