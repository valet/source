#!/usr/bin/python -E
# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

#
# Setup the path to the VALET install and import it.  The assumption
# here is that if VALET_PREFIX is _not_ set in the environment, the
# system has VALET installed in one of its default Python module
# containers.
#
import os
import sys

valetPrefix = os.getenv('VALET_PREFIX')
if valetPrefix:
  valetLibPath = os.path.join(valetPrefix, 'lib', 'python')
  if valetLibPath not in sys.path:
    sys.path.insert(1, valetLibPath)

import valet

# Try to determine console dimensions:
import subprocess
try:
  tty_rows, tty_cols = subprocess.check_output(['stty', 'size']).split()
  tty_rows = int(tty_rows)
  tty_cols = int(tty_cols)
except:
  # Call it "infinite" I guess...
  tty_rows = 0
  tty_cols = 0

#
# Process CLI arguments:
#
import argparse

parser = argparse.ArgumentParser(
        description = 'List the available versions of one or more packages (by their package id).')
parser.add_argument(
        '--verbose', '-v',
        dest='isVerbose',
        action='store_true',
        help='increase the amount of output the program generates')
parser.add_argument(
        '--context', '-c',
        dest='context',
        metavar='<context-list>',
        help='choose alternate environment context(s) (e.g. development)')
parser.add_argument(
        '--classic',
        dest='isClassicMode',
        action='store_true',
        help='classic mode: show features as a list of tags after the version id and description line')
parser.add_argument(
        '--attributes', '-a',
        dest='shouldShowAttributes',
        action='store_true',
        help='show package and versioned package attributes')
parser.add_argument(
        '--attributes-version-only', '-V',
        dest='shouldCoalesceAttributes',
        action='store_true',
        help='show package attributes as items on each version, not as a single distinct list')
parser.add_argument(
        'pkgId',
        metavar='<pkgId>',
        nargs='+',
        help='one or more package ids')
args = parser.parse_args()

#
# Process the options we were presented:
#
if args.isVerbose:
  os.environ['VALET_VERBOSE'] = 'yes'
if args.context:
  targetContext = set(args.context.split(','))
else:
  targetContext = valet.contexts.standard
isClassicMode = args.isClassicMode
shouldShowAttributes = args.shouldShowAttributes
shouldCoalesceAttributes = args.shouldCoalesceAttributes

#
# Replace "args" with the remaining CLI arguments after options:
#
args = args.pkgId

#
# Main program:
#


def subEmptyStrForNone(obj):
  return '' if obj is None else obj


def printAttributes(attrs, lead_width, column_width):
  for k in sorted(attrs.keys()):
    v = k + ' = ' + attrs[k]
    if column_width > 0 and len(v) > (column_width - 2):
      if (column_width - 4) > 0:
        v = v[0:column_width - 4] + '..'
      else:
        v = None
    if v:
      print ''.ljust(lead_width) + '- ' + v


#
# Make a packageManager to facilitate finding and loading package definitions:
#
vmgr = valet.packageManager.sharedPackageManager()

for pkgStr in args:
  #
  # Find any packages that match with the string the user provided.  Note
  # that this _can_ return multiple packages, for example if there are definition
  # files present with the same package id in multiple paths.
  #
  pkgList = None
  try:
    pkgList = vmgr.packagesWithId(pkgStr, targetContext)
  except Exception as e:
    print 'ERROR:  ' + str(e)
    sys.exit(22)

  if pkgList is not None:
    # Print header:
    print
    print 'Available versions in package (* = default version):'
    for pkg in pkgList:
      print ''
      print '[' + pkg.sourceFile() + ']'
      defaultVersion = pkg.defaultVersionId()

      #
      # Sort the versions by their id -- the rich versioning used by versionAtom will
      # aid in sorting the numbered components:
      #
      filteredVersions = sorted(pkg.filterVersions(byContext = targetContext), key = lambda v: v.id())

      if isClassicMode:
        #
        # Determine the length of the longest version id string present (plus 2):
        #
        maxVersionLength = 2 + max(map(lambda v:len(str(v.id().versionId())), filteredVersions))
      else:
        #
        # Same, but use the version-and-features string:
        #
        maxVersionLength = 2 + max(map(lambda v:len(v.id().versionAndFeatureString()), filteredVersions))
      #
      # valet/source#6: if package name is longer use its length
      #
      packageIdLength = 2 + len(str(pkg.id().packageId()))
      if packageIdLength > maxVersionLength:
        maxVersionLength = packageIdLength - 2

      #
      # Override shouldShowAttributes if the screen seems too small:
      #
      if shouldShowAttributes:
        if (tty_cols > 0 and (tty_cols - maxVersionLength - 4) <= 10):
          shouldShowAttributes = False
    
      #
      # Print the package id and description:
      #
      print str(pkg.id().packageId()).ljust(maxVersionLength + 2) + subEmptyStrForNone(pkg.description())
      if shouldShowAttributes and not shouldCoalesceAttributes:
        printAttributes(pkg.attributes(), maxVersionLength + 2, tty_cols - maxVersionLength - 2)
      #
      # For each version…
      #
      for vers in filteredVersions:
        leadin = '* ' if vers.id() == defaultVersion else '  '
        if isClassicMode:
          if isinstance(vers, valet.packageVersionAlias):
            print leadin + str(vers.id().versionId()).ljust(maxVersionLength) + 'alias to ' + str(vers.aliasTo())
          else:
            print leadin + str(vers.id().versionId()).ljust(maxVersionLength) + subEmptyStrForNone(vers.description())
          features = vers.id().features()
          if features is not None:
            for feature in features:
              print ''.ljust(maxVersionLength + 2) + '+ ' + str(feature)
        else:
          if isinstance(vers, valet.packageVersionAlias):
            print leadin + vers.id().versionAndFeatureString().ljust(maxVersionLength) + 'alias to ' + str(vers.aliasTo())
          else:
            print leadin + vers.id().versionAndFeatureString().ljust(maxVersionLength) + subEmptyStrForNone(vers.description())
        if shouldShowAttributes:
          printAttributes(
              vers.coallescedAttributes() if shouldCoalesceAttributes else vers.attributes(),
              maxVersionLength + 2,
              tty_cols - maxVersionLength - 2)

  else:
    print 'ERROR:  No package with id "{0}"'.format(pkgStr)
    sys.exit(1)
