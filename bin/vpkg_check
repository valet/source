#!/usr/bin/python -E
# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

#
# Setup the path to the VALET install and import it.  The assumption
# here is that if VALET_PREFIX is _not_ set in the environment, the
# system has VALET installed in one of its default Python module
# containers.
#
import os
import sys

valetPrefix = os.getenv('VALET_PREFIX')
if valetPrefix:
  valetLibPath = os.path.join(valetPrefix, 'lib', 'python')
  if valetLibPath not in sys.path:
    sys.path.insert(1, valetLibPath)

import valet

#
# Process CLI arguments:
#
import argparse

parser = argparse.ArgumentParser(
        description = 'Attempt to parse the VALET package definition file at the given path.',
        epilog='If unsuccessful the warning and error message stacks are displayed and a non-zero exit code is returned.')
parser.add_argument(
        '--verbose', '-v',
        dest='isVerbose',
        action='store_true',
        help='increase the amount of output the program generates')
parser.add_argument(
        'pkgDefFile',
        metavar='<pkg-definition-file>',
        help='one or more package ids')
args = parser.parse_args()

#
# Process the options we were presented:
#
if args.isVerbose:
  os.environ['VALET_VERBOSE'] = 'yes'
pkgDefFile = args.pkgDefFile

#
# Main program:
#

#
# Make a packageManager to facilitate finding and loading package definitions:
#
vmgr = valet.packageManager.sharedPackageManager()

thePkg = vmgr.checkPackageAtPath(pkgDefFile)
if thePkg:
  print pkgDefFile + ' is OK'
  print
  print thePkg.infoString()
  print
  sys.exit(0)
sys.exit(1)
