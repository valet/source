
  About VALET...

     VALET is a recursive acronym for VALET Automates Linux Environment Tasks.  It is an alternative to the environment modules software (http://modules.sourceforge.net) and strives to be as simple as possible to configure and to use, while adding features not present in the environment modules software.  VALET is written primarily in Python; some supporting pieces are (naturally) written in the language of the shells that are supported.


  About package ids:

    A versioned package is identified by a package identifier and a version identifer separated by a forward slash:

        matlab           Default version of matlab

        matlab/default   Default version of matlab

        matlab/R2007a    R2007a version of matlab

    An optional set of feature tags can be present:

        openmpi/1.10.5:intel2017,lustre

    VALET will attempt to find a version of the package that possesses those features.  (A configuration option for VALET determines which of multiple matches should be used:  the first found, the one with the most additional features, or the one with the least additional features).


  About the commands:

    vpkg_list

      List the available packages

    vpkg_versions <pkgId>

      List the versions available for a given package

    vpkg_info <pkgId|versPkgId>

      Show information for a package (or a specific version of a package)

    vpkg_require <pkgIdversPkgId> {<pkgIdversPkgId>}

      Add configuration for the given packages to the environment

    vpkg_devrequire <pkgIdversPkgId> {<pkgIdversPkgId>}

      Add configuration for the given packages to the environment; includes setup of the LDFLAGS and CPPFLAGS variables with library/header paths for the packages

    vpkg_rollback {#|all}

      Attempt to restore the environment state that existed before the last vpkg_require.  A numerical argument does multiple rollback operations, e.g. "vpkg_rollback 2" will revert the last two calls of "vpkg_require".  The argument "all" reverts every call to "vpkg_require" made in the shell.

    vpkg_replicate_parent

      After spawning a sub-shell (e.g. "bash") the rollback history of the parent shell is generally unavailable in the child.  Use this command to copy the complete rollback history of all parent shells into this shell's state directory.

    vpkg_history

      Display a list of all versioned packages that have been added to the environment.


  About package definition files:

    The first release of VALET relied on an XML file format to define packages and their versions.

    The second release of VALET added an augmented JSON syntax (hash-delimited comments were allowed) that was more compact.  Array- and dictionary-valued properties had a natural representation in JSON, as well, which made parsing much simpler.

    In the third release, the parsing engine has been overhauled and additional support for YAML and Python has been added.  The Python literal format allows for direct specification of the native Python structured data format that the parser engine expects when manufacturing objects.  Additionally, VALET can be configured to allow execution of Python code; if the contents of a Python package definition is not a literal, then the content is executed in an isolated context.  The Python code can create a package definition either as stuctured Python data (a'la the literal) or directly as VALET objects.

    For example, an executable Python package definition could include Python code to walk a directory containing versions of the software, each installed in a unique directory.  It could build the package definition on the fly using the directories it discovers.  In theory, the package definition file would never need to be updated:  simply creating a new subdirectory and installing the new software version therein would make it available on the system.


  For more information:

    See the "valet" man page.  There are also man pages for all of the VALET commands.

