
Use this directory for any configurational scripts that VALET will
be asked to source/exec.  When the package configuration uses a
relative path for a "script" object, the path is relative to this
directory.

