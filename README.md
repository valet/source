# VALET - source

This repository contains the following components:

  * the VALET Python libraries:
    * envsnapshot: creating and manipulating lists of shell environment entities (variables, functions, aliases)
    * valet: implementation of identifiers, package/version objects, definition file parsers, etc.
  * compiled utilities:
    * generation/parsing of "unique process id" values to tie VALET env snapshots to a specific running shell instance
    * package counts: web-based facility for cluster-wide tracking of `vpkg_require` invocations
  * scripts:
    * per-shell scripts for login (profile.d), generating environment configuration statements, creating/restoring env snapshots
    * VALET utilities:  `vpkg_list`, `vpkg_versions`, `vpgk_info`, `vpkg_check`, `valet_predicateTest`