# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
from packageVersion import *
from envVar import *

class packageVersionAlias(packageVersion):
  """Alias to a versioned package
  
A packageVersionAlias instance represents an alternate, unique versioned package identifier that exists as an alias to another version of the same package.  A packageVersionAlias is often used to associate a simplified version id with a floating, underlying version of the package:  for example "openmpi/1.4:gcc" as an alias to "openmpi/1.4.4:gcc", and as newer releases in the 1.4 series are added to the system the "1.4:gcc" alias is updated to resolve to the newer releases:  "openmpi/1.4.5:gcc," then "openmpi/1.4.6:gcc", etc.
  """
  
  def __init__(self, pkgVersId, parentPackage = None, aliasTo = None, ctxts = []):
    """Constructor
    
The versioned package id is mandatory.  The parent package is optional but must be set in order for the instance to be (eventually) valid.  Likewise, the aliasTo is optional but must eventually be set.

The parentPackage must be an instance of the package class.

The aliasTo should be an instance of the id class with a package atom that matches that of this instance.

An optional ctxts argument is permissible, too.
    """
    super(packageVersionAlias, self).__init__(pkgVersId=pkgVersId, parentPackage=parentPackage, ctxts=ctxts)
    self.setAliasTo(aliasTo)
    
  
  def ___str__(self):
    outStr = 'packageVersionAlias('
    outStr += str(self.id()) if self.id() is not None else '[unset id]'
    outStr += ' => '
    outStr += str(self._aliasTo) if self._aliasTo is not None else '[unset id]'
    contextStr = super(basePackage, self).__str__()
    if contextStr:
      outStr = outStr + ' ' + contextStr
    return outStr
    
  
  def __repr__(self):
    #
    # Most of the basePackage stuff is completely unused by a version alias,
    # so don't bother including it in our representation:
    #
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(basePackage, self).__repr__())
    #
    # Add our specific stuff:
    #
    if self.parentPackage():
      s += ', parentPackage=<<pkg "' + str(self.parentPackage().id()) + '">>'
    if self._aliasTo:
      s += ', aliasTo="' + str(self._aliasTo) + '"'
    return s + ')'


  def isPackageVersionAlias(self):
    """Return True to indicate that this is an alias to another versioned package id."""
    return True
  
  
  def isValid(self):
    """Returns True if the parent class indicates that this instance is valid AND this instance's aliasTo field is set."""
    if super(packageVersionAlias, self).isValid():
      #
      # Target of alias is required:
      #
      if self.aliasTo() is not None:
        return True
    return False
  
  
  def prefix(self):
    """Never, ever return a prefix for a packageVersionAlias."""
    return None
  
  
  def setPrefix(self, prefix):
    """We override setPrefix so that no prefix can be configured on a packageVersionAlias object."""
    super(packageVersionAlias, self).setPrefix(None)
  
  
  def setDescription(self, description):
    """We override setDescription so that no description can be configured on a packageVersionAlias object."""
    super(packageVersionAlias, self).setDescription(None)
    
  
  def setReferenceURL(self, referenceURL):
    """We override setReferenceURL so that no reference URL can be configured on a packageVersionAlias object."""
    super(packageVersionAlias, self).setReferenceURL(None)
  
  
  def setFlags(self, flags):
    """We override setFlags so that no flags can be set on a packageVersionAlias object."""
    super(packageVersionAlias, self).setFlags(None)
    
  
  def setFlag(self, flag):
    """We override setFlag so that no flags can be set on a packageVersionAlias object."""
    super(packageVersionAlias, self).setFlag(None)
  
  
  def setAttributeValue(self, attr_key, attr_value = None):
    """We override setAttributeValue so that no atrtributes can be set on a packageVersionAlias object."""
    pass

  def aliasTo(self):
    """Returns this instance's aliasTo."""
    return self._aliasTo
    
    
  def setAliasTo(self, aliasTo):
    """Set this instance's aliased version.  The aliasTo argument can be None, an id object, or a string.

If an id object is passed, then its packageId() must match with this instance's id's packageId(); otherwise, a ValueError exception is raised.  If the aliasTo refers to this instance itself, a ValueError exception is raised.

A string that has no slash (/) character is inferred to be version (and features) only, and will have the parent package's packageId() prefixed to it.  The string is then used to initialize an id object, which falls through to the tests against an id object mentioned above.

Any other type of value produces a ValueError exception.
    """
    if aliasTo is None:
      self._aliasTo = None
    else:
      if isinstance(aliasTo, basestring):
        # If the string has no /, then its relative to the parent package's package id:
        if not '/' in aliasTo:
          if self.parentPackage() is not None:
            aliasTo = str(self.parentPackage().id().packageId()) + '/' + aliasTo
          else:
            raise ValueError('packageVersionAlias.setAliasTo invalid string:  ' + aliasTo + '; parent package must be set OR the alias target must be a full-fledged id string')
        aliasTo = id(aliasTo)
        # Let this cascade into the next set of checks below...
        
      if isinstance(aliasTo, id):
        if str(self.id()) == str(aliasTo):
          raise ValueError('packageVersionAlias.setAliasTo:  an alias cannot point to itself {} == {}'.format(str(self.id()), str(aliasTo)))
        if self.id().packageId() == aliasTo.packageId():
          self._aliasTo = aliasTo
        else:
          raise ValueError('packageVersionAlias.setAliasTo expects a id argument with the same package id.')
      else:
        raise ValueError('packageVersionAlias.setAliasTo expects a string or id argument.')


  def resolvedVersion(self, exactMatch = False, fullDepth = True):
    """Attempt to resolve this instance's aliasTo package id and return the packageVersion object to which it refers.

If fullDepth is False, then only the object to which this instance directly refers is returned -- which could itself be an instance of packageVersionAlias.  If fullDepth is true (default) then all packageVersionAlias objects are resolved until a packageVersion is found (or an error occurs).

If exactMatch is True, then any time a version lookup is requested from the parent package ALL feature tags in the id must be matched exactly.  If exactMatch is False (default) then a version whose id contains all the requested feature tags AND additional feature tags is also an acceptable match.

A ValueError exception is raised if a resolution loop occurs or a non-existant target version is found.
    """
    target = self
    visited = [str(self.id())]
    while isinstance(target, packageVersionAlias):
      prevTarget = target
      target = target.parentPackage().getVersionWithId(target.aliasTo(), exactMatch)
      if target is not None:
        if not fullDepth:
          break
        if str(target.id()) in visited:
          raise ValueError('Loop in alias chain: ' + ' -> '.join(map(str, visited)) + ' -> ' + str(target.id()))
        visited.append(str(target.id()))
      else:
        raise ValueError('Non-existant version ' + str(prevTarget.aliasTo()) + ' in alias chain ' + ' -> '.join(map(str, visited)))
    return target


  def infoString(self):
    """Return a string describing this instance for the sake of vpkg_info."""
    #
    # Take our parent class's infoString and drop the trailing '}<NL>' characters:
    #
    s = super(packageVersionAlias, self).infoString()[:-2]
    #
    # Then add our own info:
    #
    s += '  alias-to: ' + str(self.aliasTo()) + os.linesep
    s += '}' + os.linesep
    return s
    

if __name__ == '__main__':
  
  p = package(pkgId = 'openmpi', description = 'Open MPI: Message Passing Interface', prefix = '/opt/shared/openmpi', defaultVersionId = '1.8.2', referenceURL = 'http://www.open-mpi.org/', actions = [
        envVar(variableName = 'MPICC', variableOp = 'set', newValue = 'mpicc', ctxts = ['development'])
      ])
  
  v = packageVersion(pkgVersId = '1.8.2', referenceURL = 'https://www.open-mpi.org/software/ompi/v1.8/')
  p.setFlag('no-crying')
  p.addVersion(v)
  
  print v
  print
  
  v = packageVersion(pkgVersId = 'openmpi/1.10.2-gcc-4.9.3')
  p.setFlag('no-crying')
  p.addVersion(v)
  
  v = packageVersionAlias(pkgVersId = 'openmpi/1.10.2', ctxts = ['standard', 'development'])
  v.setFlag(basePackage.shouldNotAddStandardPathsFlag)
  p.addVersion(v)
  v.setAliasTo('1.10.2-gcc-4.9.3')
  
  v = packageVersion(pkgVersId = '1.10.1:gcc,lustre')
  p.addVersion(v)
  
  v = packageVersion(pkgVersId = '1.10.1:gcc,lustre,puke,barf')
  p.addVersion(v)
  
  print p
  print
  print p.__repr__()
  print
  print p.infoString()
  print
  
  print p.filterVersions(byId='openmpi/=>1.8')
  print
  
  altV = p.getVersionAndRankWithId('1.10.1:gcc,barf,puke')
  if altV:
    print altV
    print
  
