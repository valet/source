# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import re

class atom(object):
  """Base representation of identifiers used within VALET
  
VALET package, version, and feature identifiers all share a common syntactical form.  The atom class parses, validates, and represents VALET identifier components.
  """

  #
  # Compiled regex
  #
  atomRegex = None


  @staticmethod
  def patternString():
    """Returns a string containing the regular expression that matches an identifier component."""
    return r"[A-Za-z0-9][A-Za-z0-9_.+-]*";
  
  
  @classmethod
  def compiledRegex(cls):
    """Returns a compiled re object that matches (anchored, full-length) an identifier component."""
    if cls.atomRegex is None:
      cls.atomRegex = re.compile("^" + cls.patternString() + "$")
    return cls.atomRegex
  

  def __init__(self, atomString):
    """Constructor

Validate the atomString as a valid identifier component and initialize the instance with that string.   Raises a ValueError exception if atomString is not valid.
    """
    s = atomString
    if self.compiledRegex().match(s) is None:
      raise ValueError('Invalid VALET id atom: "' + s + '"')
    self._value = s


  def __eq__(self, other):
    """Object equality

The RHS can be another atom instance or a string.  If the RHS is a string, it can optionally begin with a caret (^) indicating that the remainder of the string should be treated as a regular expression when matching against the instance value.

A ValueError exception is raised for any other RHS.
    """
    if isinstance(other, basestring):
      if other[0:1] == '^':
        if re.search(other[1:], self._value) is None:
          return False
        return True
      return self._value == other

    return self.__cmp__(other) == 0


  def __ne__(self, other):
    """Object inequality

Negated result of the equality operator.
    """
    if isinstance(other, basestring):
      if other[0:1] == '^':
        if re.search(other[1:], self._value) is None:
          return True
        return False
      return self._value != other

    return self.__cmp__(other) != 0


  def __cmp__(self, other):
    """Object comparator
    
Comparison against another atom object.  Returns negative on ordered less-than, positive on ordered greater-than, and zero on ordered same.
    """
    if self._value < other._value:
      return -1
    if self._value > other._value:
      return 1
    return 0


  def __str__(self):
    return self._value
    
    
  def __repr__(self):
    return self.__class__.__name__ + '("' + self._value + '")'


  def __hash__(self):
    return hash(self._value)


  def stringValue(self):
    """Returns the string value the instance wraps."""
    return self._value

  def encodedForEnvVar(self):
    """When setting environment variables named according to an identifier component, not all characters accepts for id components can be used.  This function returns a string for which characters not valid in an environment variable name are replaced:

  .     is replaced with    _DOT_
  -     is replaced with    _DASH_

The string is also folded to uppercase.  Thus, the identifier component:

  python-3.2

would become

  PYTHON_DASH_3_DOT_2
    """
    encForm = self._value.replace('.', '_DOT_');
    encForm = encForm.replace('-', '_DASH_');
    return encForm.upper()


#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  f1 = atom('romio-1')
  f2 = atom('roMio-2')
  f3 = atom('vtune')

  l = [f1,f3]
  print l
  print str(f2) + ' in list l = ' + str(f2 in l)
  print '^romio-[0-9]+$ in list l = ' + str('^^romio-[0-9]+$' in l)
