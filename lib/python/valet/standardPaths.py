# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
from enum import *
from config import *


class standardPaths(object):
  """Standard software installation paths
  
Helper class that enumerates and locates standard paths into which software is organized on *nix-like systems.  The known paths are configured in the config class (config.py).
  """

  #
  # Enumerate the kinds of paths we recognize:
  #
  pathKind = enum(['bin', 'lib', 'man', 'info', 'include', 'pkgConfig'])
  
  #
  # Map the enumerated path kinds to their default subpaths from the config class:
  #
  defaultPaths = {
            pathKind.bin: config.binPathDefaults,
            pathKind.lib: config.libPathDefaults,
            pathKind.man: config.manPathDefaults,
            pathKind.info: config.infoPathDefaults,
            pathKind.include: config.includePathDefaults,
            pathKind.pkgConfig: config.pkgConfigPathDefaults,
          }
  
  
  @classmethod
  def extantPathsWithPrefix(cls, kind, prefix):
    """Given a path kind (from the pathKind enumeration) iterate over the known default sub-paths and locate any -- relative to prefix -- that exist.  Returns a list containing the paths that are present.

A ValueError exception is raised if kind is not present in the pathKind enumeration.
    """
    if not kind in cls.pathKind:
      raise ValueError('Invalid path kind: ' + str(kind))
    if isinstance(kind, basestring):
      kind = cls.pathKind[kind]
    
    foundPaths = []
    for path in cls.defaultPaths[kind]:
      path = os.path.join(prefix, path)
      if os.path.isdir(path):
        foundPaths.append(path)
        # Check for architecture-specific ones?
        if config.shouldUseHardwareArchTags and config.hwArchTags:
          for archPath in config.hwArchTags:
            foundArchPath = os.path.join(path, archPath)
            if os.path.isdir(foundArchPath):
              foundPaths.append(foundArchPath)
      
    return foundPaths


if __name__ == '__main__':
  
  print standardPaths.extantPathsWithPrefix(standardPaths.pathKind.bin , '/usr')