# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
from config import *
from shellType import *
from action import *
from enum import *

## opType
#
# Integral and textual representations of the operations that can
# be applied to environment variables.
#
class opType(object):
  """Enumeration of the modification operations.  Includes additional string representations (and functions to map between them all)."""

  #
  # enumerate the operators:
  #
  opTypeEnum = enum(['set', 'unset', 'prepend', 'prependPath', 'prependSpace', \
                     'append', 'appendPath', 'appendSpace', 'scrub', 'scrubPath'])

  #
  # Token form of each operation; ordered to match the enumeration above:
  #
  _tokensForOps       = { 
                          opTypeEnum.set: '=',
                          opTypeEnum.unset: '!',
                          opTypeEnum.prepend: '+=',
                          opTypeEnum.prependPath: '+=[:]',
                          opTypeEnum.prependSpace: '+=[ ]',
                          opTypeEnum.append: '=+',
                          opTypeEnum.appendPath: '=+[:]',
                          opTypeEnum.appendSpace: '=+[ ]',
                          opTypeEnum.scrub: '-=',
                          opTypeEnum.scrubPath: '-=[:]'
                        }
  
  #
  # Mapping of textual string forms to the enumerated constants:
  #
  _stringToOpTypeMap  = {
                          'set': opTypeEnum.set,
                          'unset' : opTypeEnum.unset,
                          'prepend' : opTypeEnum.prepend,
                          'prepend-path' : opTypeEnum.prependPath,
                          'path-prepend' : opTypeEnum.prependPath,
                          'prepend-space' : opTypeEnum.prependSpace,
                          'prependpath' : opTypeEnum.prependPath,
                          'prependspace' : opTypeEnum.prependSpace,
                          'append' : opTypeEnum.append,
                          'append-path' : opTypeEnum.appendPath,
                          'path-append' : opTypeEnum.appendPath,
                          'append-space' : opTypeEnum.appendSpace,
                          'appendpath' : opTypeEnum.appendPath,
                          'appendspace' : opTypeEnum.appendSpace,
                          'scrub' : opTypeEnum.scrub,
                          'scrub-path' : opTypeEnum.scrubPath
                        }


  @classmethod
  def tokenForOpType(cls, opType):
    """Returns a short token representing one of the enumerated constants.  Returns None if opType is not in the enumeration."""
    if opType in cls._tokensForOps:
      return cls._tokensForOps[opType]
    return None

  
  @classmethod
  def opTypeForString(cls, opString):
    """Map a textual operator to its equivalent enumerated constant.  Returns None if opString is not a valid operator string."""
    cmpStr = opString.lower()
    if cmpStr in cls._stringToOpTypeMap:
      return cls._stringToOpTypeMap[cmpStr]
    return None


  @classmethod
  def valueIsRequired(cls, opType):
    return opType != cls.opTypeEnum.unset


class envVar(action):
  """Action to take with respect to an environment variable
  
Instance of this class represent changes to be made to an environment variable.  Each instance requires the variable name, the operation to perform, and (optionally) an operand value.
  """
  
  def __init__(self, variableName, variableOp = opType.opTypeEnum.set, newValue = None, ctxts = set()):
    """Constructor

The variableName is mandatory.  By default, the operation defaults to "set" with an empty operand.

Unlike some of the other action subclasses, the envVar has no setter methods:  all fields are initialized when the instance is instantiated.
    """
    super(envVar, self).__init__(ctxts)
    
    # Must have received a variable name:
    if variableName is None:
      raise ValueError('No variable name provided')
    self._variableName = variableName
    
    # Determine what operation to perform:
    if isinstance(variableOp, basestring):
      self._variableOp = opType.opTypeForString(variableOp)
      if self._variableOp is None:
        raise ValueError('Invalid opType: ' + variableOp)
    elif variableOp in opType.opTypeEnum:
      self._variableOp = variableOp
    else:
      raise ValueError('Invalid opType: ' + str(variableOp))

    # Ensure a value was presented when necessary:
    if opType.valueIsRequired(self._variableOp) and newValue is None:
      raise ValueError('An accompanying value is required for the "' + opType.opTypeEnum.asString(self._variableOp) + '" operation');
    self._newValue = str(newValue) if newValue is not None else None
  
  
  def clone(self):
    return envVar(self._variableName, self._variableOp, self._newValue, self.contexts())
      

  def __str__(self):
    outStr = self._variableName + opType.tokenForOpType(self._variableOp)
    if self._newValue is not None:
      outStr += self._newValue
    contextStr = super(envVar, self).__str__()
    if contextStr:
      outStr = outStr + ' ' + contextStr
    return outStr


  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(envVar, self).__repr__())
    return s + ', variableName="' + self._variableName + '", variableOp=' + str(self._variableOp) + \
              ', newValue="' + self._newValue.replace('"','\\"') + '")'


  def __hash__(self):
    return hash(self._variableName)


  def __cmp__(self, other):
    """Object comparator

If other is NOT an envVar instance the comparison always orders instance > other.

Field comparison is ordered by variable name, then operator, and finally the new value involved.
    """
    if not isinstance(other, envVar):
      return +1
    if self.variableName() < other.variableName():
      return -1
    if self.variableName() == other.variableName():
      opDiff = self.variableOp() - other.variableOp()
      if opDiff == 0:
        return self.newValue().__cmp__(other.newValue())
      return opDiff
    return +1
    
    
  def __lt__(self,other):
    """Object less-than"""
    if not isinstance(other, envVar):
      return False
    return (self.__cmp__(other) < 0)
    
    
  def __le__(self,other):
    """Object less-than equal-to"""
    if not isinstance(other, envVar):
      return False
    return (self.__cmp__(other) <= 0)
    
    
  def __gt__(self,other):
    """Object greater-than"""
    if not isinstance(other, envVar):
      return True
    return (self.__cmp__(other) > 0)
    
    
  def __ge__(self,other):
    """Object greater-than equal-to"""
    if not isinstance(other, envVar):
      return True
    return (self.__cmp__(other) >= 0)
    
    
  def __eq__(self,other):
    """Object equality"""
    if not isinstance(other, envVar):
      return False
    return (self.__cmp__(other) == 0)
    
    
  def __ne__(self,other):
    """Object inequality"""
    if not isinstance(other, envVar):
      return True
    return (self.__cmp__(other) != 0)


  def variableName(self):
    """Return the environment variable name associated with this instance."""
    return self._variableName


  def variableOp(self):
    """Return this instance's modification operation."""
    return self._variableOp
    

  def newValue(self):
    """Return this instance's operand value."""
    return self._newValue
    

  def actionStringWithShellHelper(self, shellHelper, currentContext = contexts.all):
    """Return a configuration fragment string that affects this instance's change to an environment variable.  The provided shellHelper should be used for as much shell-specific work as possible."""
    variable = self.variableName()
    value = self.newValue()
    op = self.variableOp()
    
    # Promote an atomic currentContext to being a set:
    if isinstance(currentContext, basestring):
      currentContext = set(currentContext)
    
    if currentContext in self.contexts:
      if op == opType.opTypeEnum.set:
        return shellHelper.setValueOfEnvVar(variable, value, True)
      elif op == opType.opTypeEnum.unset:
        return shellHelper.unsetEnvVar(variable)
      elif op == opType.opTypeEnum.prepend:
        return shellHelper.prependValueToEnvVar(variable, value, '', True)
      elif op == opType.opTypeEnum.prependPath:
        return shellHelper.prependValueToEnvVar(variable, value, ':', True)
      elif op == opType.opTypeEnum.prependSpace:
        return shellHelper.prependValueToEnvVar(variable, value, ' ', True)
      elif op == opType.opTypeEnum.append:
        return shellHelper.appendValueToEnvVar(variable, value, '', True)
      elif op == opType.opTypeEnum.appendPath:
        return shellHelper.appendValueToEnvVar(variable, value, ':', True)
      elif op == opType.opTypeEnum.appendSpace:
        return shellHelper.appendValueToEnvVar(variable, value, ' ', True)
      elif op == opType.opTypeEnum.scrub:
        return variable + '=${' + variable + '//' + value + '/:}; export ' + variable + ';'
      elif op == opType.opTypeEnum.scrubPath:
        return variable + '=scrubPath \'' + shellHelper.escapeString(value, False) + '\'; export ' + variable + ';'
    return ''
  
  
#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  print opType._stringToOpTypeMap
  print

  x = envVar('PYTHONPATH', opType.opTypeEnum.prependPath, '"/usr/lib64/python"', [contexts.standard])
  print x
  print contexts.development in x.contexts
  print

  y = envVar('CC', opType.opTypeEnum.set, 'icc')
  print y
  print contexts.development in y.contexts
  print

  l = (x, y)
  print l
