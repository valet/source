# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
import re
from id import *
from packageManager import *

class history(object):
  """The history class aids in maintaining the VALET_HISTORY environment variable.  The VALET_HISTORY environment represents each vpkg_require transaction as:


    {[<context>]}<versPkgId>{;<versPkgId>;..}


Multiple vpkg_require transactions are delimited by the vertical bar character (|):


    [<context1>]<versPkgId1>;<versPkgId2>|[<context2>]<versPkgId3>;<versPkgId4>


  """

  
  def __init__(self, currentContext = contexts.standard):
    self._newPackages = []
    self._newPackageIds = []
    self._visitedPackages = []
    self._visitedPackageIds = []
    self._currentContext = currentContext

    # Load the package history from the environment:
    if 'VALET_HISTORY' in os.environ:
      for pkgAdd in os.environ['VALET_HISTORY'].split('|'):
        #
        # Check for a context marker at the head of the transaction:
        #
        m = re.match(r'^\[([^\]]+)\](.*)$', pkgAdd)
        if m:
          if ',' in m.group(1):
            contextWhenAdded = set(m.group(1).split(','))
          else:
            contextWhenAdded = m.group(1)
          pkgAdd = m.group(2)
        else:
          contextWhenAdded = None
        
        #
        # Split versioned package ids on the semi-colon delimiter:
        #
        for pkgId in pkgAdd.split(';'):
          #
          # Wrap the package id:
          #
          try:
            pkgId = id(pkgId)
          except Exception as e:
            raise RuntimeError('Unable to parse package id from VALET_HISTORY: ' + pkgId)
          
          #
          # Load the package:
          #
          try:
            if contextWhenAdded:
              thePkg = packageManager.sharedPackageManager().packageVersionWithId(pkgId, contextWhenAdded)
            else:
              thePkg = packageManager.sharedPackageManager().packageVersionWithId(pkgId)
          except Exception as e:
            raise RuntimeError('Unable to load package from VALET_HISTORY: ' + str(pkgId) + ': ' + str(e))
            
          if thePkg:
            #
            # Resolve a version alias if necessary:
            #
            if thePkg.isPackageVersionAlias():
              try:
                thePkg = thePkg.resolvedVersion()
              except Exception as e:
                raise RuntimeError('Unable to resolve version alias from VALET_HISTORY: ' + str(pkgId))
            #
            # Add to the list:
            #
            self._visitedPackages.append(thePkg)
            self._visitedPackageIds.append(thePkg.id())
          else:
            raise RuntimeError('Package ' + str(pkgId) + ' has been visited but is not defined.')


  def visitedPackages(self):
    return self._visitedPackages
    

  def newPackages(self):
    return self._newPackages
    
  
  def currentContextAsEnvString(self):
    if self._currentContext is None:
      return '';
    if isinstance(self._currentContext, basestring):
      return self._currentContext;
    return ','.join(self._currentContext)


  def newHistoryString(self):
    if len(self._newPackageIds) > 0:
      if 'VALET_HISTORY' in os.environ:
        return os.environ['VALET_HISTORY'] + '|[' + self.currentContextAsEnvString() + ']' + ';'.join(map(lambda p:str(p), self._newPackageIds))
      else:
        return '[' + self.currentContextAsEnvString() + ']' + ';'.join(map(lambda p:str(p), self._newPackageIds))
    return os.environ['VALET_HISTORY'] if 'VALET_HISTORY' in os.environ else ''


  def addPackageId(self, aPackageId, aLabel='package', shouldForceSuccess=False, shouldForceLoad=False, shouldLoadDependenciesOnly = False):
    try:
      if isinstance(aPackageId, id) or isinstance(aPackageId, atom):
        aPackageId = str(aPackageId)
        
      if not isinstance(aPackageId, basestring):
        raise ValueError('invalid package id (unsupported object type): ' + str(type(aPackageId)))
      
      pkg = None
      if '/' in aPackageId:
        pkg = packageManager.sharedPackageManager().packageVersionWithId(aPackageId, self._currentContext)
      else:
        pkgs = packageManager.sharedPackageManager().packagesWithId(aPackageId, self._currentContext)
        if pkgs:
          pkg = pkgs[0].defaultVersion()
          
      if pkg is None:
        errStr = 'Unable to find package: ' + aPackageId + os.linesep
        for w in packageManager.sharedPackageManager().warningStrings():
          errStr = errStr + 'W ' + w + os.linesep
        for e in packageManager.sharedPackageManager().errorStrings():
          errStr = errStr + 'E ' + e + os.linesep
        raise RuntimeError(errStr)
      else:
        self.addPackage(pkg, aLabel, shouldForceSuccess, shouldForceLoad, shouldLoadDependenciesOnly)
    except Exception as e:
      raise e
      

  def addPackage(self, aPackage, aLabel='package', shouldForceSuccess=False, shouldForceLoad=False, shouldLoadDependenciesOnly = False):
    # Resolve aliases now:
    if aPackage.isPackageVersionAlias():
      aPackage = aPackage.resolvedVersion()
    
    incomingId = aPackage.id()
    if not shouldForceLoad:
      # Check to be sure the target package has not been loaded:
      if incomingId in self._newPackageIds or incomingId in self._visitedPackageIds:
        if config.isVerbose():
          self._newPackages.append('OK: ' + aLabel + ' `' + str(incomingId) + '` already loaded in environment')
        return
    
    if not shouldForceLoad and incomingId.packageId() in map(lambda pId:pId.packageId(), self._visitedPackageIds + self._newPackageIds):
      # A versioned package with the same package id as aPackage has already
      # been loaded:
      for otherId in self._visitedPackageIds + self._newPackageIds:
        if otherId.packageId() == incomingId.packageId() and not shouldForceSuccess:
          if otherId.versionId() != incomingId.versionId():
            # There's already a package with the same id loaded, but it's
            # a different version:
            raise RuntimeError(str(incomingId) + ' conflicts with ' + str(otherId) + ' already added to environment')
          elif not otherId.featureCompatible(incomingId):
            # The existing package in the environment does not have
            # all the features that the incoming package has:
            raise RuntimeError(str(incomingId) + ' is not feature-comptible with ' + str(otherId) + ' already added to environment')
          # A compatible package already exists in the environment, so
          # just return without adding it to the list:
          if config.isVerbose():
            if str(incomingId) == str(otherId):
              self._newPackages.append('OK: using ' + aLabel + ' ' + str(otherId) + ' already loaded in the environment')
            else:
              self._newPackages.append('OK: using ' + aLabel + ' ' + str(otherId) + ' already loaded in the environment (compatible with ' + str(incomingId) + ')')
          return
    else:
      # Process id dependencies:
      for dependency in aPackage.coallescedDependenciesIterator():
        if isinstance(dependency, basestring) or isinstance(dependency, id):
          if dependency in self._newPackageIds or dependency in self._visitedPackageIds:
            pass
          else:
            try:
              otherPkg = packageManager.sharedPackageManager().packageVersionWithId(dependency, self._currentContext)
              if otherPkg is not None:
                self.addPackage(otherPkg, 'dependency', shouldForceSuccess, shouldForceLoad, False)
              else:
                errStr = str(incomingId) + ' unsatisfied dependency ' + str(dependency) + os.linesep
                for w in packageManager.sharedPackageManager().warningStrings():
                  errStr = errStr + 'W ' + w + os.linesep
                for e in packageManager.sharedPackageManager().errorStrings():
                  errStr = errStr + 'E ' + e + os.linesep
                if not shouldForceSuccess:
                  raise RuntimeError(errStr)
                else:
                  self._newPackages.append('WARNING: ' + errStr)
            except Exception as e:
              if not shouldForceSuccess:
                raise e
              else:
                self._newPackages.append('WARNING: ' + str(e))

      # Check the incoming package for any id/regex incompatibilies:
      if not shouldForceSuccess:
        for incompat in aPackage.coallescedIncompatibilitiesIterator():
          if isinstance(incompat, basestring) or isinstance(incompat, id):
            if incompat in self._newPackageIds or incompat in self._visitedPackageIds:
              raise RuntimeError(str(incomingId) + ' incompatibility: ' + str(incompat))

      # Check previously-added packages for incompatibilities with the incoming package:
      if not shouldForceSuccess:
        for pkg in self._visitedPackages:
          for incompat in pkg.coallescedIncompatibilitiesIterator():
            if isinstance(incompat, basestring) or isinstance(incompat, id):
              if incompat in self._newPackageIds or incompat in self._visitedPackageIds:
                raise RuntimeError(str(pkg.id()) + ' incompatibility: ' + str(incompat))
      
      if not shouldLoadDependenciesOnly:
        if config.isVerbose():
          self._newPackages.append('OK:  adding ' + aLabel + ' ' + str(incomingId))
        aPackage.setLabel(aLabel)
        self._newPackages.append(aPackage)
        self._newPackageIds.append(incomingId)
