# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
import re
from config import config
from enum import *
from atom import atom
from versionAtom import versionAtom

#
# Enumerates the id components for the sake of the merge() method
#
idMergeFields = enum({1:'package', 2:'version', 4:'features', 6:'versionAndFeatures', 7:'all'})


class id(object):
  """VALET versioned package identifier

A VALET versioned package identifier consists of the following components:

  [mandatory] Package identifier
  [optional]  Version identifier
  [optional]  List of feature tags

Lacking the version and feature components, the instance represents a VALET package.  A version of a package must have an id that defines a version component, as well.
  """


  #
  # The idRegex gets filled-in by the compiledIdRegex() class method
  # when first called.  It contains a compiled regular expression to
  # match against a versioned package identifier.
  #
  idRegex = None
  
  @classmethod
  def compiledIdRegex(cls):
    """Returns a compiled re object that matches a full VALET identifier -- package, version, and feature tags.  The match will produce the following parenthesized groups (by index):

  1         package component
  3         version component
  5         feature list component
    """
    if cls.idRegex is None:
      baseString = atom.patternString()
      regexString = "^(" + baseString + ")(/(\*|" + baseString + "))?(:((" + baseString + ")(,(" + baseString + "))*)?)?$"
      cls.idRegex = re.compile(regexString)
    return cls.idRegex
  

  #
  # The cmpRegex gets filled-in by the compiledCmpRegex() class method
  # when first called.  It contains a compiled regular expression to
  # match against a versioned package comparator/identifier comprised
  # of package/version/feature components that are static strings,
  # regular expressions, or (for the version) version predicates.
  #
  cmpRegex = None
  
  @classmethod
  def compiledCmpRegex(cls):
    """Returns a compiled re object that matches a VALET identifier -- package, version, and feature tags -- that can optionally include regexes and version predicates (for the sake of comparison operations).  The match will produce the following parenthesized groups (by index):

  1         package component
  3         version component
  5         feature list component
    """
    if cls.cmpRegex is None:
      baseString = atom.patternString()
      regexString = "^(\^[^/:]+|" + baseString + ")(/(\*|(\^|=)[^:]+|" + baseString + "))?(:((\^[^,]+|" + baseString + ")(,(\^[^,]+|" + baseString + "))*)?)?$"
      cls.cmpRegex = re.compile(regexString)
    return cls.cmpRegex
    

  def __init__(self, idString):
    """Constructor

Raises a ValueError exception if the idString does not have the appropriate syntax.
    """
    m = self.compiledIdRegex().match(idString)
    if m is None:
      raise ValueError('Invalid VALET id: "' + idString + '"')
    
    try:
      self._packageId = atom(m.group(1))
        
      if m.group(3) is not None and m.group(3) != '*':
        self._versionId = versionAtom(m.group(3))
      else:
        self._versionId = ''
        
      self._features = set()
      if m.group(5):
        self._features.update(map(lambda f:atom(f), m.group(5).split(',')))
    except:
      raise


  def __eq__(self, other):
    """Object equality

If RHS is also a id, then it must have the same exact package and version id atoms and the exact same set of features.

If RHS is a string, then it is broken-down into package, version, and feature atoms.  Any atom that starts with a caret (^) is instead considered to be a regular expression, and as such regex pattern matching is used for it versus simple string comparison.  If the version component starts with an equals sign (=) it is considered as a version predicate string.

Raises a ValueError exception if the RHS is anything else.
    """
    if isinstance(other, basestring):
      return self.__cmp__(other) == 0

    if isinstance(other, id):
      if self._packageId != other.packageId():
        return False
      if self._versionId != other.versionId():
        return False
      if self._features == other.features():
        return True

    return False


  def __ne__(self, other):
    """Object inequality

Negated result of the equality operator.
    """
    return not self.__eq__(other)

  ## id __cmp__
  #
  # Compare a id to another id.  Negative if ascending,
  # zero if same, positive if descending.
  #
  def __cmp__(self, other):
    """Object comparator

Compare this id instance against another, returning negative, positive, or zero when this instance is less-than, greater-than, or equal to the other, respectively.

If other is a string, then the function attempts to break it into package, version, and feature component comparators and use those substrings in __cmp__ invocations against those components of this instance.

Any other value for the RHS produces a ValueError exception.
    """
    if isinstance(other, id):
      if self._packageId == other.packageId():
        if self._versionId == other.versionId():
          if self._features == other.features():
            return 0;
          else:
            f1 = ','.join(map(str, self._features))
            f2 = ','.join(map(str, other.features()))
            if f1 < f2:
              return -1
            return 1
        else:
          if self._versionId < other.versionId():
            return -1
          return 1
      else:
        if self._packageId < other.packageId():
          return -1
        return 1

    if isinstance(other, basestring):
      m = self.compiledCmpRegex().match(other)
      if m is not None:
        if self._packageId != m.group(1):
          return -1

        if m.group(3) is None:
          if self._versionId != '':
            return -1
        else:
          if self._versionId != m.group(3):
            return -1
        
        if m.group(6) is not None:
          if self._features is None:
            return -1
          found = list()
          wants = m.group(6).split(',')
          for f in wants:
            if not f in self._features:
              return -1
            if not f in found:
              found.append(f)
          if len(found) != len(wants):
            return -1
        return 0
        
    raise ValueError('Invalid VALET id comparison: "' + str(other) + '"')


  def __str__(self):
    s = str(self._packageId)
    if self._versionId != '':
      s = s + '/' + str(self._versionId)
      if len(self._features):
        s = s + ':' + ','.join(map(str, self._features))
    return s
  

  def __repr__(self):
    s = self.__class__.__name__ + '("' + str(self._packageId)
    if self._versionId != '':
      s = s + '/' + str(self._versionId)
      if len(self._features):
        s = s + ':' + ','.join(map(str, self._features))
    return s + '")';


  def __hash__(self):
    return hash(str(self));


  def packageId(self):
    """Returns this instance's package component."""
    return self._packageId


  def versionId(self):
    """Returns this instance's version component."""
    return self._versionId
  
  def versionAndFeatureString(self):
    """Returns a string comprised of this instance's version component with any features appended."""
    if len(self._features) > 0:
      return str(self._versionId) + ':' + ','.join(map(lambda f:str(f), sorted(self._features)))
    return str(self._versionId)

  def features(self):
    """Returns this instance's set of feature tags."""
    return self._features
  
  def hasFeature(self, feature):
    """Returns True is this instance contains the given feature tag."""
    return feature in self._features
  
  def hasFeatures(self, features):
    """Returns True is this instance contains all the feature tags in the features list/set/tuple."""
    if len(features) > 0:
      for feature in features:
        if not feature in self._features:
          return False
      return True
    return False
  
  def addFeature(self, feature):
    """Turn feature into an atom and add that atom to this instance's set of features.
    
Raises a ValueError exception if feature is a string and is not a valid atom, or if feature is neither a string nor an atom instance.
    """
    if isinstance(feature, basestring):
      if atom.compiledIdRegex().match(feature) is None:
        raise ValueError('Attempt to add invalid feature tag: ' + feature)
      feature = atom(feature)
    if isinstance(feature, atom):
      self._features.add(feature)
    else:
      raise ValueError('An id feature must be an atom or a string')
      

  def samePackage(self, other):
    """Returns True iff this instance's package idenfier matches that of the other id.

If other is a string rather than a id instance, the full string must match the instance's package identifier.  If the string has a caret (^) prefix then it is interpreted as a regular expression and is matched accordingly.
    """
    if isinstance(other, basestring):
      m = self.compiledCmpRegex().match(other)
      if m is not None:
        if m.group(1) is None or self._packageId != m.group(1):
          return False
        return True
      return False

    if isinstance(other, id):
      return self._packageId == other.packageId()

    raise ValueError('Invalid package id check, id vs. ' + type(other))

  ## sameVersion
  #
  # 
  #
  def sameVersion(self, other):
    """Returns True iff this instance's package and version idenfiers match those of the other id.

If other is a string rather than a id instance, the full string must match the instance's package and version identifiers.  Any atom in the string having a caret (^) prefix is interpreted as a regular expression and is matched accordingly.  Likewise, the version may use a version predicate (delimited by an initial equal sign character).
    """
    if isinstance(other, basestring):
      m = self.compiledCmpRegex().match(other)
      if m is not None:
        if m.group(1) is None or self._packageId != m.group(1):
          return False

        if m.group(3) is None:
          if self._versionId:
            return False
        else:
          if not self._versionId:
            return False
          if self._versionId != m.group(3):
            return False
        return True
      return False

    if isinstance(other, id):
      return self._versionId == other.versionId()
      
    raise ValueError('Invalid version id check, id vs. ' + type(other))


  def featureCompatible(self, other):
    """Returns True iff the instance's package and version idenfiers match those of the 'other' id and the instance has all of the feature tags specified by other.

If other is a string rather than a id instance, the full string must match the instance's package and version identifiers; the instance must have all of the feature tags specified in other.  Any atom in the string having a caret (^) prefix is interpreted as a regular expression and is matched accordingly.  Likewise, the version may use a version predicate (delimited by an initial equal sign character).
    """
    if isinstance(other, basestring):
      m = self.compiledCmpRegex().match(other)
      if m is not None:
        if m.group(1) is None:
          return False
        if self._packageId != m.group(1):
          return False

        if m.group(3) is None:
          if self._versionId is not None:
            return False
        else:
          if not self._versionId:
            return False
          if self._versionId != m.group(3):
            return False

        if m.group(6) is None:
          # No features?  We're a match for that:
          return True
        
        # Parse feature list into a set:
        try:
          altF = set(map(lambda f:atom(f), m.group(6).split(',')))
          # The "other" string contained plain feature names, so
          # a simple set operator decides it:
          return altF.issubset(self._features)
        except:
          # The other string may contain regex patterns, so test as
          # a sequence of strings:
          for testFeature in m.group(6).split(','):
            isPresent = False
            for myFeature in self._features:
              if myFeature == testFeature:
                isPresent = True
            if not isPresent:
              return False
          return True
        
      return False

    if isinstance(other, id):
      if self._packageId != other.packageId():
        return False

      altV = other.versionId()
      if altV is None:
        if self._versionId is not None:
          return False
      else:
        if not self._versionId:
          return False
        if self._versionId != altV:
          return False
      
      # If other's features are a subset of our own, we are
      # compatible:
      return other.features().issubset(self._features)

    raise ValueError('Invalid feature check, id vs. ' + str(type(other)))


  def merge(self, fields, other, otherFields):
    """Create a new id by combining fields from this instance and other.

The fields and otherFields bitmasks indicate which fields to use from this instance versus other.  The bits are enumerated in idMergeFields and can be combined using a bitwise OR.

By preference the package and version components are taken from other if those bits are set in otherFields.  Both fields and otherFields may have the features bit set, in which case features from both this instance and other are combined.

To combine the package from the "pv1" instance with the version and features of the "pv2" instance:

   pv3 = pv1.merge(idMergeFields.package, pv2, idMergeFields.versionAndFeatures)
    """
    if otherFields & idMergeFields.package:
      s = str(other.packageId())
    elif fields & idMergeFields.package:
      s = str(self.packageId())
    else:
      raise RuntimeError('No package field included in merge.')
    s += '/'
    if otherFields & idMergeFields.version:
      s += str(other.versionId())
    elif fields & idMergeFields.version:
      s += str(self.versionId())
    else:
      raise RuntimeError('No version field included in merge.')
    if (fields | otherFields) & idMergeFields.features:
      features = set()
      if self._features is not None and fields & idMergeFields.features:
        features.update(self._features)
      if other.features() is not None and otherFields & idMergeFields.features:
        features.update(other.features())
      if len(features) > 0:
        s += ':' + ','.join(map(lambda f:str(f), features))
    return id(s)


  def packageOnlyId(self):
    """Return a new instance of id which is initialized using this instances package id component alone."""
    return id(str(self.packageId()))
  
  
  def pathRelativeToPrefix(self, prefix):
    """Construct a path relative to the given prefix that embodies the version id plus any feature tags.

Note that if hardware architecture tags are enabled and the [prefix]/[version] directory does not exist, the most-specific hardware architecture tag may be found, e.g. [prefix]/[hwarch]/[version] instead.  If none of them exist, then the fallback is to return the [prefix]/[version] even though it doesn't exist.
    """
    if self._versionId is not None:
      subPath = str(self._versionId)
      if config.shouldAddFeatureTagsToVersionPrefixes and self._features:
        subPath += config.autoGeneratedPrefixFeatureTagDelimiter + config.autoGeneratedPrefixFeatureTagDelimiter.join(map(lambda f:str(f), sorted(self._features)))
      fullPath = os.path.join(prefix, subPath)
      if not os.path.isdir(fullPath) and config.shouldUseHardwareArchTags and config.hwArchTags:
        for archPath in reversed(config.hwArchTags):
          fullArchPath = os.path.join(prefix, archPath, subPath)
          if os.path.isdir(fullArchPath):
            return fullArchPath
      return fullPath
    raise ValueError('Cannot form path for non-versioned id.')

#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  pkgs = [
            id('openmpi/1.4.4-intel:64bit'),
            id('openmpi/1.5.1-intel:64bit'),
            id('openmpi/1.6.3-intel:64bit'),
            id('openmpi/1.6.3-intel:64bit,romio'),
            id('openmpi/1.7.2-intel:64bit,romio'),
            id('python/2.7.2-intel'),
            id('python/2.7.2:intel'),
            id('python/2.7.2-gcc'),
            id('python/3.0.1-gcc'),
            id('intel/2013-sp1:64bit'),
            id('intel/2013-sp1:32bit'),
            id('openmpi/1.4.4-intel:32bit'),
            id('openmpi/1.5.1-intel:32bit'),
            id('openmpi/1.6.3-intel:32bit'),
            id('python/2.6.5-gcc'),
            id('python/2.6.5-intel'),
            id('python/2.6.5:intel'),
            id('openmpi/1.6.3-intel:32bit,romio'),
            id('openmpi/1.7.2-intel:32bit,romio'),
            id('openmpi/1.10.1-intel:64bit,romio')
          ];
  
  print 'id("' + str(pkgs[1]) + '").packageOnlyId() = ' + str(pkgs[1].packageOnlyId())
  print
  
  altId = pkgs[1].merge(idMergeFields.package | idMergeFields.features, pkgs[18], idMergeFields.versionAndFeatures)
  print str(pkgs[1]) + ' + ' + str(pkgs[18]) + ' = ' + str(altId)
  print
  
  print pkgs
  
  print 'Packages available:'
  for p in pkgs:
    print ' + ' + str(p)
  print
  
  print 'Sorted package list:'
  pkgs = sorted(pkgs)
  for p in pkgs:
    print ' + ' + str(p)
  print

  print 'Find an Open MPI >= 1.4 and < 1.7 (predicate "=>=1.4&<1.7"):'
  for p in pkgs:
    if p.featureCompatible('openmpi/=>=1.4&<1.7'):
      print ' + ' + str(p)
  print

  print 'Find an Open MPI >= 1.4 and < 1.7, with ROMIO (predicate "=>=1.4&<1.7"):'
  for p in pkgs:
    if p.featureCompatible('openmpi/=>=1.4&<1.7:romio'):
      print ' + ' + str(p)
  print

  print 'Find a Python 2.x for Intel (by version regex):'
  for p in pkgs:
    if p.featureCompatible('python/^^2\.([.0-9]+)-intel$'):
      print ' + ' + str(p)
  print

  print 'Find a Python 2.x for Intel (by version predicate plus feature regex):'
  for p in pkgs:
    if p.featureCompatible('python/==2:^[Ii]ntel'):
      print ' + ' + str(p)
  print

  print 'Find a Python 2.x (by version predicate alone):'
  for p in pkgs:
    if p.featureCompatible('python/==2'):
      print ' + ' + str(p)
  print

  print 'Find stable releases of Open MPI for Intel:'
  for p in pkgs:
    if p.featureCompatible('openmpi/^^[0-9]+\.[0-9]*[02468](\.([.0-9]+))*-intel$'):
      print ' + ' + str(p)
  print

  print 'Find stable releases of Open MPI for Intel, 32-bit:'
  for p in pkgs:
    if p.featureCompatible('openmpi/^^[0-9]+\.[0-9]*[02468](\.([.0-9]+))*-intel$:32bit'):
      print ' + ' + str(p)
  print

  print 'Find stable releases of Open MPI for Intel, 32-bit with ROMIO:'
  for p in pkgs:
    if p.featureCompatible('openmpi/^^[0-9]+\.[0-9]*[02468](\.([.0-9]+))*-intel$:32bit,romio'):
      print ' + ' + str(p)
  print
