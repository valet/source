# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

from configEntity import *
from shellType import *
from contexts import *


class action(configEntity):
  """Environment alteration action

Base class for all actions taken against the environment.
  """
  
  def clone(self):
    """Return a cloned copy of this instance of the class."""
    return action(self.contexts)
  
  def actionStringWithShellHelper(self, shellHelper, currentContext = contexts.all):
    """Concrete subclasses must implement this method.  This method should return a string containing the shell commands necessary to produce the represented action in the environment."""
    raise NotImplementedError('Concrete subclasses must implement the actionStringWithShellHelper method.')
    