# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

from contexts import contexts


class configEntity(object):
  """Configurational entity

An abstract base class for all VALET configurational objects.  Implements all common attributes.

Every configurational entity has a set of contexts in which it is enabled.  When a user interacts with VALET, s/he can provide a context for each command, which alters which packages are present, what versions of packages are present, and what configuration actions will be applied to the environment.

An entity's contexts is a public instance variable which can be accessed directly as entity.contexts.  For example, establishing than an entity is enabled for the 'foo' context:

  if 'foo' in theEntity.contexts:

  """
  
  
  def __init__(self, ctxts = []):
    """Constructor

Initialize the instance's contexts.  Subclasses should be sure to chain to this method.
    """
    if len(ctxts) > 0:
      self.contexts = contexts(ctxts)
    else:
      self.contexts = contexts(contexts.default)


  def __str__(self):
    if len(self.contexts) > 0:
      return '(contexts: ' + ','.join(self.contexts) + ')'
    return ''


  def __repr__(self):
    s = self.__class__.__name__ + '('
    if len(self.contexts) > 0:
      s += 'ctxts=' + self.contexts.__repr__()
    return s + ')'
