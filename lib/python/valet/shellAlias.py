# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

from config import *
from shellType import *
from action import *


class shellAlias(action):
  """Wrapper for a shell alias

Each instance of shellAlias wraps a named alias and a dictionary of strings, keyed by canonical shell name, which would be assigned to that name.  For example, the dictionary:

  { 'sh': 'ls -lh' }

for aliasName='lh' would produce the following configuration directive in Bourne and related shells:

  alias lh='ls -lh'
  """

  def __init__(self, aliasName, commandsByShell = {}, ctxts = []):
    """Constructor
    
An aliasName is mandatory.  The commandsByShell dictionary can be provided but is optional.

An optional ctxts argument is permissible, too.
    """
    super(shellAlias, self).__init__(ctxts)
    
    if not isinstance(aliasName, basestring) or aliasName == '':
      raise ValueError('Invalid aliasName')
    self._aliasName = aliasName
    self._commandsByShell = {}
    if commandsByShell is not None:
      for shell, command in commandsByShell.iteritems():
        if isinstance(command, basestring):
          self.setCommandForShell(shell, command)
  
  
  def clone(self):
    return shellAlias(self._aliasName, self._commandsByShell, self.contexts)


  def __str__(self):
    s = self._aliasName + ' => { ' + os.linesep
    for shell, command in self._commandsByShell.iteritems():
      s += '  ' + shellType.stringWithShellType(shell) + ' : ' + command + os.linesep
    s += '}'
    contextStr = super(shellAlias, self).__str__()
    if contextStr:
      s = s + ' ' + contextStr
    return s
  
  
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(shellAlias, self).__repr__())
    s += ', aliasName="' + self._aliasName.replace('"', '\\"') + '"'
    if self._commandsByShell is not None and len(self._commandsByShell):
      s += ', commandsByShell=' + self._commandsByShell.__repr__()
    return s + ')'


  def __hash__(self):
    """Instances of shellAlias hash by their aliasName."""
    return hash(self._aliasName)


  def __cmp__(self, other):
    """Object comparator

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return 0
    if self.aliasName() < other.aliasName():
      return -1
    if self.aliasName() == other.aliasName():
      return 0
    return +1
    
    
  def __lt__(self,other):
    """Object less-than

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return False
    return (self.aliasName() < other.aliasName())
    
    
  def __le__(self,other):
    """Object less-than equal-to

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return False
    return (self.aliasName() <= other.aliasName())
    
    
  def __gt__(self,other):
    """Object greater-than

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return True
    return (self.aliasName() > other.aliasName())
    
    
  def __ge__(self,other):
    """Object greater-than equal-to

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return True
    return (self.aliasName() >= other.aliasName())
    
    
  def __eq__(self,other):
    """Object equality

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return False
    return (self.aliasName() == other.aliasName())
    
    
  def __ne__(self,other):
    """Object inequality

Two instances of shellAlias are compared based upon their aliasName field alone.
    """
    if not isinstance(other, shellAlias):
      return True
    return (self.aliasName() != other.aliasName())


  def aliasName(self):
    """Returns the instane's aliasName."""
    return self._aliasName


  def commandForShell(self, shell):
    """Given a shell type -- by string or by enumerated integer value -- returns the command string that the shell in question would associate with the instance's aliasName.
    
Returns None if there is nothing defined for the shell in question.

Raises a ValueError exception if the shell argument is not valid.
    """
    if isinstance(shell, basestring):
      resolvedShell = shellType.shellTypeWithString(shell)
      if not shellType.isValidShellType(resolvedShell):
        raise ValueError('The given shell is not understood by VALET: ' + shell)
      shell = resolvedShell
    if shell in self._commandsByShell:
      return self._commandsByShell[shell]
    return None
    
  def setCommandForShell(self, shell, scriptCommand):
    """Given a shell type -- by string or by enumerated integer value -- set the command string that the shell in question should associate with the instance's aliasName.

Raises a ValueError exception if the shell argument is not valid.
    """
    if isinstance(shell, basestring):
      resolvedShell = shellType.shellTypeWithString(shell)
      if not shellType.isValidShellType(resolvedShell):
        raise ValueError('The given shell is not understood by VALET: ' + shell)
      shell = resolvedShell
    if shellType.isValidShellType(shell):
      self._commandsByShell[shell] = scriptCommand
      

  def actionStringWithShellHelper(self, shellHelper, currentContext = contexts.all):
    """Return a configuration fragment string that affects this instance's command alias alteration.  Uses the shellHelper createAlias and removeAlias methods for shell-specific bits."""
    
    # Promote an atomic currentContext to being a set:
    if isinstance(currentContext, basestring):
      currentContext = set(currentContext)
    
    if currentContext in self.contexts:
      aliasName = self.aliasName()
      command = None
      if aliasName:
        for shellType in shellHelper.matchShellTypes():
          command = self.commandForShell(shellType)
          if command:
            break
      if command is not None:
        if command:
          return shellHelper.createAlias(aliasName, command)
        else:
          return shellHelper.removeAlias(aliasName)
    return ''
  

if __name__ == '__main__':
  x = shellAlias('filteredLs', ctxts=['development'])
  x.setCommandForShell('bash', '{ ls -l $1 | grep ^d }');
  print x
  print
  print x.__repr__()
  print

