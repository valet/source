# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import ast
from baseParser import *
from ..package import package
from ..config import *


class pyParser(baseParser):
  """Concrete subclass of baseParser that handles files that contain Python code.

This parser first tries treating the file content as a Python literal -- a serialized, static representation of a Python data structure as expected by the valet.parser.createPackage() function.

If any exception is raised when attempting to parse as a literal, this parser then attempts to execute the file content within an isolated execution context.  If the code produces a variable named "package" then the value of that variable is returned and will be passed to the valet.parser.createPackage() function.
  """
  
  def __init__(self, filePath, shouldAutoReadPackage = True):
    super(pyParser, self).__init__(filePath, shouldAutoReadPackage)
    self._cachedPyStruct = None
    

  def readPackageFromFile(self):
    try:
      filePtr = self.filePtr()
      pyCode = filePtr.read()
      
      try:
        #
        # Attempt to evaluate the python code as a literal:
        #
        self._cachedPyStruct = ast.literal_eval(pyCode)
        return super(pyParser, self).readPackageFromFile()
      except:
        if not config.isExecStyleConfigDisabled:
          if config.confIsBlessed(self.filePath()):
            #
            # If the code could not be evaluated as a literal, try treating it as though
            # it were a script itself.  The script is expected to produce a variable named
            # 'thePackage' in its local context that contains the dict to be passed to the
            # createPackage() function.
            #
            # The execution context has several useful variables provided to it:
            #
            #   VALET_SourceFile    the file being parsed
            #   VALET_SourceName    the name component of the file being parsed
            #   VALET_SourceExt     the file extension of the file being parsed
            #
            # A configuration file of this type might include Python code that walks a
            # directory to find all version of the software and build that list
            # dynamically.  Hypothetically, there would never be any need to update the
            # configuration -- the moment a new version is installed under the package
            # prefix VALET would begin picking it up.
            #
            globals = { 'T': tokenEnum }
            sourceFile = self.filePath()
            sourceName, sourceExt = os.path.splitext(os.path.basename(sourceFile))
            locals = { 'VALET_SourceFile': sourceFile, 'VALET_SourceName': sourceName, 'VALET_SourceExt': sourceExt }
            try:
              exec(pyCode, globals, locals)
              if 'thePackage' in locals:
                pyStruct = locals['thePackage']
                if isinstance(pyStruct, package):
                  #
                  # The code produced an actual VALET package object, so just
                  # return that directly.  Ensure it's source path is set, though:
                  #
                  pyStruct.setSourceFile(sourceFile)
                  return pyStruct
                elif isinstance(pyStruct, dict):
                  #
                  # A dict, stash it and let the parent implementation get it from our
                  # implementation of readIntermediaryDataFromFile:
                  #
                  self._cachedPyStruct = pyStruct
                  return super(pyParser, self).readPackageFromFile()
              self.pushError(self.filePath() + ' did not produce a package dict.')
            except Exception as e:
              self.pushError(self.filePath() + ' does not contain valid Python: ' + str(e))
          else:
            self.pushError(self.filePath() + ' is not in a blessed configuration path')
        else:
          self.pushError(self.filePath() + ' could not be used:  not a Python literal, and executable-style config types are disabled')
    except ValueError as e:
      self.pushError(self.filePath() + ' could not be read: ' + str(e))
    return None
    
  
  def readIntermediaryDataFromFile(self):
    """This method should only get called when the readPackageFromFile method received a dictionary back from the Python code it parsed/executed.  When that happens, the assumption is that that routine stashed the dict in our _cachedPyStruct instance variable."""
    return self._cachedPyStruct

baseParser.registerParser('.vpkg_py', pyParser)
