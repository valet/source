# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import yaml
from baseParser import *


class yamlParser(baseParser):
  """Concrete subclass of baseParser that handles files that contain data in the YAML format.  Since YAML parses directly to the container-based structured format we want, there's not much to do."""

  def readIntermediaryDataFromFile(self):
    # Read the YAML data:
    try:
      if config.shouldUseYAMLSafeLoadMethod:
        return yaml.safe_load(self.filePtr().read())
      return yaml.load(self.filePtr().read())
    except ValueError as e:
      self.pushError(self.filePath() + ' is not a valid YAML file: ' + str(e))
    return None

baseParser.registerParser('.vpkg_yaml', yamlParser)
