# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import types
import collections
import numbers

from ..config import config

from ..enum import *

from ..id import *
from ..envVar import *
from ..executable import *
from ..shellAlias import *
from ..warning import *
from ..predicate import *
from ..package import *
from ..packageVersion import *
from ..packageVersionAlias import *


specialDirectories = [
            #
            # special directories:
            #
              'bindir',
              'libdir',
              'incdir',
              'mandir',
              'infodir',
              'pkgconfigdir'
            ]
            

tokenEnum = enum(specialDirectories + [
            #
            # configEntity
            #
              'contexts',
              # backward compatibility (absorbed into contexts):
              'development-env',
              'no-development-env',
            #
            # basePackage (inherits configEntity):
            #
              'prefix',
              'url',
              'description',
              'actions',
              'dependencies',
              'incompatibilities',
              'attributes',
              'flags',
              # backward compatibility (absorbed into flags):
              'standard-paths',
              'no-standard-paths',
              'feature',
            #
            # package (inherits basePackage):
            #
              'versions',
              'default-version',
            #
            # packageVersion (inherits basePackage):
            #
              # nothing additional
            #
            # packageVersionAlias (inherits basePackage):
            #
              'alias-to',
            #
            # envVar (inherits configEntity):
            #
              'variable',
              'value',
              'operator',
              # backward compatibility (renamed to 'operator'):
              'action',
            #
            # executables (inherits configEntity):
            #
              'order',
              'success',
              'failure',
              'method',
              # backward compatibility (renamed to 'method'):
              #'action',
              'target',
              # backward compatibility (renamed to 'target'):
              'script',
            #
            # shell alias (inherits configEntity):
            #
              'shell-alias',
              'command',
            #
            # warning (inherits configEntity):
            #
              'warning',
            #
            # predicate (inherits configEntity):
            #
              #'variable',
              'path',
              #'operator',
              #'value',
              'stage',
              'message',
            ])


def parseContexts(data, shouldCheckDevFlags = True):
  """Check a container data structure for the "contexts" key-value pair and populate a list with the value/values associated with that key.

For backward compatibility, also checks for the "development-env"/"no-development-env" key and reacts appropriately (add/remove the "development" context).
  """
  out = list()
  if tokenEnum[tokenEnum.contexts] in data:
    if isinstance(data[tokenEnum[tokenEnum.contexts]], types.StringTypes):
      if ',' in data[tokenEnum[tokenEnum.contexts]]:
        out.extend([x.strip() for x in data[tokenEnum[tokenEnum.contexts]].split(',')])
      else:
        out.append(data[tokenEnum[tokenEnum.contexts]])
    elif isinstance(data[tokenEnum[tokenEnum.contexts]], collections.Iterable):
      out.extend(data[tokenEnum[tokenEnum.contexts]])
    else:
      raise ValueError('Invalid value associated with ' + tokenEnum[tokenEnum.contexts] + ' key: ' + str(type(data[tokenEnum[tokenEnum.contexts]])))
  
  if shouldCheckDevFlags:
    devYes = None
    if 'development-env' in data:
      if 'no-development-env' in data:
        devYes = bool(data['development-env']) and not bool(data['no-development-env'])
      else:
        devYes = bool(data['development-env'])
    elif 'no-development-env' in data:
      devYes = not bool(data['no-development-env'])
    if devYes is not None:
      if not contexts.development in out and devYes:
        out.append(contexts.development)
      elif contexts.development in out and not devYes:
        out.remove(contexts.development)
  
  return out


def parseFlags(data):
  """Check a container data structure for the "flags" key-value pair and populate a list with the value/values associated with that key.

For backward compatibility, also checks for the "standard-paths"/"no-standard-paths" key and reacts appropriately (add/remove the "no-standard-paths" flag).
  """
  out = []
  if tokenEnum[tokenEnum.flags] in data:
    if isinstance(data[tokenEnum[tokenEnum.flags]], types.StringTypes):
      if ',' in data[tokenEnum[tokenEnum.flags]]:
        out.append([x.strip() for x in data[tokenEnum[tokenEnum.flags]].split(',')])
      else:
        out.append(data[tokenEnum[tokenEnum.flags]])
    elif isinstance(data[tokenEnum[tokenEnum.flags]], collections.Iterable):
      out.extend(data[tokenEnum[tokenEnum.flags]])
    else:
      raise ValueError('Invalid value associated with ' + tokenEnum[tokenEnum.flags] + ' key: ' + str(type(data[tokenEnum[tokenEnum.flags]])))

  stdYes = None
  if 'standard-paths' in data:
    if 'no-standard-paths' in data:
      stdYes = bool(data['standard-paths']) and not bool(data['no-standard-paths'])
    else:
      stdYes = bool(data['standard-paths'])
  elif 'no-standard-paths' in data:
    stdYes = not bool(data['no-standard-paths'])
  if stdYes is not None:
    if basePackage.shouldNotAddStandardPathsFlag in out and stdYes:
      out.remove(basePackage.shouldNotAddStandardPathsFlag)
    elif not basePackage.shouldNotAddStandardPathsFlag in out and not stdYes:
      out.append(basePackage.shouldNotAddStandardPathsFlag)

  devYes = None
  if 'development-env' in data:
    if 'no-development-env' in data:
      devYes = bool(data['development-env']) and not bool(data['no-development-env'])
    else:
      devYes = bool(data['development-env'])
  elif 'no-development-env' in data:
    devYes = not bool(data['no-development-env'])
  if devYes is not None:
    if basePackage.shouldNotAddDevStandardPathsFlag in out and devYes:
      out.remove(basePackage.shouldNotAddDevStandardPathsFlag)
    elif not basePackage.shouldNotAddDevStandardPathsFlag in out and not devYes:
      out.append(basePackage.shouldNotAddDevStandardPathsFlag)
  
  return out


def createEnvVar(data):
  """The argument should be a dict containing the fields of an envVar action keyed by the standard keys (see tokenEnum above)."""
  try:
    if tokenEnum[tokenEnum.variable] in data:
      variableName = data[tokenEnum[tokenEnum.variable]]
    else:
      raise KeyError('variable is a required field')
      
    if not tokenEnum[tokenEnum.operator] in data:
      if not tokenEnum[tokenEnum.action] in data:
        variableOp = opType.opTypeEnum.set
      else:
        variableOp = opType.opTypeForString(data[tokenEnum[tokenEnum.action]])
    else:
      variableOp = opType.opTypeForString(data[tokenEnum[tokenEnum.operator]])
    
    if tokenEnum[tokenEnum.value] in data:
      newValue = data[tokenEnum[tokenEnum.value]]
    else:
      newValue = None
    
    ctxts = parseContexts(data)
    
    return envVar(variableName, variableOp, newValue, ctxts)
  except:
    raise
  
  
def createExecutable(data):
  """The argument should be a dict containing fields of an executable action keyed by the standard keys (see tokenEnum above)."""
  try:
    method = executableMethod.source
    if not tokenEnum[tokenEnum.method] in data:
      if tokenEnum[tokenEnum.action] in data:
        method = executableMethod[data[tokenEnum[tokenEnum.action]]]
    else:
      method = executableMethod[data[tokenEnum[tokenEnum.method]]]
    
    order = rcOrder.succeedThenFail
    if tokenEnum[tokenEnum.order] in data:
      orderStr = data[tokenEnum[tokenEnum.order]]
      if orderStr in rcOrder:
        order = rcOrder[orderStr]
      else:
        map = { 'success-first': rcOrder.succeedThenFail, 'failure-first': rcOrder.failThenSucceed,
                'success_first': rcOrder.succeedThenFail, 'failure_first': rcOrder.failThenSucceed }
        if orderStr in map:
          order = map[orderStr]
        else:
          raise ValueError('Invalid executable outcome order: ' + orderStr)
      
    successOnRc = int(data[tokenEnum[tokenEnum.success]]) if tokenEnum[tokenEnum.success] in data else 0
    failureOnRc = int(data[tokenEnum[tokenEnum.failure]]) if tokenEnum[tokenEnum.failure] in data else None
    
    targets = {}
    if tokenEnum[tokenEnum.target] in data:
      targets = data[tokenEnum[tokenEnum.target]]
    elif tokenEnum[tokenEnum.script] in data:
      targets = data[tokenEnum[tokenEnum.script]]
    if not isinstance(targets, dict):
      raise ValueError('Invalid target/script value associated with target/action key: ' + str(type(targets)))
    
    # Empty targets dict is a problem:
    if len(targets) == 0:
      raise ValueError('Invalid executable defintion: empty list of targets/scripts')
    
    ctxts = parseContexts(data)
    
    return executable(method, order, successOnRc, failureOnRc, targets, ctxts)
  except:
    raise
  
  
def createShellAlias(data):
  """The argument should be a dict containing fields of a shellAlias action keyed by the standard keys (see tokenEnum above)."""
  try:
    if tokenEnum[tokenEnum['shell-alias']] in data:
      aliasName = data[tokenEnum[tokenEnum['shell-alias']]]
    else:
      raise KeyError('shell-alias is a required field')
      
    commands = {}
    if tokenEnum[tokenEnum.command] in data:
      commands = data[tokenEnum[tokenEnum.command]]
      if not isinstance(commands, dict):
        raise ValueError('Invalid data associated with command key: ' + str(type(commands)))
      
    ctxts = parseContexts(data)
    
    return shellAlias(aliasName, commands, ctxts)
  except:
    raise
  
  
def createWarning(data):
  """The argument should be a dict containing fields of a warning action keyed by the standard keys (see tokenEnum above)."""
  try:
    if tokenEnum[tokenEnum.warning] in data:
      message = data[tokenEnum[tokenEnum.warning]]
    else:
      raise KeyError('message is a required field')
      
    ctxts = parseContexts(data)
    
    return warning(message, ctxts)
  except:
    raise
  
  
def createPredicate(data):
  """The argument should be a dict containing fields of a predicate configEntity keyed by the standard keys (see tokenEnum above)."""
  try:
    if not tokenEnum[tokenEnum.variable] in data:
      if tokenEnum[tokenEnum.path] in data:
        variableName = data[tokenEnum[tokenEnum.path]]
      else:
        raise KeyError(tokenEnum[tokenEnum.variable] + '/' + tokenEnum[tokenEnum.path] + ' is a required field')
    else:
      variableName = data[tokenEnum[tokenEnum.variable]]
      
    if tokenEnum[tokenEnum.operator] in data:
      operator = data[tokenEnum[tokenEnum.operator]]
    else:
      raise KeyError(tokenEnum[tokenEnum.operator] + ' is a required field')
      
    testValue = data[tokenEnum[tokenEnum.value]] if tokenEnum[tokenEnum.value] in data else None
      
    if tokenEnum[tokenEnum.stage] in data:
      stage = data[tokenEnum[tokenEnum.stage]]
    else:
      stage = 'pre-condition'
      
    message = data[tokenEnum[tokenEnum.message]] if tokenEnum[tokenEnum.message] in data else None
    
    ctxts = parseContexts(data)
    
    return predicate(variableName, operator, testValue, stage, message, ctxts)
  except:
    raise


def createId(data):
  """The argument should be a string.  Instantiates an id object using data as the argument to the constructor.

Since the id class raises a ValueError exception if the string is invalid, a caller should be prepared to receive an exception.
  """
  if not isinstance(data, types.StringTypes):
    raise ValueError('expected string type, not ' + str(type(data)))
  return id(data)


def createIdOrString(data):
  """The argument should be a string.  Attempts to instantiate an id object using data as the argument to the constructor.  If the id class raises an exception, then assume the string contains regex/version predicates and just return the string itself.
  """
  if not isinstance(data, types.StringTypes):
    raise ValueError('expected string type, not ' + str(type(data)))
  try:
    return id(data)
  except:
    return data
    
    
def createDependenciesOrIncompatibilities(data):
  """The argument should be a list.  Iterates over the elements of that list and turns them into VALET test objects:  strings become id instances or stay as a package id pattern string; predicate instances are created for aptly structured dicts.

This method will raise a ValueError if any item of the data list is unhandled.  See also the createPredicate method.
  """
  tests = []
  if data:
    for testSpec in data:
      # If it's just a string => package id/pattern:
      if isinstance(testSpec, types.StringTypes):
        tests.append( createIdOrString(testSpec) )
      # Hand a dict off to createPredicate:
      elif isinstance(testSpec, dict):
        tests.append( createPredicate(testSpec) )
      else:
        raise ValueError('invalid dependency/incompatibility item: ' + str(testSpec))
  return tests


def createSpecialDirectories(data):
  """The argument should be a dict containing strings/lists of strings that are keyed by one of the special directory tokens.  An envVar instance is created to represent each modification; for lib and include directories, envVar instances with the "development" context only are also created.

This method will raise a ValueError if any item of the data list is unhandled.
  """
  specialDirActions = []
  for dtype, dirs in data.iteritems():
    #
    # Promote a string to a list to make the rest of the code easier:
    #
    if isinstance(dirs, types.StringTypes):
      dirs = [dirs]
      
    if dtype == tokenEnum[tokenEnum.bindir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('PATH', opType.opTypeEnum.prependPath, dir) )
        
    elif dtype == tokenEnum[tokenEnum.libdir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('LD_LIBRARY_PATH', opType.opTypeEnum.prependPath, dir) )
        specialDirActions.append( envVar('LDFLAGS', opType.opTypeEnum.appendSpace, '-L' + dir, [contexts.development]) )
        
    elif dtype == tokenEnum[tokenEnum.incdir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('CPPFLAGS', opType.opTypeEnum.appendSpace, '-I' + dir, [contexts.development]) )
        
    elif dtype == tokenEnum[tokenEnum.mandir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('MANPATH', opType.opTypeEnum.prependPath, dir) )
        
    elif dtype == tokenEnum[tokenEnum.infodir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('INFOPATH', opType.opTypeEnum.prependPath, dir) )
        
    elif dtype == tokenEnum[tokenEnum.pkgconfigdir]:
      for dir in dirs:
        # If it's a relative path, prefix with the VALET_PATH_PREFIX
        # bit:
        if not os.path.isabs(dir) and not dir.startswith('${'):
          dir = os.path.join('${VALET_PATH_PREFIX}', dir)
        specialDirActions.append( envVar('PKG_CONFIG_PATH', opType.opTypeEnum.prependPath, dir) )
        
    else:
      raise KeyError('invalid special directory type: ' + dtype)
    
  return specialDirActions


def createActions(data):
  """The argument should be a list.  Iterates over the elements of that list and turns them into VALET action objects.

This method will raise a ValueError if any item of the data list is unhandled.  See the other create methods of this class for additional exception conditions.
  """
  actions = []
  if data:
    for actionSpec in data:
      if isinstance(actionSpec, dict):
        # An envVar requires the variable name:
        if tokenEnum[tokenEnum.variable] in actionSpec:
          actions.append( createEnvVar(actionSpec) )
        # An executable should have the target (nee script) key present:
        elif tokenEnum[tokenEnum.target] in actionSpec or tokenEnum[tokenEnum.script] in actionSpec:
          actions.append( createExecutable(actionSpec) )
        # A shell alias must have the 'shell-alias' key present:
        elif tokenEnum[tokenEnum['shell-alias']] in actionSpec:
          actions.append( createShellAlias(actionSpec) )
        # A warning should have the 'warning' key present:
        elif tokenEnum[tokenEnum.warning] in actionSpec:
          actions.append( createWarning(actionSpec) )
        else:
          # Special directory?
          specialDirectory = False
          for k in actionSpec.keys():
            specialDirectory = specialDirectory or (k in specialDirectories)
          if specialDirectory:
            actions.extend( createSpecialDirectories(actionSpec) )
          else:
            raise ValueError('unhandled action item: ' + str(actionSpec))
      else:
        raise ValueError('invalid action item: ' + str(actionSpec))
  return actions


def createPackageVersion(parentPackage, withId, data):
  """The argument should be a dict containing fields of a packageVersion/packageVersionAlias keyed by the standard keys (see tokenEnum above).

This method will raise exceptions for a variety of reasons; see the underlying create* methods for explanations.
  """
  ctxts = parseContexts(data, False)
  
  # The id _could_ be a number, so cast it to a string if so:
  if isinstance(withId, numbers.Number):
    withId = str(withId)
  
  if 'alias-to' in data:
    aliasTo = data['alias-to']
    # The id _could_ be a number, so cast it to a string if so:
    if isinstance(aliasTo, numbers.Number):
      aliasTo = str(aliasTo)
    # Package alias:
    theVersion = packageVersionAlias(withId, parentPackage, aliasTo, ctxts)
  else:
    # Prefix:
    prefix = data[tokenEnum[tokenEnum.prefix]] if tokenEnum[tokenEnum.prefix] in data else None
    
    # Description:
    description = data[tokenEnum[tokenEnum.description]] if tokenEnum[tokenEnum.description] in data else None
    
    # Reference URL:
    referenceURL = data[tokenEnum[tokenEnum.url]] if tokenEnum[tokenEnum.url] in data else None
    
    # Flags:
    flags = parseFlags(data)
    
    # Dependencies:
    dependencies = createDependenciesOrIncompatibilities(data[tokenEnum[tokenEnum.dependencies]]) if tokenEnum[tokenEnum.dependencies] in data else []
    
    # Incompatibilities:
    incompatibilities = createDependenciesOrIncompatibilities(data[tokenEnum[tokenEnum.incompatibilities]]) if tokenEnum[tokenEnum.incompatibilities] in data else []
    
    # Actions:
    actions = createActions(data[tokenEnum[tokenEnum.actions]]) if tokenEnum[tokenEnum.actions] in data else []
    
    theVersion = packageVersion(withId, prefix, description, referenceURL, flags, incompatibilities, dependencies, actions, parentPackage, ctxts)
    if theVersion:
      # Attributes?
      if tokenEnum[tokenEnum.attributes] in data:
        theVersion.setAttributes(data[tokenEnum[tokenEnum.attributes]])

def createPackage(withId, data, sourceFile = None):
  """The argument should be a dict containing fields of a package keyed by the standard keys (see tokenEnum above).

This method will raise exceptions for a variety of reasons; see the underlying create* methods for explanations.
  """
  ctxts = parseContexts(data, False)
  
  # The id _could_ be a number, so cast it to a string if so:
  if isinstance(withId, numbers.Number):
    withId = str(withId)
  
  # Prefix:
  prefix = data[tokenEnum[tokenEnum.prefix]] if tokenEnum[tokenEnum.prefix] in data else None
  
  # Description:
  description = data[tokenEnum[tokenEnum.description]] if tokenEnum[tokenEnum.description] in data else None
  
  # Reference URL:
  referenceURL = data[tokenEnum[tokenEnum.url]] if tokenEnum[tokenEnum.url] in data else None
  
  # Flags:
  flags = parseFlags(data)
  
  # Dependencies:
  dependencies = createDependenciesOrIncompatibilities(data[tokenEnum[tokenEnum.dependencies]]) if tokenEnum[tokenEnum.dependencies] in data else []
  
  # Incompatibilities:
  incompatibilities = createDependenciesOrIncompatibilities(data[tokenEnum[tokenEnum.incompatibilities]]) if tokenEnum[tokenEnum.incompatibilities] in data else []
  
  # Actions:
  actions = createActions(data[tokenEnum[tokenEnum.actions]]) if tokenEnum[tokenEnum.actions] in data else []
  
  # Default version:
  defaultVersionId = data['default-version'] if 'default-version' in data else None
  if defaultVersionId and isinstance(defaultVersionId, numbers.Number):
    defaultVersionId = str(defaultVersionId)
  
  thePackage = package(withId, prefix, description, referenceURL, flags, incompatibilities, dependencies, actions, None, defaultVersionId, sourceFile, ctxts)
  if thePackage is not None:
    # Attributes?
    if tokenEnum[tokenEnum.attributes] in data:
      thePackage.setAttributes(data[tokenEnum[tokenEnum.attributes]])
    
    # Versions:
    if tokenEnum[tokenEnum.versions] in data:
      for pkgVersId, versData in data[tokenEnum[tokenEnum.versions]].iteritems():
        createPackageVersion(thePackage, pkgVersId, versData)
  
  return thePackage


class baseParser(object):
  """Abstract base class for VALET configuration file parsers."""
  
  # File extension => subclass of baseParser 
  registeredParsers = dict()
  
  
  @classmethod
  def registerParser(cls, extensions, theClass):
    """Given one or more filename extensions, register theClass as the baseParser concrete subclass that handles the import of configuration data from files which have one of the extensions.

The extensions argument can be a string or a list of strings.

This method should be called by each subclass when it is imported.
    """
    if isinstance(extensions, types.StringTypes):
      cls.registeredParsers[extensions] = theClass
    else:
      for extension in extensions:
        cls.registeredParsers[extension] = theClass
  
  
  @classmethod
  def supportedFileExtensions(cls):
    """Returns a list of the file extensions associated with parser subclasses."""
    return cls.registeredParsers.keys()
  

  def __init__(self, filePath, shouldAutoReadPackage = True):
    """Constructor
    
Setup the parser.
    """
    self._didAddDefaults = False
    self._filePtr = None
    self._filePath = os.path.abspath(filePath)
    self._errorStack = []
    self._warningStack = []
    
    if not os.path.exists(filePath):
      raise ValueError('file does not exist: ' + filePath)
    
    if not os.access(filePath, os.R_OK):
      raise ValueError('file is not readable: ' + filePath)

    if shouldAutoReadPackage:
      self._parsedPackage = self.readPackageFromFile()


  def __del__(self):
    """Destructor
    
Close the file if it's still open.
    """
    if self._filePtr is not None:
      self._filePtr.close()
    
  
  def filePtr(self):
    """Lazily opens the source file and returns the file pointer."""
    if self._filePtr is None:
      try:
        self._filePtr = open(self._filePath, 'r')
      except Exception as e:
        raise e
    return self._filePtr
  
  
  def filePath(self):
    """Return this parser's source file path."""
    return self._filePath


  def readPackageFromFile(self):
    """Calls readIntermediaryDataFromFile() to get the intermediary-format of the configuration data.  If successful, parses that data to produce a VALET package object.  Returns the new object if successful, None otherwise."""
    try:
      pkgData = self.readIntermediaryDataFromFile()
      if pkgData and isinstance(pkgData, dict):
        if len(pkgData) == 1:
          # Length of 1 => we iterate just once:
          for pkgId, pkgPayload in pkgData.iteritems():
            return createPackage(pkgId, pkgPayload, self._filePath)
        else:
          self.pushError('Top-level dictionary must be a single key-value pair.')
      else:
        self.pushError('Invalid package data returned by intermediary parser.')
    except Exception as e:
      self.pushError(str(e))
    return None

  
  def readIntermediaryDataFromFile(self):
    """Subclasses should provide their own implementation that reads the configuration file and returns the intermediate form for the package data."""
    raise NotImplementedError('concrete subclasses of baseParser must reimplement the readFromFile() method.')


  def parsedPackage(self):
    """Returns the package object that was parsed.  The first time this method is called we add any default actions et al."""
    if self._parsedPackage:
      if not self._didAddDefaults:
        if config.shouldAddPackagePrefixAction:
          encPkgId = self._parsedPackage.id().packageId().encodedForEnvVar()
          prefixAction = envVar(encPkgId + '_PREFIX', opType.opTypeEnum.set, '${VALET_PATH_PREFIX}', [contexts.development])
          self._parsedPackage.addAction(prefixAction)
        self._didAddDefaults = True
    return self._parsedPackage


  def hasErrors(self):
    """Returns True if the error stack is not empty."""
    return len(self._errorStack) > 0


  def errorStack(self):
    """Returns the error stack (list)."""
    return self._errorStack


  def pushError(self, errorStr):
    """Adds an error string to the stack."""
    self._errorStack.append(errorStr)


  def popError(self):
    """Removes the oldest error string from the stack and returns it.  If the stack is empty, returns None."""
    if len(self._errorStack) > 0:
      return self._errorStack.pop(0)
    return None


  def hasWarnings(self):
    """Returns True if the warning stack is not empty."""
    return len(self._warningStack) > 0


  def warningStack(self):
    """Returns the warning stack (list)."""
    return self._warningStack


  def pushWarning(self, warnStr):
    """Adds a warning string to the stack."""
    self._warningStack.append(warnStr)


  def popWarning(self):
    """Removes the oldest warning string from the stack and returns it.  If the stack is empty, returns None."""
    if len(self._warningStack) > 0:
      return self._warningStack.pop(0)
    return None


def parserForFile(filePath, shouldAutoReadPackage = True):
  """Locates the subclass of baseParser that handles files with whatever extension is present on filePath.  Raises a RuntimeError exception if the extension is not handled by any subclass."""
  filename, fileExtension = os.path.splitext(filePath)
  if fileExtension in baseParser.registeredParsers:
    return baseParser.registeredParsers[fileExtension](filePath, shouldAutoReadPackage)
  else:
    raise RuntimeError('No VALET parser registered for file extension: ' + fileExtension)
