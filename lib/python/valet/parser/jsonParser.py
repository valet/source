# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import json
from baseParser import *


class jsonParser(baseParser):
  """Concrete subclass of baseParser that handles files that contain a modified JSON format.

JSON itself does not allow for comments to be included in the stream.  This parser scrubs all lines prefixed by whitespace and a hash tag before parsing."""

  def readIntermediaryDataFromFile(self):
    try:
      # Scrub all comments:
      comments = re.compile(r'^\s*#.*$', re.MULTILINE)
      jsonData = comments.sub("\n", self.filePtr().read())
      
      return json.loads(jsonData)
    except ValueError as e:
      self.pushError(self.filePath() + ' is not a valid JSON file: ' + str(e))
    return None

baseParser.registerParser('.vpkg_json', jsonParser)
