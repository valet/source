# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import xml.dom.minidom
from baseParser import *

#
# Tokens that are specific to the XML parser.  These are primarily
# tag names for XML elements:
#
xmlTokens = enum([
                'package',
                'version',
                'export',
                'id',
                'flag',
                'feature',
                'features',
                'context',
                'name',
                'shell',
                'predicate'
              ]);


#
# XML elements that have scalar string values:
#
scalarElements = ['prefix', 'url', 'description']


#
# XML elements that have a boolean value:
#
booleanScalarElements = ['no-development-env', 'no-standard-paths', 'development-env', 'standard-paths']


def xmlCollapseTextAndCDATA(xmlParentNode):
  """Concatenate a sequence of TEXT and CDATA nodes into a single string."""
  text = None
  if len(xmlParentNode.childNodes):
    text = ''
    for node in xmlParentNode.childNodes:
      if node.nodeType in (xml.dom.Node.TEXT_NODE, xml.dom.Node.CDATA_SECTION_NODE):
        text = text + node.nodeValue
  return text


class xmlParser(baseParser):
  """Concrete subclass of baseParser that handles files that contain data in the XML format.  XML is no longer the preferred format, primarily due to the shear complexity of this parser relative to the others.  Some of the newer features are simply not available in the XML format, either."""

  def readIntermediaryDataFromFile(self):
    try:
      filePtr = self.filePtr()
      xmlDoc = xml.dom.minidom.parse(filePtr)

      # What's the package id?
      rootNode = xmlDoc.documentElement
      if rootNode is None:
        self.pushError(
            self.filePath() + ' is not a valid XML file: no document element'
          )
      elif rootNode.tagName != xmlTokens[xmlTokens.package]:
        self.pushError(
            self.filePath() + ' is not a valid XML file: document element should be <package>'
          )
      elif rootNode.getAttribute(xmlTokens[xmlTokens.id]) == '':
        self.pushError(
            self.filePath() + ' is not a valid VALET package file: no id attribute on document element'
          )
      else:
        pkgObj = self.__packageOrVersionWithNode(rootNode, None)
        return { rootNode.getAttribute(xmlTokens[xmlTokens.id]): pkgObj }
    except Exception as e:
      self.pushError(self.filePath() + ' is not a valid XML file: ' + str(e))
    return None


  def __packageOrVersionWithNode(self, pkgNode, parentEntity = None):
    """Process an XML <package> or <version> element.  The element comes in as pkgNode, and the parent package comes in as parentEntity.

Various exceptions may be raised, which would be caught by the readIntermediaryDataFromFile() method and be added to the error stack for this parser.

If successful, returns a dict containing the key-value pairs expected by the valet.parser.createPackage() function.
    """
    topObj = dict()
    pkgId = pkgNode.getAttribute(xmlTokens[xmlTokens.id])
    features = set()
    
    # Check for a contexts attribute:
    if pkgNode.hasAttribute(tokenEnum[tokenEnum.contexts]):
      topObj[tokenEnum[tokenEnum.contexts]] = pkgNode.getAttribute(tokenEnum[tokenEnum.contexts])
    
    if parentEntity is not None and pkgNode.hasAttribute('alias-to'):
      #
      # This is a version alias:
      #
      return { 'alias-to': pkgNode.getAttribute('alias-to') }

    # Check for all fields in common between <package> and <version>
    for element in pkgNode.childNodes:
      if element.nodeType == xml.dom.Node.ELEMENT_NODE:
        # Scalars:
        if element.tagName in scalarElements:
          topObj[element.tagName] = xmlCollapseTextAndCDATA(element)
        elif element.tagName in booleanScalarElements:
          property = element.tagName
          value = True
          if property.startswith('no-'):
            negate = True
            property = property[3:]
          else:
            negate = False
          strValue = xmlCollapseTextAndCDATA(element)
          if strValue is not None:
            strValue = strValue.lower()
            if strValue in ('true', 'yes') or int(strValue):
              value = True
            elif strValue in ('false', 'no') or int(strValue) == 0:
              value = False
          if negate:
            value = not value
          topObj[property] = value

        # Features:
        elif element.tagName == xmlTokens[xmlTokens.feature]:
          # Features are only viable on a package version:
          if parentEntity is not None:
            # Remove whitespace:
            feature = re.sub(r'\s+', '_', xmlCollapseTextAndCDATA(element))
            # Remove any characters that aren't allowed:
            feature = re.sub(r'[^A-Za-z0-9._-]+', '', feature)
            # Extract the largest feature tag that's left:
            feature = re.sub(atom.patternString() + '$', '', feature)
            features.add(feature)
          else:
            self.pushWarning('Features usable only on package versions.')
        
        # Contexts:
        elif element.tagName == xmlTokens[xmlTokens.context]:
          context = xmlCollapseTextAndCDATA(element)
          if tokenEnum[tokenEnum.contexts] in topObj:
            topObj[tokenEnum[tokenEnum.contexts]].append(context)
          else:
            topObj[tokenEnum[tokenEnum.contexts]] = list(context)
        
        # Flags:
        elif element.tagName == xmlTokens[xmlTokens.flag]:
          flag = xmlCollapseTextAndCDATA(element)
          if tokenEnum[tokenEnum.flags] in topObj:
            topObj[tokenEnum[tokenEnum.flags]].append(flag)
          else:
            topObj[tokenEnum[tokenEnum.flags]] = list(flag)

        # If this is a package entity then default-version is allowed:
        elif element.tagName == 'default-version':
          if parentEntity is None:
            topObj['default-version'] = xmlCollapseTextAndCDATA(element)
          else:
            self.pushWarning('The default-version property is not usable inside a package version.')
        
        # Actions to be taken across all versions of the package; should be
        # a list:
        elif element.tagName == tokenEnum[tokenEnum.actions]:
          pkgActions = self.__actionsFromNode(element)
          if pkgActions is not None and len(pkgActions):
            if tokenEnum[tokenEnum.actions] in topObj:
              topObj[tokenEnum[tokenEnum.actions]].extend(pkgActions)
            else:
              topObj[tokenEnum[tokenEnum.actions]] = pkgActions

        # Dependencies:
        elif element.tagName == tokenEnum[tokenEnum.dependencies]:
          pkgDepends = self.__packagesAndPredicatesFromNode(element)
          if pkgDepends is not None and len(pkgDepends):
            if tokenEnum[tokenEnum.dependencies] in topObj:
              topObj[tokenEnum[tokenEnum.dependencies]].extend(pkgDepends)
            else:
              topObj[tokenEnum[tokenEnum.dependencies]] = pkgDepends

        # Incompatibilies:
        elif element.tagName == tokenEnum[tokenEnum.incompatibilities]:
          pkgIncompats = self.__packagesAndPredicatesFromNode(element)
          if pkgIncompats is not None and len(pkgIncompats):
            if tokenEnum[tokenEnum.incompatibilities] in topObj:
              topObj[tokenEnum[tokenEnum.incompatibilities]].extend(pkgIncompats)
            else:
              topObj[tokenEnum[tokenEnum.incompatibilities]] = pkgIncompats

        # Version:
        elif element.tagName == xmlTokens[xmlTokens.version]:
          versId = element.getAttribute(xmlTokens[xmlTokens.id])
          if versId:
            if not '/' in versId:
              versId = pkgId + '/' + versId
            version = self.__packageOrVersionWithNode(element, topObj)
            if version:
              if not tokenEnum[tokenEnum.versions] in topObj:
                topObj[tokenEnum[tokenEnum.versions]] = dict()
              if xmlTokens[xmlTokens.features] in version:
                featureStr = ','.join(version[xmlTokens[xmlTokens.features]])
                topObj.pop(xmlTokens[xmlTokens.features], None)
                if ':' in versId:
                  versId += ',' + featureStr
                else:
                  versId += ':' + featureStr
              topObj[tokenEnum[tokenEnum.versions]][versId] = version
          else:
            self.pushError('A version element must have an id attribute.')

        #
        # VERSION 1.0 XML COMPATIBILITY: Allow for <export>, <script>, and special
        #                                dirs (e.g. <bindir>) outside an <actions>
        #                                element.
        #
        elif element.tagName == xmlTokens[xmlTokens.export]:
          newAction = self.__envExportFromNode(element)
          if newAction is not None:
            if tokenEnum[tokenEnum.actions] in topObj:
              topObj[tokenEnum[tokenEnum.actions]].append(newAction)
            else:
              topObj[tokenEnum[tokenEnum.actions]] = [newAction]
        elif element.tagName == tokenEnum[tokenEnum.script]:
          newAction = self.__executableFromNode(element)
          if newAction is not None:
            if tokenEnum[tokenEnum.actions] in topObj:
              topObj[tokenEnum[tokenEnum.actions]].append(newAction)
            else:
              topObj[tokenEnum[tokenEnum.actions]] = [newAction]
        elif element.tagName in specialDirectories:
          newAction = self.__specialDirFromNode(element)
          if newAction is not None and len(newAction) > 0:
            if tokenEnum[tokenEnum.actions] in topObj:
              topObj[tokenEnum[tokenEnum.actions]].append(newAction)
            else:
              topObj[tokenEnum[tokenEnum.actions]] = [newAction]
        #
        # Not handled:
        #
        else:
          self.pushWarning(
              'Unhandled element: ' + element.toxml()
            );
    
    if len(features):
      topObj[xmlTokens[xmlTokens.features]] = features
    
    return topObj
    
  
  def __legacyDevEnvContextForNode(self, node):
    """Check for the "development-env" or "no-development-env" attribute on an XML element.  Returns a dict that should be used to extend the package/version being parsed."""
    isDev = False
    isDevVal = node.hasAttribute('development-env')
    if isDevVal:
      isDev = True
    else:
      isDevVal = node.getAttribute('no-development-env')
      if isDevVal:
        isDev = False
      else:
        isDevVal = None
    if isDevVal is not None and isinstance(isDevVal, basestring):
      isDevVal = isDevVal.lower()
      if isDevVal == 'true' or isDevVal == 'yes' or int(isDevVal):
        pass
      elif isDevVal == 'false' or isDevVal == 'no' or int(isDevVal) == 0:
        isDev = not isDev
      return { 'development-env': isDev }
    return {}
    
    
  def __envExportFromNode(self, node):
    """Process an XML <export> element to produce a dict containing key-value pairs expected by the valet.parser.createEnvVar() function."""
    newObj = None
    varName = node.getAttribute(tokenEnum[tokenEnum.variable])
    if varName:
      newObj = { tokenEnum[tokenEnum.variable]: varName }
      
      # How will we be modifying the envvar?
      if node.hasAttribute(tokenEnum[tokenEnum.action]):
        newObj[tokenEnum[tokenEnum.operator]] = node.getAttribute(tokenEnum[tokenEnum.action])
      
      # Textual content of the element is the operand:
      newObj[tokenEnum[tokenEnum.value]] = xmlCollapseTextAndCDATA(node)
      
      # Contexts?
      if node.hasAttribute(tokenEnum[tokenEnum.contexts]):
        newObj[tokenEnum[tokenEnum.contexts]] = node.getAttribute(tokenEnum[tokenEnum.contexts])
      
      # Legacy development env flag:
      newObj.update(self.__legacyDevEnvContextForNode(node))
    else:
      self.pushError('invalid <export> element: ' + node.toxml())
      
    return newObj


  def __warningFromNode(self, node):
    """Process an XML <warning> element to produce a dict containing key-value pairs expected by the valet.parser.createWarning() function."""
    newObj = { tokenEnum[tokenEnum.message]: xmlCollapseTextAndCDATA(node) }
      
    # Contexts?
    if node.hasAttribute(tokenEnum[tokenEnum.contexts]):
      newObj[tokenEnum[tokenEnum.contexts]] = node.getAttribute(tokenEnum[tokenEnum.contexts])
    
    # Legacy development env flag:
    newObj.update(self.__legacyDevEnvContextForNode(node))
    
    return newObj


  def __shellAliasFromNode(self, node):
    """Process an XML <shell-alias> element to produce a dict containing key-value pairs expected by the valet.parser.createShellAlias() function."""
    newObj = None
    aliasName = node.getAttribute(node, xmlTokens[xmlTokens.name])
    if aliasName:
      newObj = { 'shell-alias': aliasName }
      
      shell = node.getAttribute(xmlTokens[xmlTokens.shell])
      if not shell:
        shell = '*'
      newObj[tokenEnum[tokenEnum.command]] = { shell: xmlCollapseTextAndCDATA(node) }
      
      # Contexts?
      if node.hasAttribute(tokenEnum[tokenEnum.contexts]):
        newObj[tokenEnum[tokenEnum.contexts]] = node.getAttribute(tokenEnum[tokenEnum.contexts])
      
      # Legacy development env flag:
      newObj.update(self.__legacyDevEnvContextForNode(node))
    else:
      self.pushError('shell alias must have a "name" attribute: ' + node.toxml())
    
    return newObj


  def __executableFromNode(self, node):
    """Process an XML <script> element to produce a dict containing key-value pairs expected by the valet.parser.createExecutable() function."""
    newObj = None
    execPath = xmlCollapseTextAndCDATA(node)
    if execPath:
      shell = node.getAttribute(xmlTokens[xmlTokens.shell])
      if not shell:
        shell = '*'
        
      newObj = { tokenEnum[tokenEnum.target]: { shell: execPath } }
      
      if node.hasAttribute(tokenEnum[tokenEnum.action]):
        newObj[tokenEnum[tokenEnum.method]] = node.getAttribute(tokenEnum[tokenEnum.action])
      
      # Contexts?
      if node.hasAttribute(tokenEnum[tokenEnum.contexts]):
        newObj[tokenEnum[tokenEnum.contexts]] = node.getAttribute(tokenEnum[tokenEnum.contexts])
      
      # Legacy development env flag:
      newObj.update(self.__legacyDevEnvContextForNode(node))
    else:
      self.pushError('invalid <script> element: ' + node.toxml())
    
    return newObj


  def __specialDirFromNode(self, node):
    """Process an XML element whose tag is one of the special directory types to produce a dict containing key-value pairs expected by the valet.parser.createSpecialDirectories() function."""
    newObj = { node.tagName: xmlCollapseTextAndCDATA(node) }
    
    # Contexts?
    if node.hasAttribute(tokenEnum[tokenEnum.contexts]):
      newObj[tokenEnum[tokenEnum.contexts]] = node.getAttribute(tokenEnum[tokenEnum.contexts])
    
    # Legacy development env flag:
    newObj.update(self.__legacyDevEnvContextForNode(node))
    
    return newObj
    

  def __actionsFromNode(self, actionsNode):
    """Process an XML <actions> element.  This element is a container whose child nodes are all action elements."""
    pkgActions = []
    for node in actionsNode.childNodes:
      newAction = None
      if node.nodeType == xml.dom.Node.ELEMENT_NODE:
        #
        # Generic variable:
        #
        if node.tagName == xmlTokens[xmlTokens.export]:
          newAction = self.__envExportFromNode(node)
        #
        # Aliases:
        #
        elif node.tagName == 'shell-alias':
          newAction = self.__shellAliasFromNode(node)
        #
        # Scripts:
        #
        elif node.tagName == tokenEnum[tokenEnum.script]:
          newAction = self.__executableFromNode(node)
        #
        # Warnings:
        #
        elif node.tagName == tokenEnum[tokenEnum.warning]:
          newAction = self.__warningFromNode(node)
        #
        # Special variables:
        #
        elif node.tagName in specialDirectories:
          newAction = self.__specialDirFromNode(node)
        #
        # Not recognized:
        #
        else:
          self.pushWarning(
              'Invalid action entity: ' + node.toxml()
            )
      
      # Add new action to the list:
      if newAction:
        pkgActions.append(newAction)
    return pkgActions


  def __predicateFromNode(self, node):
    """Process an XML <predicate> element to produce a dict containing key-value pairs expected by the valet.parser.createPredicate() function."""
    newObj = { }

    if node.hasAttribute(tokenEnum[tokenEnum.stage]):
      newObj[tokenEnum[tokenEnum.stage]] = node.getAttribute(tokenEnum[tokenEnum.stage])
    else:
      newObj[tokenEnum[tokenEnum.stage]] = 'pre-condition'

    if node.hasAttribute(tokenEnum[tokenEnum.operator]):
      newObj[tokenEnum[tokenEnum.operator]] = node.getAttribute(tokenEnum[tokenEnum.operator])
      
    for subNode in node.childNodes:
      if subNode.nodeType == xml.dom.Node.ELEMENT_NODE:
        if subNode.tagName in (tokenEnum[tokenEnum.variable], tokenEnum[tokenEnum.path]):
          newObj[tokenEnum[tokenEnum.variable]] = xmlCollapseTextAndCDATA(subNode).strip()
        elif subNode.tagName ==tokenEnum[tokenEnum.operator]:
          newObj[tokenEnum[tokenEnum.operator]] = xmlCollapseTextAndCDATA(subNode).strip()
        elif subNode.tagName == tokenEnum[tokenEnum.value]:
          newObj[tokenEnum[tokenEnum.value]] = xmlCollapseTextAndCDATA(subNode)
        elif subNode.tagName == tokenEnum[tokenEnum.message]:
          newObj[tokenEnum[tokenEnum.message]] = xmlCollapseTextAndCDATA(subNode).strip()
    return newObj


  def __packagesAndPredicatesFromNode(self, dependNode):
    """Process an XML <dependencies> or <incompatibilities> element.  This element is a container whose child nodes are all <package> or <predicate> elements."""
    pkgItems = []
    for node in dependNode.childNodes:
      if node.nodeType == xml.dom.Node.ELEMENT_NODE:
        #
        # Predicate:
        #
        if node.tagName == xmlTokens[xmlTokens.predicate]:
          newPredicate = self.__predicateFromNode(node)
          if newPredicate is not None:
            pkgItems.append(newPredicate)
          else:
            self.pushError('Invalid predicate specification: ' + node.toxml())
        #
        # Package:
        #
        if node.tagName == xmlTokens[xmlTokens.package]:
          pkgId = node.getAttribute(xmlTokens[xmlTokens.id])
          if pkgId:
            pkgItems.append(pkgId)
          else:
            self.pushError('invalid <package> dependency/incompatibility: ' + node.toxml())

    return pkgItems


baseParser.registerParser(('.vpkg', '.vpkg_xml'), xmlParser)
