# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

from baseParser import *


class directoryParser(baseParser):
  """Concrete subclass of baseParser that handles a directory of component files.

  The directory should contain a base package definition file named "__package"
  with an appropriate format extension, e.g.

      __package.vpkg_json

  Individual versions can be named with any base names you want, again with format
  extensions to trigger format-specific reads into the intermediary format:\

      1.0.1-gcc.vpkg_json
      1.0.1-pgi.vpkg_yaml
      original.vpkg

  This class' readIntermediaryDataFromFile method first attempts to create an
  intermediary data structure using the base package definition.  If successful,
  each version file is read and anything from its "versions" dictionary is added
  to the base package's "version" dictionary.

  Since the readIntermediaryDataFromFile method is used, it's important that each
  component file produces the expected data structure:

      { "<pkg-id>": { "versions": { ... } } }

  """

  def readIntermediaryDataFromFile(self):
    baseDir = self.filePath()
    if os.path.isdir(baseDir):
      #
      # At least one base package file must exist:
      #
      basePkgPaths = filter(lambda p:os.path.isfile(p),
                            map(lambda p:os.path.join(baseDir, '__package' + p), baseParser.supportedFileExtensions())
                         );
      if len(basePkgPaths) == 1:
        if basePkgPaths[0] == os.path.join(baseDir, '__package'):
          self.pushError(self.filePath() + ' cannot use a directory base package file (__package)')
        else:
          #
          # Match to a parser:
          #
          aParser = parserForFile(basePkgPaths[0], False)
          if aParser:
            try:
              basePkgData = aParser.readIntermediaryDataFromFile()
            except Exception as e:
              basePkgData = None
              self.pushError(str(e))

            # Absorb any warnings:
            for w in aParser.warningStack():
              self.pushWarning(w)
            # If there are errors, push them and exit now:
            if aParser.hasErrors():
              for e in aParser.errorStack():
                self.pushError(e)
              return None;

            if isinstance(basePkgData, dict) and len(basePkgData) == 1:
              pkgId = basePkgData.keys()[0]

              # If there's no prefix path, then stop now:
              if not tokenEnum[tokenEnum.prefix] in basePkgData[pkgId]:
                self.pushError(self.filePath() + ': no prefix present in base package definition')
                return None

              # Ensure we have a versions dictionary:
              if not tokenEnum[tokenEnum.versions] in basePkgData[pkgId]:
                basePkgData[pkgId][tokenEnum[tokenEnum.versions]] = dict()

              #
              # At this point basePkgData has the skeleton for our final
              # package data structure.  Next, read all additional files
              # in the directory and attempt to parse them:
              #
              (dirpath, dirnames, filenames) = os.walk(baseDir, topdown = True).next()
              for filename in filenames:
                componentPath = os.path.join(self.filePath(), filename)
                name, extension = os.path.splitext(filename)
                if name != '__package':
                  if extension != '':
                    aParser = parserForFile(componentPath, False)
                    if aParser:
                      try:
                        componentData = aParser.readIntermediaryDataFromFile()
                      except Exception as e:
                        componentData = None
                        self.pushWarning(str(e))
                      # Absorb any warnings:
                      for w in aParser.warningStack():
                        self.pushWarning('Warning in underlying component ' + componentPath + ': ' + w)
                      # If there are errors, make them warnings, too:
                      for e in aParser.errorStack():
                        self.pushWarning('Error in underlying component ' + componentPath + ': ' + e)

                      if componentData:
                        if pkgId in componentData:
                          #
                          # If the component has its own "prefix" then we need
                          # to behave differently:
                          #
                          #   relative path:  relative to base package prefix
                          #   absolute path:  if equal to base package prefix, ignore
                          #                   otherwise, use as-is
                          #
                          # Then the versions within the component need to have an explicit
                          # "prefix" set on them using the derived component "prefix":
                          #
                          prefixPath = None
                          if tokenEnum[tokenEnum.prefix] in componentData[pkgId]:
                            prefixPath = componentData[pkgId][tokenEnum[tokenEnum.prefix]]
                            if prefixPath == '' or prefixPath == basePkgData[pkgId][tokenEnum[tokenEnum.prefix]]:
                              prefixPath = None
                            elif not os.path.isabs(prefixPath):
                              prefixPath = os.path.join(basePkgData[pkgId][tokenEnum[tokenEnum.prefix]], prefixPath)
                          #
                          # Now check the component for "versions" to add:
                          #
                          if tokenEnum[tokenEnum.versions] in componentData[pkgId]:
                            for pkgVersId, versData in componentData[pkgId][tokenEnum[tokenEnum.versions]].iteritems():
                              if not pkgVersId in basePkgData[pkgId][tokenEnum[tokenEnum.versions]]:
                                #
                                # Do we need to produce an explicit prefix?
                                #
                                if prefixPath is not None:
                                  if tokenEnum[tokenEnum.prefix] in versData:
                                    # Only modify if it's an absolute path:
                                    if not os.path.isabs(versData[tokenEnum[tokenEnum.prefix]]):
                                      versData[tokenEnum[tokenEnum.prefix]] = os.path.join(prefixPath, versData[tokenEnum[tokenEnum.prefix]])
                                  else:
                                    # Auto-prefix from the version id:
                                    versId = id(pkgVersId)
                                    versData[tokenEnum[tokenEnum.prefix]] = versId.pathRelativeToPrefix(prefixPath)
                                basePkgData[pkgId][tokenEnum[tokenEnum.versions]][pkgVersId] = versData
                              else:
                                self.pushWarning(componentPath + ': repeated component version definition ' + pkgVersId)
                      else:
                        self.pushWarning(componentPath + ': invalid component version definition')
                    else:
                      self.pushWarning(componentPath + ': could not create parser for this component')
                  else:
                    self.pushWarning(componentPath + ': cannot use a directory type for directory components')

              # All done!
              return basePkgData
            else:
              self.pushError(self.filePath() + ': improper package definition in ' + basePkgPaths[0])
          else:
            self.pushError(self.filePath() + ': could not create parser for ' + basePkgPaths[0])

      elif len(basePkgPaths) == 0:
        self.pushError(self.filePath() + ' contains no base package file (__package.<ext>)')
      else:
        self.pushError(self.filePath() + ' contains multiple base package files (__package.<ext>)')
    else:
      self.pushError(self.filePath() + ' is not a directory of component files')
    return None

baseParser.registerParser('.vpkg_dir', directoryParser)
