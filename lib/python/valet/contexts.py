# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

class contexts(set):
  """Configurational context sets

Configuration directives can be configured to affect the environment or not based upon the context of the VALET command.  Contexts are an extension of the VALET 2.0 "development-env" flag.

The contexts class is a subclass of a mutable set that has some convenience methods and bundles some constants for pre-defined contexts and helpful class methods.
  """

  #
  # Pre-defined contexts:
  #
  all = 'all'
  standard = 'standard'
  development = 'development'
  
  #
  # Set of all predefined contexts:
  #
  setOfPredefined = (all, standard, development)
  
  #
  # Default set of contexts assigned to configurational
  # entities:
  #
  default = [all]
  

  @classmethod
  def isPredefinedContext(cls, context):
    """Returns True if theContext is a recognized pre-defined context type."""
    return context in cls.setOfPredefined
  

  def __contains__(self, context):
    """Overriding this method means we can handle special cases, like:

  empty set         implies all, any context matches
  contains 'all'    any context matches
    """
    if isinstance(context, contexts):
      context = set(context)
    if isinstance(context, set):
      # An empty incoming context set means no match is possible:
      if len(context) == 0:
        return False
      # An incoming context containing 'all' automatically means a match:
      if self.__class__.all in context:
        return True
      # If the incoming context overlaps at all with us, that implies a match:
      if len(context & self) > 0:
        return True
    elif context == contexts.all:
      # Incoming scalar context is 'all' implies a match:
      return True
    return len(self) == 0 or super(contexts, self).__contains__(self.__class__.all) or super(contexts, self).__contains__(context)


if __name__ == '__main__':
  s1 = contexts(['development', 'blas'])
  s2 = contexts([contexts.all])
  print str(s1) + ' in ' + str(s2) + ' = ' + str(s1 in s2)
  print str(s2) + ' in ' + str(s1) + ' = ' + str(s2 in s1)
  
  print 'development in ' + str(s1) + ' = ' + str('development' in s1)
  print 'development in ' + str(s2) + ' = ' + str('development' in s2)
  
  s2 = contexts()
  print ''
  print 'retry with empty context:'
  print ''
  print str(s1) + ' in ' + str(s2) + ' = ' + str(s1 in s2)
  print str(s2) + ' in ' + str(s1) + ' = ' + str(s2 in s1)
  
  print 'development in ' + str(s1) + ' = ' + str('development' in s1)
  print 'development in ' + str(s2) + ' = ' + str('development' in s2)
