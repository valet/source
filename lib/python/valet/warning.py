# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import re
from config import *
from action import *


class warning(action):
  """An informational message to be displayed when package/version is added to the environment."""

  ## warning constructor
  #
  def __init__(self, message, ctxts = []):
    """Constructor
    
The message to be displayed should be passed to the contructor.  A list/set/tuple/contexts instance containing all contexts for which the warning is enabled can be passed, too.

An optional ctxts argument is permissible, too.
    """
    super(warning, self).__init__(ctxts)
    self._message = message
  
  
  def clone(self):
    return warning(self._message, self.contexts)


  def __str__(self):
    outStr = 'WARNING("' + self._message + '")'
    contextStr = super(warning, self).__str__()
    if contextStr:
      outStr = outStr + ' ' + contextStr
    return outStr


  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(warning, self).__repr__())
    return s + ', message="' + self._message.replace('"', '\\"') + '")'


  def __hash__(self):
    return hash(self._message)


  def __cmp__(self, other):
    """Object comparator
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return 0
    if self.message() < other.message():
      return -1
    if self.message() == other.message():
      return 0
    return +1
    
  def __lt__(self,other):
    """Object less-than
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return False
    return (self.message() < other.message())
    
    
  def __le__(self,other):
    """Object less-than equal-to
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return False
    return (self.message() <= other.message())


  def __gt__(self,other):
    """Object greater-than
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return True
    return (self.message() > other.message())


  def __ge__(self,other):
    """Object greater-than equal-to
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return True
    return (self.message() >= other.message())
    
    
  def __eq__(self,other):
    """Object equality
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return False
    return (self.message() == other.message())
    
    
  def __ne__(self,other):
    """Object inequality
    
Simple string comparison against another warning instance.
    """
    if not isinstance(other, warning):
      return True
    return (self.message() != other.message())


  def message(self):
    """Returns this instance's warning message."""
    return self._message


  def actionStringWithShellHelper(self, shellHelper, currentContext = contexts.all):
    """Return a configuration fragment string that affects this instance's display of a message."""
    
    # Promote an atomic currentContext to being a set:
    if isinstance(currentContext, basestring):
      currentContext = set(currentContext)
    
    if currentContext in self.contexts:
      return shellHelper.wrapCommand('echo WARNING: \'' + shellHelper.escapeString(self.message(), True) + '\'')
    return ''
    
#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  x = warning('This is just a test.', ['barf', 'puke'])
  print x
  print
  print x.__repr__()
  print
  
