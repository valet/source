# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import sys
import os
import glob
import cPickle
from config import *
from contexts import *
from id import *
from parser import *


class packageManager(object):
  """Package loader

An instance of the packageManager class is used to locate and load packages from their definition files.  A packageManager can produce lists of all packages and all versions of a package (optionally filtered by a context).

The class also includes a method to simply load a package defintion file and display any errors or warnings that are received during parsing.  This method is used by the vpkg_check utility to verify the integrity of a definition file.
  """
  
  _sharedPackageManager = None
  @classmethod
  def sharedPackageManager(cls):
    if not cls._sharedPackageManager:
      cls._sharedPackageManager = packageManager()
    return cls._sharedPackageManager
    

  def __init__(self):
    """Constructor

Initialize the aggregate error and warning string lists.
    """
    self._errorStrings = []
    self._warningStrings = []


  def __packagePathsWithIdString(self, pkgIdStr):
    """Returns a list of paths corresponding to the given package id string.  For each directory in the list of configuration directories, any files present that are named with the value of pkgIdStr and any of the supported file extensions are included.  The list is ordered first according to the configuration directory order and by supported extension within each directory."""
    validExtensions = baseParser.supportedFileExtensions()
    confPaths = list()
    for path in config.sysConfDirList():
      confPaths.extend(
          filter(
              lambda p:os.path.exists(p),
              map(lambda p:os.path.join(path, pkgIdStr + p), validExtensions)
            )
        )
    return confPaths


  def __packagePathsWithRegex(self, pkgIdRegex):
    """Returns a list of paths corresponding to the given package id pattern.  For each directory in the list of configuration directories, any files present that are named with any of the supported file extensions and whose non-extension portion match pkgIdRegex are included.  The list is ordered first according to the configuration directory order and by supported extension within each directory."""
    validExtensions = baseParser.supportedFileExtensions()
    confPaths = list()
    for path in config.sysConfDirList():
      #
      # Walk all files in the directory:
      #
      (dirpath, dirnames, filenames) = os.walk(path, topdown = True).next()
      for filename in filenames:
        name, extension = os.path.splitext(filename)
        if extension in validExtensions and pkgIdRegex.match(name) is not None:
          confPaths.append(os.path.join(path, filename))
    return confPaths
  
  
  def __loadPackageWithPath(self, pkgPath):
    """Private method.  Attempt to load the package definition at pkgPath.  If the package fails to load, then any warnings or errors held by the parser are appended to this instance's warning and error string lists and None is returned."""
    if os.path.exists(pkgPath):
      #
      # Is there a cached binary form newer than the source?
      #
      cachePath = os.path.join(os.path.dirname(pkgPath), '.' + os.path.basename(pkgPath) + '.cache')
      if config.isCachingEnabled and os.path.exists(cachePath):
        if os.path.isdir(pkgPath):
          srcMTime = max(os.path.getmtime(d) for d,_,_ in os.walk('/tmp/x'))
        else:
          srcMTime = os.path.getmtime(pkgPath)
        if os.path.getmtime(cachePath) > srcMTime:
          try:
            cacheFile = open(cachePath, 'r')
            pkg = cPickle.load(cacheFile)
            cacheFile.close()
            if pkg is not None:
              return pkg
          except Exception as e:
            pass
      
      try:
        # Find a parser:
        pkgParser = parserForFile(pkgPath)
        if pkgParser:
          if pkgParser.parsedPackage() is not None:
            #
            # We got a package object, hooray!
            #
            pkg = pkgParser.parsedPackage()
            
            #
            # Attempt to cache the updated binary form:
            #
            if config.isCachingEnabled:
              try:
                cacheFile = open(cachePath, 'w')
                cPickle.dump(pkg, cacheFile)
                cacheFile.close()
              except Exception as e:
                pass
            
            return pkg
          #
          # Something bad happened...
          #
          if config.isVerbose() and pkgParser.hasWarnings():
            self._warningStrings.extend(pkgParser.warningStack())
          if pkgParser.hasErrors():
            self._errorStrings.extend(pkgParser.errorStack())
        else:
          self._errorStrings.append('ERROR:   unable to allocate parser for ' + pkgPath)
      except Exception as e:
        self._errorStrings.append('ERROR:   ' + str(e))
    else:
      self._errorStrings.append('ERROR:  File not found: ' + pkgPath)
    return None
    

  def checkPackageAtPath(self, pkgPath):
    """Attempt to parse the package definition at pkgPath.  Returns True if successful.   Otherwise, any warnings or errors held by the parser are displayed and False is returned."""
    if os.path.exists(pkgPath):
      try:
        # Find a parser:
        pkgParser = parserForFile(pkgPath)
        if pkgParser:
          if config.isVerbose():
            print 'INFO:    Parser chosen for ' + pkgPath + ' was ' + pkgParser.__class__.__name__
          if config.isVerbose() and pkgParser.hasWarnings():
            for warning in pkgParser.warningStack():
              print 'WARNING: ' + str(warning)
          if pkgParser.hasErrors():
            for error in pkgParser.errorStack():
              print 'ERROR:   ' + str(error)
          else:
            return pkgParser.parsedPackage()
        else:
          self._errorStrings.extend('ERROR:   Unable to allocate parser for ' + pkgPath)
      except Exception as e:
        print 'ERROR:   ' + str(e)
    else:
      print 'ERROR:   File not found: ' + pkgPath
    return False


  def _packagesWithPattern(self, pkgIdPattern, context = contexts.standard):
    """Given a pattern (regex) string pkgIdPattern, find any package definitions that match the pattern and attempt to load them.  Returns the resulting list of package objects, or None if no matching packages were found."""
    pkgPaths = self.__packagePathsWithRegex(re.compile(pkgIdPattern))
    if pkgPaths:
      pkgs = filter(
                lambda p:p is not None and context in p.contexts,
                map(
                  lambda p:self.__loadPackageWithPath(p),
                  pkgPaths
                  )
              )
      if len(pkgs) > 0:
        return pkgs
    return None


  def packagesWithId(self, pkgId, context = contexts.standard):
    """Given a pkgId (as a string, an id, or an atom), find any package definitions with that id and attempt to load them.  Returns the resulting list of package objects, or None if no matching packages were found.
    
If pkgId is a string and is a valid package id pattern string, the _packagesWithPattern method will be used.
    """
    if isinstance(pkgId, basestring):
      try:
        pkgId = id(pkgId)
      except:
        # The pkgId might be a pattern-based format:
        m = id.compiledCmpRegex().match(pkgId)
        if m:
          return self._packagesWithPattern(m.group(1), context)
        raise ValueError('Invalid package id (badly formatted): ' + pkgId)
    
    if isinstance(pkgId, id):
      pkgPaths = self.__packagePathsWithIdString(str(pkgId.packageId()))
    elif isinstance(pkgId, atom):
      pkgPaths = self.__packagePathsWithIdString(str(pkgId))
    else:
      raise ValueError('Invalid package id (wrong object type): ' + str(type(pkgId)))

    if pkgPaths:
      pkgs = filter(
                lambda p:p is not None and context in p.contexts,
                map(
                  lambda p:self.__loadPackageWithPath(p),
                  pkgPaths
                  )
              )
      if len(pkgs) > 0:
        return pkgs
    return None
    
  
  def _packageVersionWithPattern(self, regexMatch, context):
    #
    # From id.compiledCmpRegex, the parenthesized groups are:
    #
    #  1         package component
    #  3         version component
    #  5         feature list component
    #
    # First, lets see if we're doing a simple package load:
    #
    try:
      pkgId = atom(regexMatch.group(1))
      pkgs = self.packagesWithId(pkgId, context)
    except:
      #
      # Package id must be a pattern, so grab all packages whose
      # package id match that regex:
      #
      pkgs = self._packagesWithPattern(regexMatch.group(1), context)
    
    if pkgs:
      #
      # Okay, we got one or more packages that matched.  Now, let's
      # filter by version id (and context):
      #
      versions = list()
      for pkg in pkgs:
        versions.extend(pkg.filterVersions(byContext = context, byId = regexMatch.group(0)))
      
      if versions:
        #
        # We got some matching versions.  If there's only one or the
        # configuration says to "return the first match" then do so now:
        #
        if len(versions) == 1 or config.featureMatchingMode == featureMatchingModes.first_match:
          return versions[0]
          
        #
        # Least or most matching features?
        #
        outVersion = None
        if config.featureMatchingMode == featureMatchingModes.least_additional:
          matchCount = float('inf')
          for version in versions:
            vId = version.id()
            if len(vId.features()) < matchCount:
              outVersion = version
              matchCount = len(vId.features())
        else:
          matchCount = float('-inf')
          for version in versions:
            vId = version.id()
            if len(vId.features()) > matchCount:
              outVersion = version
              matchCount = len(vId.features())
        return outVersion
        
    return None
    

  def packageVersionWithId(self, pkgId, context = contexts.standard):
    if isinstance(pkgId, basestring):
      try:
        pkgId = id(pkgId)
      except:
        # The pkgId might be a pattern-based format:
        m = id.compiledCmpRegex().match(pkgId)
        if m:
          return self._packageVersionWithPattern(m, context)
        raise ValueError('Invalid package id (badly formatted): ' + pkgId)
    elif not isinstance(pkgId, id):
      raise ValueError('Invalid package id (wrong object type): ' + str(type(pkgId)))
        
    # Get the list of packages for the packageId:
    pkgs = self.packagesWithId(pkgId.packageId(), context)
    if pkgs is not None:
      #
      # If the versionId is not set, then walk each returned package,
      # each time grabbing the default version.  If a package's default
      # version does NOT include the current context, move on to the
      # next.  If it does, return that version:
      #
      if pkgId.versionId() is None or str(pkgId.versionId()).lower() == 'default':
        for thePkg in pkgs:
          theVersion = thePkg.defaultVersion()
          if context in theVersion.contexts:
            return theVersion
        return None
      
      #
      # Use the package class's best-match logic to find the very best
      # match to the incoming pkgId.
      #
      m = [-1.0, None]
      for pkg in pkgs:
        v = pkg.getVersionAndRankWithId(pkgId, context)
        if v is not None and v[0] > m[0]:
          m = v
      if m[0] >= 0.0:
        return m[1]
    return None
    

  def versionList(self, pkgIdAtom, context = contexts.standard):
    pkgs = self.packagesWithId(pkgIdAtom, context)
    if pkgs:
      versList = list()
      for pkg in pkgs:
        versList.extend(map(
                      lambda v: v.id(),
                      pkg.filterVersions(byContext = context)
                    )
                  );
      versList.sort()
      return versList
    return None
    

  def packageIdList(self, unify = False, context = contexts.standard):
    outList = None
    validExtensionPatterns = map(lambda p:'*' + p, baseParser.supportedFileExtensions())
    
    if unify:
      pkgIds = set()
      #
      # Walk all config directory paths:
      #
      for basePath in config.sysConfDirList():
        #
        # Walk each globbing pattern:
        #
        for basePattern in validExtensionPatterns:
          #
          # For each globbed path match attempt to load the package,
          # filter based on the context, and pull the package id:
          #
          pkgIds.update(map(
              lambda p: p.id(),
              filter(
                  lambda p: p and context in p.contexts,
                  map(
                      lambda p: self.__loadPackageWithPath(p),
                      glob.glob(os.path.join(basePath, basePattern))
                    )
                )
            ))
      outList = list(pkgIds)
      outList.sort()
    else:
      outList = {}
      #
      # Walk all config directory paths:
      #
      for basePath in config.sysConfDirList():
        pkgIds = set()
        #
        # Walk each globbing pattern:
        #
        for basePattern in validExtensionPatterns:
          #
          # For each globbed path match attempt to load the package,
          # filter based on the context, and pull the package id:
          #
          pkgIds.update(map(
              lambda p: p.id(),
              filter(
                  lambda p: p and context in p.contexts,
                  map(
                      lambda p: self.__loadPackageWithPath(p),
                      glob.glob(os.path.join(basePath, basePattern))
                    )
                )
            ))
        if pkgIds:
          pkgIds = list(pkgIds)
          pkgIds.sort()
          outList[basePath] = pkgIds

    return outList


  def warningStrings(self):
    return self._warningStrings


  def errorStrings(self):
    return self._errorStrings
    

