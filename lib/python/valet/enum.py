# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
import numbers


class enum(object):
  """Analog to C enums
  
A class that behaves like an enumerated type.  When an instance is created a list/set/tuple should be passed to the constructor.  The constructor iterates over the list/set/tuple, associating with each value an increasing integer value (starting from zero).  If the bitwise argument to the constructor is True, then each value is instead assigned an increasing power of two, starting from 2^0.

The argument can also be a dict, in which case the keys are expected to be the specific integer values that should be assigned to the corresponding value in the dict.  This provides a way to ensure a specific numbering scheme:  e.g. in instances where the enum value represents a bit mask.
  """

  def __init__(self, tokens = [], bitwise = False):
    """Constructor

Instantiating an enum() object requires a list, set, or tuple of strings.  The
list/set/tuple is iterated, and each string is assigned a unique integer value.
If the bitwise argument is False (default), the integers increase from zero by
the addition of one; if bitwise is True, the integers start at 1 and increase as
powers of two.

The tokens argument can also be a dict containing number => string key-value
pairs.  The number key is used as the integer assigned to each string.
    """
    self._tokens = dict()
    self._max = 0
    if isinstance(tokens, list) or isinstance(tokens, set) or isinstance(tokens, tuple):
      #
      # Initialization is deterministic, based on the
      # order of the incoming list.
      #
      i = 1 if bitwise else 0
      for token in tokens:
        if not isinstance(token, basestring):
          continue
        if not token in self._tokens:
          self._tokens[token] = i
          self._tokens[i] = token
          i = (i * 2) if bitwise else (i + 1)
        else:
          raise RuntimeError('Repeated token in enum: ' + token)
      self._max = i
    elif isinstance(tokens, dict):
      for idx, token in tokens.iteritems():
        if not isinstance(idx, numbers.Number):
          continue
        if not isinstance(token, basestring):
          continue
        if not token in self._tokens:
          self._tokens[token] = int(idx)
          self._tokens[int(idx)] = token
          self._max += 1
        else:
          raise RuntimeError('Repeated token in enum: ' + token)
    
    if self._max == 0:
      raise ValueError('Invalid enum() instance: no values enumerated')
        
            
  def __len__(self):
    return self._max
  
  
  def __getitem__(self, key):
    if key in self._tokens:
      return self._tokens[key]
    raise KeyError('No token/index ' + str(key) + ' in enumeration.')
  
  
  def __getattr__(self, name):
    if name in self._tokens:
      return self._tokens[name]
    raise AttributeError('No token ' + name + ' in enumeration.')
  
  
  def __iter__(self):
    i = 0
    while i < self._max:
      yield self._tokens[i]
      i = i + 1
  
  
  def __contains__(self, item):
    return item in self._tokens
  
  
  def __repr__(self):
    s = self.__class__.__name__ + '({ '
    comma = ''
    for idx in sorted(filter(lambda e:isinstance(e, numbers.Number), self._tokens)):
      s = s + comma + str(idx) + ':\'' + self._tokens[idx] + '\''
      comma = ', '
    s = s + ' })'
    return s
  
  
  def asString(self, item):
    """Given either a string or a number argument, item, return the string form of the enumeration token.  If item is not valid raises a KeyError exception."""
    if isinstance(item, numbers.Number):
      if item in self._tokens:
        return self._tokens[item]
      raise KeyError('No index ' + str(item) + ' in enumeration.')
    if isinstance(item, basestring):
      if item in self._tokens:
        return item
      raise KeyError('No token ' + str(item) + ' in enumeration.')
    raise KeyError('Invalid token type: ' + str(type(item)))


#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  e = enum(['development', 'standard', 'all'], bitwise=True)
  print e[1]
  print e['development']
  print e.standard
  print e.asString('all')
  
  print
  e = enum({1:'verbose', 2:'dynamic', 4:'threaded'})
  print e
  
  print
  print enum.__init__.__doc__
  
