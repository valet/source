# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import urlparse
import os
from id import *
from predicate import *
from configEntity import *


def indentString(s, indentUsing):
  """Prefix every line of s with the string indentUsing."""
  outStr = indentUsing + s.replace(os.linesep, os.linesep + indentUsing)
  if s and s[-1] == os.linesep:
    # Remove the trailing indentUsing
    outStr = outStr[:-len(indentUsing)]
  return outStr


class basePackage(configEntity):
  """Common functionality for a package as well as a version of a package (and a version alias).

Instances of basePackage should not be instantiated directly.  Only instances of its subclasses have any real value.

An instance contains lists of incompatiblities and dependencies:  members of these lists may be strings, instances of the id class, or instances of the predicate class.

An instance also contains a list of actions to be taken if this package/version is chosen.  Members of that list should be instances of subclasses of the action class.
  """

  #
  # Pre-defined flags:
  #
  shouldNotAddStandardPathsFlag = 'no-standard-paths'
  shouldNotAddDevStandardPathsFlag = 'no-development-env'


  def __init__(self, pkgId, prefix = None, description = None, referenceURL = None, flags = None, incompatibilities = None, dependencies = None, actions = None, ctxts = []):
    """Constructor

The pkgId is mandatory.  All other fields are optional and can be configured after instantiation.
    """
    super(basePackage, self).__init__(ctxts)
    
    self.setId(pkgId)
    self._label = 'package'

    self.setPrefix(prefix)
    self.setDescription(description)
    self.setReferenceURL(referenceURL)
    self.setFlags(flags)

    self._incompatibilities = []
    if incompatibilities is not None:
      for incompat in incompatibilities:
        self.addIncompatibility(incompat)
        
    self._dependencies = []
    if dependencies is not None:
      for depend in dependencies:
        self.addDependency(depend)
        
    self._actions = []
    if actions is not None:
      for act in actions:
        self.addAction(act)
        
    self._attributes = {}
        
        
  def __str__(self):
    s = 'basePackage(' + str(self.id())
    if self.prefix() is not None:
      s += '; prefix: ' + self.prefix()
    if self.description() is not None:
      s += '; description: ' + self.description()
    if self.referenceURL() is not None:
      s += '; referenceURL: ' + str(self.referenceURL())
    if len(self.flags()) > 0:
      s += '; flags: (' + ','.join(map(lambda t:str(t), self.flags())) + ')'
    if len(self.incompatibilities()) > 0:
      s += '; incompatibilities: (' + ','.join(map(lambda t:str(t), self.incompatibilities())) + ')'
    if len(self.dependencies()) > 0:
      s += '; dependencies: (' + ','.join(map(lambda t:str(t), self.dependencies())) + ')'
    print self.actions()
    if len(self.actions()) > 0:
      s += '; actions: (' + ','.join(map(lambda t:str(t), self.actions())) + ')'
    s += ')'
    contextStr = super(basePackage, self).__str__()
    if contextStr:
      s = s + ' ' + contextStr
    return s
  
  
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(basePackage, self).__repr__())
    s += ', pkgId=\"' + str(self._id) + '"'
    if self._prefix is not None:
      s += ', prefix="' + self._prefix.replace('"', '\\"') + '"'
    if self._description is not None:
      s += ', description="' + self._description.replace('"', '\\"') + '"'
    if self._referenceURL is not None:
      s += ', referenceURL="' + str(self._referenceURL).replace('"', '\\"') + '"'
    if len(self._flags) > 0:
      s += ', flags=' + self._flags.__repr__()
    if len(self._incompatibilities) > 0:
      s += ', incompatibilities=' + self._incompatibilities.__repr__()
    if len(self._dependencies) > 0:
      s += ', dependencies=' + self._dependencies.__repr__()
    if len(self._actions) > 0:
      s += ', actions=' + self._actions.__repr__()
    return s + ')'


  def isPackage(self):
    """Meta data:  instance is a package object?"""
    return False
    
    
  def isPackageVersion(self):
    """Meta data:  instance is a versioned package object?"""
    return False
    
    
  def isPackageVersionAlias(self):
    """Meta data:  instance is an alias to a versioned package?"""
    return False


  def isValid(self):
    """An id is the only requirement for a package to be valid."""
    return self._id is not None


  def id(self):
    """Return this instance's id."""
    return self._id
    
    
  def setId(self, pkgId):
    """Set this instance's id.

Note that at this level of the class hierarchy we simply use whatever we've been handed; subclasses can override (and chain to) this implemention to validate the pkgId before setting it.
    """
    if pkgId is None or isinstance(pkgId, id):
      self._id = pkgId


  def prefix(self):
    """Return this instance's path prefix."""
    return self._prefix;
    
    
  def setPrefix(self, prefix):
    """Set this instance's path prefix.  The path does not have to exist (or even be an appropriate path)."""
    if prefix is None or isinstance(prefix, basestring):
      self._prefix = prefix;


  def description(self):
    """Return this instance's description string."""
    return self._description
    
    
  def setDescription(self, description):
    """Set this instance's description string."""
    if description is None or isinstance(description, basestring):
      self._description = description;


  def referenceURL(self):
    """Return this instance's reference URL."""
    return self._referenceURL
    
    
  def setReferenceURL(self, referenceURL):
    """Set this instance's reference URL.  The referenceURL argument is validated to ensure that it is a valid http/https-scheme URL; if not, a ValueError exception is raised.

A result from urlparse is also acceptable, but again the scheme must be http/https or else a ValueError exception will be raised.
    """
    if referenceURL is None:
      self._referenceURL = None
    else:
      if isinstance(referenceURL, basestring):
        referenceURL = urlparse.urlparse(referenceURL)
      elif not isinstance(referenceURL, urlparse.ParseResult):
        raise ValueError('basePackage.setReferenceURL expects a string or urlparse.ParseResult.')
      
      if referenceURL.scheme == 'http' or referenceURL.scheme == 'https':
        self._referenceURL = urlparse.urlunparse(referenceURL)
      else:
        raise ValueError('basePackage.setReferenceURL expects a valid http/https URL.')


  def flags(self):
    """Return this instance's set of flags."""
    return self._flags
    
    
  def setFlags(self, flags):
    """Set this instance's set of flags to contain all unique members of the flags collection."""
    self._flags = set()
    if flags is not None:
      for flag in flags:
        self.setFlag(flag)
        
        
  def hasFlag(self, flag):
    """Return True if this instance's set of flags contains the given flag."""
    return flag in self._flags if flag else False
    
    
  def setFlag(self, flag):
    """Add flag to this instance's set of flags."""
    if flag:
      self._flags.add(flag)
      
      
  def removeFlag(self, flag):
    """Remove flag to this instance's set of flags."""
    if flag:
      self._flags.discard(flag)
      
  
  def coallescedFlags(self):
    """Subclasses which form parent-child relationships should override this method."""
    return self._flags
  
  
  def coallescedFlagsIterator(self, filterCallable = None):
    """Return an iterator over this instance's coallesced flags."""
    if filterCallable is not None:
      return iter(filter(filterCallable, self.coallescedFlags()))
    return iter(self.coallescedFlags())


  def label(self):
    """Return this instance's label string."""
    return self._label
    
    
  def setLabel(self, aLabel):
    """Set this instance's label string to aLabel."""
    self._label = None if not aLabel else str(aLabel)


  def incompatibilities(self):
    """Return this instance's list of incompatiblities."""
    return self._incompatibilities
    
    
  def incompatibilitiesIterator(self):
    """Return an iterator over this instance's list of incompatiblities."""
    return iter(self._incompatibilities)
    
    
  def addIncompatibility(self, incompatibility):
    """If incompatibility is a string, id, or predicate, append to this instance's list of incompatibilities.  Otherwise, a ValueError exception is raised."""
    if isinstance(incompatibility, basestring) or isinstance(incompatibility, id) or isinstance(incompatibility, predicate):
      self._incompatibilities.append(incompatibility)
    else:
      raise ValueError('basePackage.addIncompatibility expects a regex string, id, or predicate.')
  
  
  def coallescedIncompatibilities(self):
    """Subclasses which form parent-child relationships should override this method."""
    return self._incompatibilities
    
    
  def coallescedIncompatibilitiesIterator(self, filterCallable = None):
    """Return an iterator over this instance's coallesced incompatibilities."""
    if filterCallable is not None:
      return iter(filter(filterCallable, self.coallescedIncompatibilities()))
    return iter(self.coallescedIncompatibilities())
    

  def dependencies(self):
    """Return this instance's list of dependencies."""
    return self._dependencies
    
    
  def dependenciesIterator(self):
    """Return an iterator over this instance's list of dependencies."""
    return iter(self._dependencies)
    
    
  def addDependency(self, dependency):
    """If dependency is a string, id, or predicate, append to this instance's list of dependencies.  Otherwise, a ValueError exception is raised."""
    if isinstance(dependency, basestring) or isinstance(dependency, id) or isinstance(dependency, predicate):
      self._dependencies.append(dependency)
    else:
      raise ValueError('basePackage.addDependency expects a regex string, id, or predicate.')
      
      
  def coallescedDependencies(self):
    """Subclasses which form parent-child relationships should override this method."""
    return self._dependencies
    
    
  def coallescedDependenciesIterator(self, filterCallable = None):
    """Return an iterator over this instance's coallesced dependencies."""
    if filterCallable is not None:
      return iter(filter(filterCallable, self.coallescedDependencies()))
    return iter(self.coallescedDependencies())


  def actions(self):
    """Return this instance's list of actions."""
    return self._actions
    
    
  def actionsIterator(self):
    """Return an iterator over this instance's list of actions."""
    return iter(self._actions)
    
    
  def addAction(self, theAction):
    """If theAction is an instance of a subclass of the action class, append to this instance's list of actions.  Otherwise, a ValueError exception is raised."""
    if isinstance(theAction, action):
      self._actions.append(theAction)
    else:
      raise ValueError('basePackage.addAction expects a subclass of the action class.')
  
  
  def coallescedActions(self):
    """Subclasses which form parent-child relationships should override this method."""
    return self._actions
  
  
  def coallescedActionsIterator(self, filterCallable = None):
    """Return an iterator over this instance's coallesced actions."""
    if filterCallable is not None:
      return iter(filter(filterCallable, self.coallescedActions()))
    return iter(self.coallescedActions())
    
  
  def attributeCount(self):
    """Return the number of attributes associated with this instance."""
    return len(self._attributes)
  
  
  def attributes(self):
    """Return this instance's attribute dictionary."""
    return self._attributes
    
  
  def attributesKeys(self):
    """Return a list of the keys present in this instance's attribute dictionary."""
    return self._attributes.keys()
    
  
  def attributesKeysIterator(self):
    """Return an interator over the keys in this instance's attribute dictionary."""
    return self._attributes.iterkeys()
    
  
  def attributesValuesIterator(self):
    """Return an interator over the values in this instance's attribute dictionary."""
    return self._attributes.itervalues()
  
  
  def attributesIterator(self):
    """Return an iterator over this instance's attribute dictionary."""
    return self._attributes.iteritems()
  
  
  def attributeValue(self, attr_key):
    """Return the value associated with a given attribute key."""
    if attr_key in self._attributes:
      return self._attributes[attr_key]
    return None
  
  
  def setAttributeValue(self, attr_key, attr_value = None):
    """Set the value associated with a given attribute key.  If attr_value is None (the default) the key is removed from the attribute dictionary."""
    try:
      attr_key = str(attr_key)
    except:
      raise ValueError('invalid key type for attribute')
      
    if attr_value is None:
      self._attributes.pop(attr_key, None)
    else:
      try:
        self._attributes[attr_key] = str(attr_value)
      except:
        raise ValueError('invalid value type for attribute ' + attr_key)
  
  
  def addAttributes(self, attributes):
    """Update the instance's attribute dictionary with key-value pairs present in the given dictionary."""
    for k,v in attributes.iteritems():
      self.setAttributeValue(k, v)
      
  
  def clearAttributes(self):
    """Clear the instance's attribute dictionary."""
    self._attributes.clear()
  
  
  def setAttributes(self, attributes):
    """Clear the instance's attribute dictionary and add all key-value pairs present in the given dictionary."""
    self.clearAttributes()
    self.addAttributes(attributes)
  
  
  def coallescedAttributes(self):
    """Subclasses which form parent-child relationships should override this method."""
    return self._attributes
  
      
  def infoString(self):
    """Return a string describing this instance for the sake of vpkg_info.  All subclasses will take this string and modify it in some way to adapt it to their particular implementation."""
    s = ''
    if len(self.contexts) > 0:
      s += 'contexts: ' + ','.join(self.contexts) + os.linesep
    
    if len(self.flags()) > 0:
      s += 'flags: ' + ','.join(self.flags()) + os.linesep

    if len(self.actions()) > 0:
      s += 'actions: {' + os.linesep
      for act in self.actions():
        s += indentString(str(act) + os.linesep, '  ')
      s += '}' + os.linesep

    if len(self.dependencies()) > 0:
      s += 'dependencies: {' + os.linesep
      for depend in self.dependencies():
        s += indentString(str(depend) + os.linesep, '  ')
      s += '}' + os.linesep

    if len(self.incompatibilities()) > 0:
      s += 'incompatibilities: {' + os.linesep
      for incompat in self.incompatibilities():
        s += indentString(str(incompat) + os.linesep, '  ')
      s += '}' + os.linesep
    
    if len(self.attributes()) > 0:
      s += 'attributes: {' + os.linesep
      for k, v in self.attributesIterator():
        s += indentString(k + ' = ' + v + os.linesep, '  ')
      s += '}' + os.linesep

    return s


if __name__ == '__main__':
  from envVar import *
  
  p = basePackage(pkgId='valet', description='VALET Automates Linux Environment Tasks', flags=['standard-paths', 'blotto', 'blippo'], actions=[envVar('PYTHONPATH', opType.opTypeEnum.prependPath, '"/usr/lib64/python"', [contexts.standard])])
  print p
  print
  print p.__repr__()
  print

