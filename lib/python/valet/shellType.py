# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
import re
import zlib
import base64
import pickle
from config import *
from enum import *

## @package shellType
#
# Enumeration of shell types that we handle.
#
class shellType:
  """Bundles together the enumeration of the shell types that VALET understands with some helper functions and string-to-integer mappings."""

  #
  # Enumerate the shell types:
  #
  shellTypeEnum   = enum(['invalid', 'any', 'sh', 'bash', 'csh'])

  #
  # Mapping of possible shell strings to the
  # enumerated types:
  #
  stringMapping   = {
                      '*': shellTypeEnum.any,
                      'any': shellTypeEnum.any,
                      'sh': shellTypeEnum.sh,
                      '/bin/sh': shellTypeEnum.sh,
                      'bash': shellTypeEnum.bash,
                      '/bin/bash': shellTypeEnum.bash,
                      'csh': shellTypeEnum.csh,
                      '/bin/csh': shellTypeEnum.csh
                    };


  @classmethod
  def isValidShellType(cls, shellType):
    """Returns True if the integer shellType is one of the enumerated shell types."""
    return (isinstance(shellType, int) or isinstance(shellType, long)) and shellType in cls.shellTypeEnum


  @classmethod
  def shellTypeWithString(cls, shellString):
    """If the shellString is one of the names -- canonical or otherwise -- that VALET recognizes, returns the corresponding value from the enumerated shell types."""
    if isinstance(shellString, basestring) and shellString in cls.stringMapping:
      return cls.stringMapping[shellString]
    return cls.shellTypeEnum.invalid


  @classmethod
  def stringWithShellType(cls, shellType):
    """Returns the canonical type name string for the given value from the enumerated shell types."""
    if (isinstance(shellType, int) or isinstance(shellType, long)) and shellType in cls.shellTypeEnum:
      return cls.shellTypeEnum[shellType]
    return None


  @classmethod
  def shellHelperForShellType(cls, shellType):
    """Factory:  returns a shellHelper subclass instance for the given shellType from the enumerated shell types."""
    if isinstance(shellType, basestring):
      shellType = cls.shellTypeWithString(shellType)
    if shellType == cls.shellTypeEnum.sh:
      return shShellHelper()
    if shellType == cls.shellTypeEnum.bash:
      return bashShellHelper()
    if shellType == cls.shellTypeEnum.csh:
      return cshShellHelper()
    return None


class shellHelper:
  """Per-shell helper functions
  
Abstract base class for a class hierarchy that handles string escaping, environment variable modifications, etc., for each kind of shell VALET supports.

Concrete subclasses must ensure that each of the constituent methods is implemented.
  """
  
  
  def matchShellTypes(self):
    """Returns the shell type(s) which this class implements.  The return value is an ordered list of types (from most to least significant)."""
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def scriptingHeader(self):
    """Returns a string that will be added to the beginning of all shell statements produced by VALET.  Useful for adding any helper functions into the shell environment that the rest of the VALET statements may use.

For example, a function to remove a path from the PATH environment variable.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def scriptingFooter(self):
    """Returns a string that will be added to the end of all shell statements produced by VALET.  Useful for removing any helper functions from the shell environment that were added by the scriptingHeader() method."""
    raise NotImplementedError('The shellHelper class is an abstract base class.')

  
  def escapeString(self, str, allowEnvSubs=True):
    """Escape a string that will be added to a VALET shell configuration fragment. The string will be enclosed in whatever kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them (e.g. for sh that would be single quotes).  Any variable references are assumed to be of the form ${VARIABLE_NAME} and should be moved outside the quoting context, e.g. for sh:

  '"${VARIABLE_NAME}"'

The single quote moves outside the implicit quoting that VALET adds; then the variable is added, inside double quotes; finally, another single quote moves back inside the implicit quoting that VALET adds.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')
  

  def unsetEnvVar(self, variable):
    """Return a configuration fragment string that removes a variable from the environment."""
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def setValueOfEnvVar(self, variable, value, allowEnvSubs=True):
    """Return a configuration fragment string that sets the value of an environment variable.  The value should be quoted using the kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them.

If allowEnvSubs is true, any references to environment variables in value (as ${VARIABLE_NAME}) are written such that the shell will perform substitution on them.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def appendValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    """Return a configuration fragment string that appends a value to an environment variable.  The value should be quoted using the kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them.

The delimiter is used to separate the new value from any existing value of the variable.

If allowEnvSubs is true, any references to environment variables in value (as ${VARIABLE_NAME}) are written such that the shell will perform substitution on them.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def prependValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    """Return a configuration fragment string that prepends a value to an environment variable.  The value should be quoted using the kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them.

The delimiter is used to separate the new value from any existing value of the variable.

If allowEnvSubs is true, any references to environment variables in value (as ${VARIABLE_NAME}) are written such that the shell will perform substitution on them.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')
  
  
  def scrubValueFromEnvVar(self, variable, value, allowEnvSubs=True):
    """Return a configuration fragment string that removes the given value from an environment variable.  The value should be quoted using the kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them.

If allowEnvSubs is true, any references to environment variables in value (as ${VARIABLE_NAME}) are written such that the shell will perform substitution on them.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')
  
  
  def scrubPathFromEnvVar(self, variable, value, allowEnvSubs=True):
    """Return a configuration fragment string that removes the given path from an environment variable.  The value should be quoted using the kind of quotes the shell in question uses for static strings that will not have any substitutions performed on them.

If allowEnvSubs is true, any references to environment variables in value (as ${VARIABLE_NAME}) are written such that the shell will perform substitution on them.
    """
    raise NotImplementedError('The shellHelper class is an abstract base class.')


  def createAlias(self, name, command):
    """For shells that support aliases, return a configuration fragment string that adds a new command alias to the environment."""
    raise NotImplementedError('The shellHelper class is an abstract base class.')
  
  
  def removeAlias(self, name):
    """For shells that support aliases, return a configuration fragment string that removes a named command alias from the environment."""
    raise NotImplementedError('The shellHelper class is an abstract base class.')
  
  
  def wrapCommand(self, command):
    """Simple wrap the given command as necessary for packaging in a VALET config emission."""
    return command


class shShellHelper(shellHelper):
  """Subclass of shellHelper that implements methods that apply to the Bourne shell (and all its derivatives)."""
  
  def matchShellTypes(self):
    return [shellType.shellTypeEnum.sh, shellType.shellTypeEnum.any]

  def scriptingHeader(self):
    return 'scrubPath () { PATH=":$PATH:"; PATH="${PATH//:$1:/:}"; PATH="${PATH#:}"; PATH="${PATH%:}"; };'

  def scriptingFooter(self):
    return 'unset -f scrubPath;'

  #
  # Escape strings for sh derivatives; it is implied that the generated command containing
  # str will have the string enclosed in single quotes.
  #
  def escapeString(self, inStr, allowEnvSubs=True):
    if not inStr:
      return ''
    safeStr = inStr.replace("'", "'\"'\"'")
    if not allowEnvSubs:
      return safeStr
    # Move variable references outside the quotes:
    return re.sub('\${([A-Za-z_][A-Za-z0-9_]*)}', "'\"${\\1}\"'", safeStr)

  def unsetEnvVar(self, variable):
    return 'unset ' + variable + ';'

  def setValueOfEnvVar(self, variable, value, allowEnvSubs=True):
    return variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\';' + ' export ' + variable + ';'
  
  def appendValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    return variable + '="${' + variable + '}${' + variable + ':+' + delimiter + '}"\''+ self.escapeString(value, allowEnvSubs) + '\'; export ' + variable + ';'
  
  def prependValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    return variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\'"${' + variable + ':+' + delimiter + '}${' + variable + '}"; export ' + variable + ';'

  #def appendValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
  #  return 'if [ -z "$' + variable + '" ]; then ' + variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\'; else ' + variable + '="${' + variable + '}"\'' + delimiter + '\'\'' + self.escapeString(value, allowEnvSubs) + '\'; fi; export ' + variable + ';'

  #def prependValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
  #  return 'if [ -z "$' + variable + '" ]; then ' + variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\'; else ' + variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\'\'' + delimiter + '\'"${' + variable + '}"; fi; export ' + variable + ';'
  
  def scrubValueFromEnvVar(self, variable, value, allowEnvSubs=True):
    if not value:
      return ''
    return variable + '=${' + variable + '//' + value + '/:}; export ' + variable + ';'
  
  def scrubPathFromEnvVar(self, variable, value, allowEnvSubs=True):
    if not value:
      return ''
    return variable + '=scrubPath \'' + shHelper.escapeString(value, False) + '\'; export ' + variable + ';'


  def createAlias(self, name, command):
    if not command:
      return ''
    return 'alias ' + name + '=\'' + self.escapeString(command, True) + '\';'
          
  
  def removeAlias(self, name):
    return 'unalias ' + name + ' >& /dev/null;'
    
  
  def wrapCommand(self, command):
    return command + ';'


class bashShellHelper(shShellHelper):
  """Subclass of shShellHelper that overrides methods to use Bash's more concise "export VARIABLE=VALUE" form versus the "VARIABLE=VALUE; export VARIABLE" form."""

  def matchShellTypes(self):
    return [shellType.shellTypeEnum.bash, shellType.shellTypeEnum.sh, shellType.shellTypeEnum.any]

  def setValueOfEnvVar(self, variable, value, allowEnvSubs=True):
    return 'export ' + variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\';'
  
  def appendValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    return 'export ' + variable + '="${' + variable + '}${' + variable + ':+' + delimiter + '}"\''+ self.escapeString(value, allowEnvSubs) + '\';'
  
  def prependValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    return 'export ' + variable + '=\'' + self.escapeString(value, allowEnvSubs) + '\'"${' + variable + ':+' + delimiter + '}${' + variable + '}";'
  
  def scrubValueFromEnvVar(self, variable, value, allowEnvSubs=True):
    if not value:
      return ''
    return 'export ' + variable + '=${' + variable + '//' + value + '/:};'
  
  def scrubPathFromEnvVar(self, variable, value, allowEnvSubs=True):
    if not value:
      return ''
    return 'export ' + variable + '=scrubPath \'' + self.escapeString(value, False) + '\';'



class cshShellHelper(shellHelper):
  """Subclass of shellHelper that implements methods that apply to the C shell (and its derivatives)."""
  
  def matchShellTypes(self):
    return [shellType.shellTypeEnum.csh, shellType.shellTypeEnum.any]
    
  #
  # No function support, can't use foreach inside an alias...CSH really does force one to
  # do some creative problem solving for relatively simple use cases!
  #
  def scriptingHeader(self):
    return 'alias scrubPath \'eval "set path = `' + config.binDir + '/csh/csh_munge \!:1`"\'' + os.linesep

  def scriptingFooter(self):
    return 'unalias scrubPath' + os.linesep

  #
  # Escape strings for csh derivatives; it is implied that the generated command containing
  # str will have the string enclosed in single quotes.
  #
  def escapeString(self, str, allowEnvSubs=True):
    if not str:
      return ''
    safeStr = str.replace("'", "'\"'\"'").replace('!', '\\!')
    if not allowEnvSubs:
      return safeStr
    # Move variable references outside the quotes:
    return re.sub('\${([A-Za-z_][A-Za-z0-9_]*)}', "'\"${\\1}\"'", safeStr)

  def unsetEnvVar(self, variable):
    return 'unsetenv ' + variable + os.linesep

  def setValueOfEnvVar(self, variable, value, allowEnvSubs=True):
    return 'setenv ' + variable + ' \'' + self.escapeString(value, allowEnvSubs) + '\'' + os.linesep
  
  def appendValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    if delimiter == ':':
      if variable == 'PATH':
        return 'set path = ($path \'' + self.escapeString(value, allowEnvSubs) + '\')' + os.linesep
    return 'if ( $?' + variable + ' ) then' + os.linesep + '  setenv ' + variable + ' "$' + variable + '"\'' + delimiter + self.escapeString(value, allowEnvSubs) + '\'' + os.linesep + 'else' + os.linesep + '  setenv ' + variable + ' ' + '\'' + self.escapeString(value, allowEnvSubs) + '\'' + os.linesep + 'endif' + os.linesep
  
  def prependValueToEnvVar(self, variable, value, delimiter=' ', allowEnvSubs=True):
    if delimiter == ':':
      if variable == 'PATH':
        return 'set path = (\'' + self.escapeString(value, allowEnvSubs) + '\' $path)' + os.linesep
    return 'if ( $?' + variable + ' ) then' + os.linesep + '  setenv ' + variable + ' \'' + self.escapeString(value, allowEnvSubs) + delimiter + '\'"$' + variable + '"' + os.linesep + 'else' + os.linesep + '  setenv ' + variable + ' ' + '\'' + self.escapeString(value, allowEnvSubs) + '\'' + os.linesep + 'endif' + os.linesep


  def createAlias(self, name, command):
    if not command:
      return ''
    return 'alias ' + name + ' \'' + self.escapeString(command, True) + '\'' + os.linesep
          
  
  def removeAlias(self, name):
    return 'unalias ' + name + os.linesep
    
  
  def wrapCommand(self, command):
    return command + os.linesep


if __name__ == '__main__':
  print
  
  csh = shellType.shellHelperForShellType('csh')
  
  s = csh.scriptingHeader()
  s += csh.setValueOfEnvVar('ARCH_BITS', '64')
  s += csh.setValueOfEnvVar('LD_PRELOAD', '/usr/lib${ARCH_BITS}/preload_geco.so', True)
  s += csh.appendValueToEnvVar('PATH', '/usr/local/geco/bin/${ARCH_BITS}', ':', True)
  s += csh.prependValueToEnvVar('LD_LIBRARY_PATH', '/usr/local/geco/lib${ARCH_BITS}', ':', True)
  s += csh.appendValueToEnvVar('PATH', '/usr/local/bin', ':')
  s += csh.unsetEnvVar('ARCH_BITS')
  s += csh.scriptingFooter()
  print s
  print
  
  sh = shellType.shellHelperForShellType('sh')
  
  s = sh.scriptingHeader()
  s += sh.setValueOfEnvVar('ARCH_BITS', '64')
  s += sh.setValueOfEnvVar('LD_PRELOAD', '/usr/lib${ARCH_BITS}/preload_geco.so', True)
  s += sh.appendValueToEnvVar('PATH', '/usr/local/geco/bin/${ARCH_BITS}', ':', True)
  s += sh.prependValueToEnvVar('LD_LIBRARY_PATH', '/usr/local/geco/lib${ARCH_BITS}', ':', True)
  s += sh.appendValueToEnvVar('PATH', '/usr/local/bin', ':')
  s += sh.unsetEnvVar('ARCH_BITS')
  s += sh.scriptingFooter()
  print s
  print
  
  bash = shellType.shellHelperForShellType('bash')
  
  s = bash.scriptingHeader()
  s += bash.setValueOfEnvVar('ARCH_BITS', '64')
  s += bash.setValueOfEnvVar('LD_PRELOAD', '/usr/lib${ARCH_BITS}/preload_geco.so', True)
  s += bash.appendValueToEnvVar('PATH', '/usr/local/geco/bin/${ARCH_BITS}', ':', True)
  s += bash.prependValueToEnvVar('LD_LIBRARY_PATH', '/usr/local/geco/lib${ARCH_BITS}', ':', True)
  s += bash.appendValueToEnvVar('PATH', '/usr/local/bin', ':')
  s += bash.unsetEnvVar('ARCH_BITS')
  s += bash.scriptingFooter()
  print s
  print
