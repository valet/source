# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import sys
from config import *
from basePackage import *


class package(basePackage):
  """Software package

A package encompasses baseline attributes (such as path prefix, reference URL, description) and actions, dependencies, and incompatibilities that are common to all versions of the package.

A package also includes zero or more versions of itself -- a collection of packageVersion and packageVersionAlias objects that augment the baseline functionality of their parent package.  A default version is specified either explicitly or is implicitly the first packageVersion or packageVersionAlias that is added to the package.

Each package has a label associated with it.  This field can be used by consumer code to indicate in what capacity the package is being used:  the field defaults to the string "package" but might be changed to "dependency" when the package is loaded to satisfy another package's dependencies.
  """

  def __init__(self, pkgId, prefix = None, description = None, referenceURL = None, flags = None, incompatibilities = None, dependencies = None, actions = None, versions = [], defaultVersionId = None, sourceFile = None, ctxts = []):
    """Constructor

The package constructor shares the arguments available to the basePackage constructor, with the addition of the versions, defaultVersionId, and sourceFile arguments.

If the pkgId is an instance of the id class, the packageOnlyId() method is used to return a new instance of id that uses ONLY the packageId portion.  If pkgId is an atom instance, an id instance is initialized directly from that.  Finally, if pkgId is a string, any slash (/) delimited suffix is dropped and an id instance is initialized with the remnant.

Raises a ValueError exception if pkgId is not an id, atom, or string, or doesn't parse properly.

The sourceFile argument is typically passed by whatever code parses a configuration file and allocates an instance from data therein.

This class implements the container and iteration magic methods (__len__, __iter__,  __contains__, __getitem__) and additional dict-like iteration methods (keys, iterkeys, iteritems).
    """
    if isinstance(pkgId, id):
      pkgId = pkgId.packageOnlyId()
    elif isinstance(pkgId, atom):
      pkgId = id(str(pkgId))
    elif isinstance(pkgId, basestring):
      if '/' in pkgId:
        pkgId = re.sub('/.*$', '', pkgId)
      pkgId = id(pkgId)
    else:
      raise ValueError('package.__init__ expects an id, atom, or str.')
  
    super(package, self).__init__(pkgId, prefix, description, referenceURL, flags, incompatibilities, dependencies, actions, ctxts)
    
    self.setLabel('package')
    self.setSourceFile(sourceFile)
    self._firstVersion = None
    self._versions = {}
    if versions is not None:
      for aVersion in versions:
        self.addVersion(aVersion)
    self.setDefaultVersionId(defaultVersionId)
    
    
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(package, self).__repr__())
    s = s[:-1]
    #
    # Add our specific stuff:
    #
    if self._sourceFile:
      s += ', sourceFile="' + self._sourceFile.replace('"', '\\"') + '"'
    if self._defaultVersionId:
      s += ', defaultVersionId="' + str(self._defaultVersionId).replace('"', '\\"') + '"'
    if len(self._versions) > 0:
      s += ', versions=' + self._versions.__repr__()
    return s
  
  
  def __len__(self):
    return len(self._versions)
    
    
  def __iter__(self):
    return self._versions.iterkeys()
    
    
  def keys(self):
    return self._versions.keys()
    
    
  def iterkeys(self):
    return self._versions.iterkeys()
    
    
  def iteritems(self):
    return self._versions.iteritems()
    
  
  def __contains__(self, item):
    theVers = None
    if isinstance(item, basestring):
      try:
        versId = id(item)
        if versId is not None:
          return versId in self._versions
      except:
        pass
    elif isinstance(item, id):
      return item in self._versions
    elif isinstance(item, packageVersion):
      return item in self._versions.items.values()
    return False


  def __getitem__(self, key):
    if isinstance(key, basestring):
      try:
        versId = id(key)
        if versId is not None and versId in self._versions:
          return self._versions[versId]
        raise KeyError('Invalid key: ' + key)
      except Exception as e:
        raise KeyError('Invalid key: ' + key)
    elif isinstance(key, id):
      if key in self._versions:
        return self._versions[key]
      raise KeyError('Invalid key: ' + str(key))
    raise TypeError('Invalid key type: ' + str(key))


  def isPackage(self):
    """Return True to indicate that this is a software package kind of a basePackage."""
    return True
  
  
  def isValid(self):
    """Return True if basePackage says we're valid AND a prefix path is defined."""
    if super(package, self).isValid():
      # We need a prefix path:
      if self.prefix() is not None:
        return True
    return False
  
  
  def normalizeVersionId(self, aVersionId, fixPackageComponent = False):
    """If aVersionId is a string, it is turned into an id instance.  If the string contains a slash (/), all characters leading up to and including the slash are removed.  The string is then appended to this instance's package id and an id instance created using it.  A ValueError exception is raised if the string is not a valid package id.

If aVersionId is an atom instance, an id instance is created using this instance's package id with the atom appended to it.

If aVersionId is an id instance, its package id component is replaced with this instance's package id if they do not match (so long as fixPackageComponent is True).  If the version component of aVersionId is empty, the default pacakge id for this instance is returned.

A ValueError exception is raised if aVersionId is not a string, atom, or id instance.
    """
    if isinstance(aVersionId, basestring):
      if '/' in aVersionId:
        aVersionId = re.sub('^[^/]*/', '', aVersionId)
      aVersionId = id(str(self.id().packageId()) + '/' + aVersionId)
    elif isinstance(aVersionId, atom):
      # An atom must have correct syntax already, so just go ahead and create an id using it
      # and this instance's package id component:
      aVersionId = id(str(self.id().packageId()) + '/' + str(aVersionId))
    elif isinstance(aVersionId, id):
      if aVersionId.packageId() != self.id().packageId():
        if not fixPackageComponent:
          return aVersionId
        aVersionId = self.id().merge(idMergeFields.package, aVersionId, idMergeFields.versionAndFeatures)
    else:
      raise ValueError('package.normalizeVersionId expects a string, atom, or id')
      
    if not aVersionId.versionId():
      aVersionId = self._defaultVersionId
    return aVersionId


  def setVersion(self, aVersion):
    """"Convenience method that calls addVersion with shouldReplace = True."""
    self.addVersion(aVersion, True)


  def addVersion(self, aVersion, shouldReplace = False):
    """"Add aVersion to the package.  If the id for aVersion matches a version already present in this instance's version dictionary and shouldReplace is False, a ValueError exception is raised.

The version and feature components of the id from aVersion are merged with this instance's package component and aVersion is updated with the merged form.  This ensures a consistent id present in both.

This method also sets the parent package attribute of aVersion to this instance. 
    """
    if not isinstance(aVersion, basePackage) or not aVersion.isPackageVersion():
      raise ValueError('package.addVersion expects a packageVersion argument.')
    versionId = self.normalizeVersionId(aVersion.id(), fixPackageComponent = True)
    
    if shouldReplace or not versionId in self._versions:
      #  
      # If we already have one, make sure we drop ourself as that
      # item's parent package:
      #
      if versionId in self._versions:
        self._versions[versionId].setParentPackage(None)
      #
      # Ensure the version has an id consistent with my own:
      #
      aVersion.setId(versionId)
      #
      # Set me as its parent:
      #
      aVersion.setParentPackage(self)
      #
      # Set in my version dictionary:
      #
      self._versions[versionId] = aVersion
      #
      # If we don't have an explicit default version, the first
      # one added is it:
      #
      if self._defaultVersionId is None:
        self._defaultVersionId = versionId
      if self._firstVersion is None:
        self._firstVersion = aVersion
    else:
      raise ValueError('package.addVersion: ' + str(versionId) + ' already exists')
      
      
  def filterVersions(self, byContext = contexts.all, byId = None):
    """Return a list of version instances in this instance's version dictionary which match the given criteria.

If byContext is a string, then the version dictionary is filtered by selecting only those version instances whose contexts include that string.  If byContext is None or the "all" context, all versions are chosen.

Next, if byId is not None, the previously selected list of versions is refined by filtering their id() return values against byId, where byId can be an id or atom instance, or a string containing simple or regex components as well as version predicates.

The list of matching versions (in arbitrary order) is returned.  If this instance has no versions or there are no matches an empty list is returned.
    """
    if len(self._versions) > 0:
      if byContext is None or byContext == contexts.all:
        firstPass = self._versions.values()
      else:
        firstPass = filter(lambda f:byContext in f.contexts, self._versions.values())
      if len(firstPass) > 0:
        if byId is not None:
          #
          # First, try to turn byId into an actual id if it's not:
          #
          if not isinstance(byId, id):
            try:
              # This will succeed if byId is a string with valid format OR an atom:
              byId = self.normalizeVersionId(id(byId), fixPackageComponent = False)
            except:
              if isinstance(byId, basestring):
                # The id class can match against strings that contain regular expressions and version predicates:
                pass
              else:
                raise ValueError('package.filterVersion:  invalid object for byId: ' + type(byId))
          else:
            # Sanity check:
            byId = self.normalizeVersionId(byId, fixPackageComponent = False)
          #
          # Filter the firstPass list by the id and return the resulting list:
          #
          return filter(lambda f:f.id() == byId, firstPass)
      return firstPass
    return []


  def getVersionWithId(self, aVersionId, exactMatch = False, context = contexts.standard):
    """Retrieve a version of this package by its versioned id.

The aVersionId argument is normalized according to the normalizeVersionId method, with fixPackageComponent = False.

If exactMatch is True, then only a version with the required features -- and no additional features -- matches aVersionId.  By default, exactMatch is False and the value of config.featureMatchingMode affects which version is ultimately selected.
    """
    aVersionId = self.normalizeVersionId(aVersionId, fixPackageComponent = False)
    if aVersionId in self._versions:
      if context in self._versions[aVersionId].contexts:
        return self._versions[aVersionId]
      return None
  
    #
    # Since the version is keyed by its id string, the above is the only
    # way to get an exact match:
    #
    if exactMatch:
      return None
      
    #
    # Setup for finding a version with the most-compatible feature
    # match:
    #
    outVersion = None
    matchCount = float('inf') if config.featureMatchingMode == featureMatchingModes.least_additional else float('-inf')
    
    for vId, version in self._versions.iteritems():
      if vId.featureCompatible(aVersionId) and context in version.contexts:
        #
        # The VALET config says to return the first match:
        #
        if config.featureMatchingMode == featureMatchingModes.first_match:
          outVersion = version
          break;
        #
        # Fewest or most extra features?
        #
        elif config.featureMatchingMode == featureMatchingModes.least_additional:
          if len(vId.features()) < matchCount:
            outVersion = version
            matchCount = len(vId.features())
        elif config.featureMatchingMode == featureMatchingModes.most_additional:
          if len(vId.features()) > matchCount:
            outVersion = version
            matchCount = len(vId.features())
    return outVersion


  def getVersionAndRankWithId(self, aVersionId, context = contexts.standard):
    """Return a tuple containing (in order):

  - the fraction of desired features matched
  - the package version instance which matched

If there are multiple package instances with the same id, then this method can be used to generate a set of matches across all like-id instances, and then the value of config.featureMatchingMode can be used to select the best match. 
    """
    aVersionId = self.normalizeVersionId(aVersionId)
    candidateVersion = self.getVersionWithId(aVersionId, False, context)
    if candidateVersion:
      wantFeatureCount = len(aVersionId.features())
      gotFeatureCount = len(candidateVersion.id().features())
      return (0.0 if wantFeatureCount == 0 else (float(gotFeatureCount) / float(wantFeatureCount)), 
               candidateVersion)
    return None
    

  def defaultVersionId(self):
    """Return this instance's default version id."""
    return self._defaultVersionId
    
    
  def setDefaultVersionId(self, pkgVersId):
    """Set this instance's default version id."""
    if not pkgVersId:
      pkgVersId = None
    else:
      pkgVersId = self.normalizeVersionId(pkgVersId, fixPackageComponent = True)
    self._defaultVersionId = pkgVersId
    
    
  def defaultVersion(self):
    """If this instance has a defaultVersionId set, then return that package version instance if it exists.  Otherwise, the firstVersion is returned."""
    if self._defaultVersionId in self._versions:
      return self._versions[self._defaultVersionId]
    return self._firstVersion
    
    
  def setDefaultVersion(self, pkgVers):
    """If pkgVers is None, the defaultVersionId is set to None, as well.  Otherwise, if pkgVers exists in this instance's package version dictionary, set the defaultVersionId to pkgVers's id.
    
Raises a ValueError exception if pkgVers is not a valid argument.
    """
    if pkgVers is None:
      self.setDefaultVersionId(None)
    elif isinstance(self, basePackage) and self.isPackageVersion():
      if pkgVers in self._versions:
        self.setDefaultVersionId(pkgVers.id())
    else:
      raise ValueError('package.setDefaultVersion expects a packageVersion or packageVersionAlias argument that exists as a version of the package.')
      

  def sourceFile(self):
    """Return this instance's source file attribute."""
    return self._sourceFile
    
    
  def setSourceFile(self, sourceFile):
    """Set this instance's source file attribute."""
    self._sourceFile = str(sourceFile) if sourceFile else None


  def infoString(self):
    """Return a string describing this instance for the sake of vpkg_info."""
    s  = '[' + str(self._id) + '] {' + os.linesep
    
    s += indentString(super(package, self).infoString(), '  ')
    
    if self._referenceURL is not None:
      s += '  ' + self._referenceURL + os.linesep
    
    if self._description is not None:
      s += '  ' + self._description + os.linesep

    if self._prefix is not None:
      s += '  prefix: ' + self._prefix + os.linesep
    
    if self._sourceFile is not None:
      s += '  source file: ' + self._sourceFile + os.linesep
    
    if self._defaultVersionId is not None:
      s += '  default version: ' + str(self._defaultVersionId) + os.linesep
    
    s += '  versions: {' + os.linesep
    # Walk the versions in alphabetical order:
    vKeys = self._versions.keys()
    vKeys.sort()
    for vId in vKeys:
      version = self._versions[vId]
      s += indentString(version.infoString(), '    ')
    s += '  }' + os.linesep + '}' + os.linesep
    return s



if __name__ == '__main__':

  p = package(pkgId = 'openmpi', description = 'Open MPI: Message Passing Interface', prefix = '/opt/shared/openmpi')
  print p
  print
  print p.__repr__()
  print
  print p.infoString()
  print
  
  print p.filterVersions(byId='openmpi/=>1.4.0')
