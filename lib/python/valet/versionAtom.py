# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import re
from atom import atom
from enum import *

#
# Attempt to load a rich version representation:
#
richVersionKind = None
try:
  from pkg_resources import parse_version
  richVersionKind = 'parse_version'
  cmpType = tuple
except:
  try:
    from distutils.version import LooseVersion
    richVersionKind = 'LooseVersion'
    cmpType = LooseVersion
  except:
    raise


def isIntegerString(s):
  """Returns True if the string argument represents an integer value."""
  if isinstance(s, int):
    return True
  if isinstance(s, basestring):
    if s[0] in ('-', '+'):
      return s[1:].isdigit()
    return s.isdigit()
  return False


def richVersionToString(v):
  """Unparses a rich version representation."""
  if richVersionKind == 'LooseVersion':
    return str(v)
  if richVersionKind == 'parse_version':
    out = ''
    delim = ''
    for part in v:
      if isIntegerString(part):
        out += delim + str(int(part))
      elif part[0] != '*':
        out += delim + part
      delim = '.'
    return out
  return v


#
# Enumeration of comparison operators.
#
versionCmpOp = enum({1:'equal', 2:'lessThan', 3:'lessThanEqual', 4:'greaterThan', 5:'greaterThanEqual'})


class versionAtom(atom):
  """Version identifier component

Version identifier components are subject to more intelligent comparison than the atom implementation.  The version string is decomposed (lazily) into a list of components:  strings versus a rich version representation type.  Comparison of version instances (as well as ordering) then happens by individual components in sequence:  the rich representation allows for intelligent comparison that orders e.g.

  1.6, 1.10, 1.1

    as

  1.1, 1.6, 1.10

The __cmp__ operator also understands a simple predicate language when a string prefixed by '=' is passed to it.  The string is composed of one or more binary version number tests:

  =>=1.4

    Any version number in the 1.4 release series or newer

  =>=1.4.8&<1.10

    Any version number 1.4.8 or later, but before the 1.10 release series

  =<1.4|>1.7.4

    Any version number prior to the 1.4 release series or after the 1.7.4 release.
  """
  
  
  def __init__(self, atomString):
    """Constructor

Same behavior as the atom.__init__ method.  Additional instance variables are initialized.
    """
    try:
      super(versionAtom, self).__init__(atomString)
      self._decomposedForm = None
      self._versionNumberTuple = None
    except:
      raise
    

  def decomposedForm(self):
    """ Attempts to break the version string into a sequence of components of type string or of the chosen rich version representation type.

When an instance is compared against other instances any components not a string are compared according to the version numbering rules associated with that rich version representation type.

The decomposed form is cached as part of the object after the first invocation of this method.
    """
    # If we don't have a rich version representation type we
    # don't do anything differently than atom -- indicate by
    # returning None:
    if richVersionKind is None:
      return None
      
    if self._decomposedForm is None:
      self._decomposedForm = list()
      value = self.stringValue()
      while value != '':
        pieces = re.match(r"^([^0-9]*)([0-9]+(\.[A-Za-z0-9_]+){0,2})", value)
        if pieces is None:
          self._decomposedForm.append(value)
          value = ''
        else:
          if pieces.group(1) != '':
            self._decomposedForm.append(pieces.group(1))
          if pieces.group(2) != '':
            nextPiece = pieces.group(2)
            try:
              if richVersionKind == 'parse_version':
                nextPiece = parse_version(nextPiece)
              elif richVersionKind == 'LooseVersion':
                nextPiece = LooseVersion(nextPiece)
            except:
              pass
            self._decomposedForm.append(nextPiece)
          value = value[len(pieces.group(0)):]
      if len(self._decomposedForm) == 0:
        self._decomposedForm = False
    if not self._decomposedForm:
      return None
    return self._decomposedForm


  def versionNumberTuple(self):
    """Locates the first occurrence of a rich version representation in the object's decomposed form and attempts to create a (major,minor,patch) tuple from it.

The version number tuple is cached as part of the object after the first invocation of this method.
    """
    if self._versionNumberTuple is None:
      self._versionNumberTuple = False
      myDecomp = self.decomposedForm()
      if myDecomp is not None:
        myPiece = myDecomp[0]
        index = 1
        while isinstance(myPiece, basestring) and index < len(myDecomp):
          myPiece = myDecomp[index]
          index = index + 1
        if isinstance(myPiece, cmpType):
          if richVersionKind == 'parse_version':
            if len(myPiece) > 0 and isIntegerString(myPiece[0]):
              myMajor = int(myPiece[0])
              if len(myPiece) > 1 and isIntegerString(myPiece[1]):
                myMinor = int(myPiece[1])
                if len(myPiece) > 2 and isIntegerString(myPiece[2]):
                  self._versionNumberTuple = (myMajor, myMinor, int(myPiece[2]))
                else:
                  self._versionNumberTuple = (myMajor, myMinor, 0)
              else:
                self._versionNumberTuple = (myMajor, 0, 0)
          elif richVersionKind == 'LooseVersion':
            if len(myPiece.version) > 0 and isIntegerString(myPiece.version[0]):
              myMajor = int(myPiece.version[0])
              if len(myPiece.version) > 1 and isIntegerString(myPiece.version[1]):
                myMinor = int(myPiece.version[1])
                if len(myPiece.version) > 2 and isIntegerString(myPiece.version[2]):
                  self._versionNumberTuple = (myMajor, myMinor, int(myPiece.version[2]))
                else:
                  self._versionNumberTuple = (myMajor, myMinor, 0)
              else:
                self._versionNumberTuple = (myMajor, 0, 0)
                
    if not self._versionNumberTuple:
      return None
    return self._versionNumberTuple
    
  
  def versionNumberString(self):
    """Using the result of the versionNumberTuple() function, returns the tuple in string form.  If the versionNumberTuple() function found nothing, this function returns this atom's entire string value.
    """
    vnt = self.versionNumberTuple()
    if vnt:
      return '.'.join(map(lambda i:str(i), vnt))
    return self.stringValue()
    
  
  def matchesMajorVersion(self, majorVersion, operator = versionCmpOp.equal):
    """If the object has a version number tuple available, compare the passed majorVersion against that representation's major version component under the passed operator."""
    myVersionNum = self.versionNumberTuple()
    if myVersionNum is not None:
      myMajor = myVersionNum[0]
      if operator == versionCmpOp.equal:
        return myMajor == majorVersion
      if operator == versionCmpOp.lessThan:
        return myMajor < majorVersion
      if operator == versionCmpOp.lessThanEqual:
        return myMajor <= majorVersion
      if operator == versionCmpOp.greaterThan:
        return myMajor > majorVersion
      if operator == versionCmpOp.greaterThanEqual:
        return myMajor >= majorVersion
    return False
  

  def matchesMajorAndMinorVersion(self, majorVersion, minorVersion, operator = versionCmpOp.equal):
    """If the object has a version number tuple available, compare the passed majorVersion.minorVersion against that representation's major and minor version components under the passed operator."""
    myVersionNum = self.versionNumberTuple()
    if myVersionNum is not None:
      myMajor = myVersionNum[0]
      myMinor = myVersionNum[1]
      if operator == versionCmpOp.equal:
        return myMajor == majorVersion and myMinor == minorVersion
      if operator == versionCmpOp.lessThan:
        return myMajor < majorVersion or (myMajor == majorVersion and myMinor < minorVersion)
      if operator == versionCmpOp.lessThanEqual:
        return myMajor < majorVersion or (myMajor == majorVersion and myMinor <= minorVersion)
      if operator == versionCmpOp.greaterThan:
        return myMajor > majorVersion or (myMajor == majorVersion and myMinor > minorVersion)
      if operator == versionCmpOp.greaterThanEqual:
        return myMajor > majorVersion or (myMajor == majorVersion and myMinor >= minorVersion)
    return False


  def matchesMajorMinorAndPatchVersion(self, majorVersion, minorVersion, patchVersion, operator = versionCmpOp.equal):
    """If the object has a version number tuple available, compare the passed majorVersion.minorVersion.patchVersion against that representation's major, minor, and patch version components under the passed operator."""
    myVersionNum = self.versionNumberTuple()
    if myVersionNum is not None:
      myMajor = myVersionNum[0]
      myMinor = myVersionNum[1]
      myPatch = myVersionNum[2]
      if operator == versionCmpOp.equal:
        return myMajor == majorVersion and myMinor == minorVersion and myPatch == patchVersion
      if operator == versionCmpOp.lessThan:
        return myMajor < majorVersion or (myMajor == majorVersion and myMinor < minorVersion) or (myMajor == majorVersion and myMinor == minorVersion and myPatch < patchVersion)
      if operator == versionCmpOp.lessThanEqual:
        return myMajor < majorVersion or (myMajor == majorVersion and myMinor < minorVersion) or (myMajor == majorVersion and myMinor == minorVersion and myPatch <= patchVersion)
      if operator == versionCmpOp.greaterThan:
        return myMajor > majorVersion or (myMajor == majorVersion and myMinor > minorVersion) or (myMajor == majorVersion and myMinor == minorVersion and myPatch > patchVersion)
      if operator == versionCmpOp.greaterThanEqual:
        return myMajor > majorVersion or (myMajor == majorVersion and myMinor > minorVersion) or (myMajor == majorVersion and myMinor == minorVersion and myPatch >= patchVersion)
    return False
  
  ## versionAtom comparisonOrder
  #
  # Given another versionAtom, determine their relative ordering.  If
  # both have decomposed forms, then order them by means of the "intelligent"
  # comparison afforded by the rich version representation type.  Otherwise,
  # just use the inherited VAtomId __cmp__ method on the string forms.
  #
  # Returns:
  #
  #   versionCmpOp.equal        this object is ordered the same
  #                                           as the other
  #   versionCmpOp.lessThan     this object occurs before the other
  #   versionCmpOp.greaterThan  this object occurs after the other
  #
  def comparisonOrder(self, other):
    """Given another versionAtom, determine its ordering relative to this instance.  If both have decomposed forms, then order them by means of the "intelligent" comparison afforded by the rich version representation type.  Otherwise, just use the inherited VAtomId __cmp__ method on the string forms.

Returns:

  versionCmpOp.equal          this object is ordered the same as the other
  versionCmpOp.lessThan       this object occurs before the other
  versionCmpOp.greaterThan    this object occurs after the other
    """
    # Get the decomposed forms:
    myDecomp = self.decomposedForm()
    itsDecomp = other.decomposedForm()
    
    # If either of us can't produce a decomp form, revert to the atom comparison:
    if myDecomp is None or itsDecomp is None:
      order = super(versionAtom, self).__cmp__(other)
      if order < 0:
        return versionCmpOp.lessThan
      if order > 0:
        return versionCmpOp.greaterThan
      return versionCmpOp.equal
    
    myLen = len(myDecomp)
    itsLen = len(itsDecomp)
    index = 0
    
    result = versionCmpOp.equal
    while index < myLen and index < itsLen:
      myPiece = myDecomp[index]
      itsPiece = itsDecomp[index]
      if not isinstance(myPiece, cmpType) or not isinstance(itsPiece, cmpType):
        # Force to string comparison if both pieces are not rich version objects:
        myPiece = str(myPiece)
        itsPiece = str(itsPiece)
      
      # Perform the comparison:
      if myPiece == itsPiece:
        pass
      elif myPiece < itsPiece:
        result = versionCmpOp.lessThan
        break
      elif myPiece > itsPiece:
        result = versionCmpOp.greaterThan
        break
      index = index + 1
    
    if result == versionCmpOp.equal and myLen != itsLen:
      # Any remaining bits imply greater-than versus less-than:
      if index == myLen:
        result = versionCmpOp.lessThan
      else:
        result = versionCmpOp.greaterThan
    return result
  

  def satisfiesComparisonOperator(self, other, operator):
    """Returns True if the ordering of this object and the other match with the given comparison operator."""
    # We've determined ordering of the two values, check against the operator:
    order = self.comparisonOrder(other)
    return (operator & order) != 0
  

  def satisfiesVersionPredicate(self, predicate):
    """A predicate is a sequence of strings of the form

  (=|==|<|<=|>=|>)(X(.Y(.Z)?)?)

joined by '&' (logical AND) or '|' (logical OR) characters.  This method evaluates that logical expression and returns True if this object's primary version (via the versionNumberTuple method) satisfies the expression.

For example:

  >=4.2&<6
  
Indicates any version 4.2 or newer that's less than major version 6.
    """
    satisfied = False
    operatorMap = { '=': versionCmpOp.equal,
                    '==': versionCmpOp.equal,
                    '<': versionCmpOp.lessThan,
                    '<=': versionCmpOp.lessThanEqual,
                    '>': versionCmpOp.greaterThan,
                    '>=': versionCmpOp.greaterThanEqual
                  }
    logicOp = '='
    originalPredicate = predicate
    while predicate:
      pieces = re.match(r"^((<|>|=)=?)(([0-9]+)(\.([0-9]+)(\.([0-9]+))?)?)", predicate)
      if pieces is None:
        break
      majorVersion = int(pieces.group(4))
      if pieces.group(6):
        if pieces.group(8):
          nextSatisfied = self.matchesMajorMinorAndPatchVersion(majorVersion, int(pieces.group(6)), int(pieces.group(8)), operatorMap[pieces.group(1)])
        else:
          nextSatisfied = self.matchesMajorAndMinorVersion(majorVersion, int(pieces.group(6)), operatorMap[pieces.group(1)])
      else:
        nextSatisfied = self.matchesMajorVersion(majorVersion, operatorMap[pieces.group(1)])
      
      if logicOp == '=':
        satisfied = nextSatisfied
      elif logicOp == '|':
        satisfied = satisfied or nextSatisfied
      elif logicOp == '&':
        satisfied = satisfied and nextSatisfied
      predicate = predicate[len(pieces.group(0)):]
      if len(predicate) > 0:
        # Next bit of predicate must be a logical operator:
        logicOp = predicate[0:1]
        if logicOp != '|' and logicOp != '&':
          raise ValueError('Invalid version predicate: ' + originalPredicate[0:len(originalPredicate) - len(predicate)] + '[' + predicate + ']')
        predicate = predicate[1:]
    if predicate:
      raise ValueError('Invalid version predicate: ' + originalPredicate[0:len(originalPredicate) - len(predicate)] + '[' + predicate + ']')
    
    return satisfied
      

  def __eq__(self, other):
    """Object equality

The RHS can be a simple string, a regular expression prefixed by a '^', a version predicate string prefixed by an '=', or another versionAtom instance.

Raises a ValueError exception if the RHS is invalid.
    """
    if isinstance(other, basestring):
      if len(other) > 0:
        if other[0:1] == '^':
          return super(versionAtom, self).__eq__(other)
        if other[0:1] == '=':
          return self.satisfiesVersionPredicate(other[1:])
        try:
          other = versionAtom(other)
        except:
          raise
      else:
        return len(self.stringValue()) == 0
    return self.satisfiesComparisonOperator(other, versionCmpOp.equal)


  def __ne__(self, other):
    """Object inequality

Negated result of the equality operator.
    """
    if isinstance(other, basestring):
      if len(other) > 0:
        if other[0:1] == '^':
          return super(versionAtom, self).__ne__(other)
        if other[0:1] == '=':
          return not self.satisfiesVersionPredicate(other[1:])
        try:
          other = versionAtom(other)
        except:
          raise
      else:
        return len(self.stringValue()) != 0
    return not self.satisfiesComparisonOperator(other, versionCmpOp.equal)


  def __cmp__(self, other):
    """Object comparator

Performs the "intelligent" comparison of this version identifier against the other and returns a negative value if this identifier occurs before the other; a positive value if this identifier occurs after the other; and zero if the two are equivalent.
    """
    order = self.comparisonOrder(other)
    if order == versionCmpOp.equal:
      return 0
    if order == versionCmpOp.lessThan:
      return -1
    return 1

#
# For testing purposes, if this file is executed rather than imported
# let's show some tests.
#
if __name__ == '__main__':
  l = [ versionAtom('5'),
        versionAtom('4.2.2-gcc-10.1'),
        versionAtom('4.2.2-gcc-4.9.3'),
        versionAtom('4.3'),
        versionAtom('4.2.2')
      ]
  
  for x in sorted(l):
    print x
  
  print
  
  v = versionAtom('4.2.2a3-gcc-4.9.3')
  
  print str(v) + ' =[v4]=> ' + str(v.matchesMajorVersion(4))
  print str(v) + ' =[v5]=> ' + str(v.matchesMajorVersion(5))
  print str(v) + ' =[v5.1]=> ' + str(v.matchesMajorAndMinorVersion(5,1))
  print str(v) + ' =[v4.2]=> ' + str(v.matchesMajorAndMinorVersion(4,2))
  print str(v) + ' <[v5]=> ' + str(v.matchesMajorVersion(5, versionCmpOp.lessThan))
  print str(v) + ' <[v4.2]=> ' + str(v.matchesMajorAndMinorVersion(4, 2, versionCmpOp.lessThan))
  print str(v) + ' <=[v4.2]=> ' + str(v.matchesMajorAndMinorVersion(4, 2, versionCmpOp.lessThanEqual))
  
  print
  
  v = versionAtom('amd-4.3.2-intel-2016')
  print str(v) + ' satisfies(>=4.3&<6) => ' + str(v == '=>=4.3&<6')
  
  print
