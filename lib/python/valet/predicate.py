# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
import stat
import re
import pickle
import base64
import zlib

from enum import *
from contexts import *
from action import *


def sanitizePath(aPath):
  """Transforms the string in aPath by substituting all occurences of ${name} or $name with the indicated environment variable and all ~ or ~user components with home directory paths.  Returns the resulting string."""
  nextVal = os.path.expandvars(aPath)
  nextVal = os.path.expanduser(nextVal)
  return os.path.realpath(nextVal)


class operators:
  """Prediate operators
  
Class that wraps the enumeration of all binary and unary operators that are understood by the VALET predicate entity.  The class instance variable opEnum contains the enumeration.  Another class instance variable, stringMap, associates various string forms with the enumerated integer values; the codeMap class instance variable maps the enumerated values to their canonical textual representations.

The operators fall in two categories:  those that operate on the value of environment variables (contained in the envVarOps class instance variable) and those that operate on filesystem paths (contained in the filesystemOps class instance variable).
  """
  
  opEnum = enum([
              'isSet',
              'isNotSet',
              'equalTo',
              'notEqualTo',
              'lessThan',
              'lessThanEqualTo',
              'greaterThan',
              'greaterThanEqualTo',
              'startsWith',
              'notStartsWith',
              'endsWith',
              'notEndsWith',
              'contains',
              'notContains',
              'matchesRegex',
              'notMatchesRegex',
              'exists',
              'notExists',
              'isFileType',
              'notIsFileType',
              'isStrictFileType',
              'notIsStrictFileType',
              'isReadable',
              'notIsReadable',
              'isWritable',
              'notIsWritable',
              'isExecutable',
              'notIsExecutable',
              'isDescendentPath',
              'notIsDescendentPath'
            ])

  stringMap = {
              'is-set':                     opEnum.isSet,
              'is-not-set':                 opEnum.isNotSet,
              'not-is-set':                 opEnum.isNotSet,
              '==':                         opEnum.equalTo,
              'eq':                         opEnum.equalTo,
              '!=':                         opEnum.notEqualTo,
              'ne':                         opEnum.notEqualTo,
              '<':                          opEnum.lessThan,
              'lt':                         opEnum.lessThan,
              '<=':                         opEnum.lessThanEqualTo,
              'le':                         opEnum.lessThanEqualTo,
              '>':                          opEnum.greaterThan,
              'gt':                         opEnum.greaterThan,
              '>=':                         opEnum.greaterThanEqualTo,
              'ge':                         opEnum.greaterThanEqualTo,
              '<<':                         opEnum.startsWith,
              'starts-with':                opEnum.startsWith,
              '!<<':                        opEnum.notStartsWith,
              'not-starts-with':            opEnum.notStartsWith,
              '>>':                         opEnum.endsWith,
              'ends-with':                  opEnum.endsWith,
              '!>>':                        opEnum.notEndsWith,
              'not-ends-with':              opEnum.notEndsWith,
              '<>':                         opEnum.contains,
              'contains':                   opEnum.contains,
              '!<>':                        opEnum.notContains,
              'not-contains':               opEnum.notContains,
              '~':                          opEnum.matchesRegex,
              'matches':                    opEnum.matchesRegex,
              '!~':                         opEnum.notMatchesRegex,
              'not-matches':                opEnum.notMatchesRegex,
              '-e':                         opEnum.exists,
              'exists':                     opEnum.exists,
              '-E':                         opEnum.notExists,
              'not-exists':                 opEnum.notExists,
              '-t':                         opEnum.isFileType,
              'is-file-type':               opEnum.isFileType,
              '!-t':                        opEnum.notIsFileType,
              'not-is-file-type':           opEnum.notIsFileType,
              '-st':                        opEnum.isStrictFileType,
              'is-strict-file-type':        opEnum.isStrictFileType,
              '!-st':                       opEnum.notIsStrictFileType,
              'not-is-strict-file-type':    opEnum.notIsStrictFileType,
              '-r':                         opEnum.isReadable,
              'is-readable':                opEnum.isReadable,
              '!-r':                        opEnum.notIsReadable,
              'not-is-readable':            opEnum.notIsReadable,
              '-w':                         opEnum.isWritable,
              'is-writable':                opEnum.isWritable,
              '!-w':                        opEnum.notIsWritable,
              'not-is-writable':            opEnum.notIsWritable,
              '-x':                         opEnum.isExecutable,
              'is-executable':              opEnum.isExecutable,
              '!-x':                        opEnum.notIsExecutable,
              'not-is-executable':          opEnum.notIsExecutable,
              'is-descendent-path':         opEnum.isDescendentPath,
              'path-under':                 opEnum.isDescendentPath,
              '/>':                         opEnum.isDescendentPath,
              'not-is-descendent-path':     opEnum.notIsDescendentPath,
              'not-path-under':             opEnum.notIsDescendentPath,
              '!/>':                        opEnum.notIsDescendentPath
            }
  
  codeMap = {
                opEnum.isSet:               'is-set',
                opEnum.isNotSet:            'is-not-set',
                opEnum.equalTo:             'eq',
                opEnum.notEqualTo:          'ne',
                opEnum.lessThan:            'lt',
                opEnum.lessThanEqualTo:     'le',
                opEnum.greaterThan:         'gt',
                opEnum.greaterThanEqualTo:  'ge',
                opEnum.startsWith:          'starts-with',
                opEnum.notStartsWith:       'not-starts-with',
                opEnum.endsWith:            'ends-with',
                opEnum.notEndsWith:         'not-ends-with',
                opEnum.contains:            'contains',
                opEnum.notContains:         'not-contains',
                opEnum.matchesRegex:        'matches',
                opEnum.notMatchesRegex:     'not-matches',
                opEnum.exists:              'exists',
                opEnum.notExists:           'not-exists',
                opEnum.isFileType:          'is-file-type',
                opEnum.notIsFileType:       'not-is-file-type',
                opEnum.isStrictFileType:    'is-strict-file-type',
                opEnum.notIsStrictFileType: 'not-is-strict-file-type',
                opEnum.isReadable:          'is-readable',
                opEnum.notIsReadable:       'not-is-readable',
                opEnum.isWritable:          'is-writable',
                opEnum.notIsWritable:       'not-is-writable',
                opEnum.isExecutable:        'is-executable',
                opEnum.notIsExecutable:     'not-is-executable',
                opEnum.isDescendentPath:    'path-under',
                opEnum.notIsDescendentPath: 'not-path-under'
              }
  
  envVarOps = ( opEnum.isSet, opEnum.isNotSet, opEnum.equalTo, opEnum.notEqualTo, opEnum.lessThan, opEnum.lessThanEqualTo, opEnum.greaterThan, opEnum.greaterThanEqualTo, opEnum.startsWith, opEnum.notStartsWith, opEnum.endsWith, opEnum.notEndsWith, opEnum.contains, opEnum.notContains, opEnum.matchesRegex, opEnum.notMatchesRegex )
  
  filesystemOps = ( opEnum.exists, opEnum.notExists, opEnum.isFileType, opEnum.notIsFileType, opEnum.isStrictFileType, opEnum.notIsStrictFileType, opEnum.isReadable, opEnum.notIsReadable, opEnum.isWritable, opEnum.notIsWritable, opEnum.isExecutable, opEnum.notIsExecutable, opEnum.isDescendentPath, opEnum.notIsDescendentPath )
  
  unaryOps = ( opEnum.isSet, opEnum.isNotSet, opEnum.exists, opEnum.notExists, opEnum.isReadable, opEnum.notIsReadable, opEnum.isWritable, opEnum.notIsWritable, opEnum.isExecutable, opEnum.notIsExecutable )


#
#  Enumerates the stage at which a predicate should be evaluated.
#
stages = enum(['pre-condition', 'post-condition'])


class predicate(action):
  """Predicate
  
A predicate represents some test that VALET should have the shell perform and an action to take if that test is successful.  Predicate operators exist to test the existence or type of entity at a filesystem path.  Operators also exist to test the value of arbitrary environment variables.

Predicates can be serialized into an encoded binary format that can be passed between programs.  VALET writes the serialized form of a predicte into the shell script it produces, and invokes a predicate evaluation script with that serialized form as an argument in order to determine when the prediate is satisfied.  The predicate evaluation script uses the unserializeFromString class method to instantiate the predicate from the serialized form.
  """

  @staticmethod
  def unserializeFromString(predStr):
    """Reconstitute a predicate instance from a form produced by the serializeAsString method of this class."""
    pred = base64.b64decode(predStr)
    pred = zlib.decompress(pred)
    return pickle.loads(pred)

  def __init__(self, variableName = None, operator = None, testValue = None, stage = None, message = None, ctxts = []):
    """Constructor

None of the arguments are mandatory, but the instance will not be valid until at least the variableName, operator, and stage are set.  Binary operators also require the testValue to be set to be valid.

An optional ctxts argument is permissible, too.
    """
    super(predicate, self).__init__(ctxts)
    self._variableName = variableName
    self._testValue = testValue
    self._message = message
    self.setOperator(operator)
    self.setStage(stage)
    
  
  def clone(self):
    return predicate(self._variableName, self._operator, self._testValue, self._stage, self._message, self.contexts)


  def __eq__(self, other):
    """Object equality
    
Instance are equivalent if their variableName, operator, and contexts match.  For binary operators, their testValue fields must be equivalent, as well.
    """
    if isinstance(other, predicate):
      if self._variableName != other.variableName() or self._operator != other.operator() or self._testValue != other.testValue() or self.contexts != other.contexts:
        return False
    return True


  def __ne__(self, other):
    """Object inequality

Negated result of the equality operator.
    """
    if isinstance(other, predicate):
      if self._variableName == other.variableName() and self._operator == other.operator() and self._testValue == other.testValue() and self.contexts == other.contexts:
        return False
    return True


  def __str__(self):
    if not self.isValid():
      return 'Incomplete predicate'
    
    s = stages[self.stage()] + '(' + self._variableName + ' ' + operators.codeMap[self.operator()]
    if not self._operator in operators.unaryOps:
      s += ' ' + str(self.testValue())
    s += ')'
    contextStr = super(predicate, self).__str__()
    if contextStr:
      s += ' ' + contextStr
    return s

  
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(predicate, self).__repr__())
    if self._variableName is not None:
      s += ', variableName="' + self._variableName.replace('"', '\\"') + '"'
    if self._operator is not None:
      s += ', operator=' + str(self._operator)
    if self._testValue is not None:
      s += ', testValue="' + self._testValue.replace('"', '\\"') + '"'
    if self._stage is not None:
      s += ', stage=' + str(self._stage)
    if self._message is not None:
      s += ', message="' + self._message.replace('"', '\\"') + '"'
    return s + ')'
    
    
  def serializeAsString(self):
    """Returns a string that represents a serialized form of this instance of the predicate class."""
    s = pickle.dumps(self)
    s = zlib.compress(s)
    return base64.b64encode(s)


  def isValid(self):
    """Returns True is this instance of the predicate class is able to be evaluated."""
    if self._variableName is None:
      return False
    if self._operator is None:
      return False
    # Binary ops require a testValue:
    if not self._operator in operators.unaryOps and self._testValue is None:
      return False
    if self._stage is None:
      return False
    return True


  def variableName(self):
    """Returns this instance's variableName."""
    return self._variableName
    
    
  def setVariableName(self, variableName):
    """Sets this instance's variableName field."""
    self._variableName = str(variableName)


  def operator(self):
    """Returns this instance's operator."""
    return self._operator
    
    
  def setOperator(self, operator):
    """Sets this instance's operator field.  For unary operators, the testValue field is set to None, too."""
    if operator is None:
      self._operator = None
    elif isinstance(operator, basestring):
      if not operator in operators.stringMap:
        raise ValueError('Invalid predicate operator string: ' + operator)
      operator = operators.stringMap[operator]
    elif not operator in operators.opEnum:
      raise ValueError('Invalid predicate operator code: ' + str(operator))
    self._operator = operator
    if operator in operators.unaryOps:
      self._testValue = None


  def testValue(self):
    """Returns this instance's right-hand-side for binary operators."""
    return self._testValue
    
    
  def setTestValue(self, testValue):
    """Sets this instance's right-hand-side for binary operators."""
    if self._operator is not None and self._operator in operators.unaryOps:
      self._testValue = None
    else:
      self._testValue = str(testValue)


  def stage(self):
    """Returns this instance's testing stage."""
    return self._stage
    
    
  def setStage(self, stage):
    """Sets this instance's testing stage.
    
Raises a ValueError exception if the stage argument does not map to one of the strings or integers in the stages enum.    
"""
    if stage is None:
      self._stage = None
    elif isinstance(stage, basestring):
      if not stage in stages:
        raise ValueError('Invalid predicate stage string: ' + stage)
      stage = stages[stage]
    elif not stage in stages:
      raise ValueError('Invalid predicate stage code: ' + str(stage))
    self._stage = stage


  def message(self):
    """Returns this instance's message string."""
    return self._message
    
    
  def setMessage(self, message):
    """Sets this instance's message string."""
    self._message = str(message)


  def evaluate(self):
    """Attempt to evaluate this predicate.

Returns True or False if the prediate is valid and evaluates as such.  A RuntimeError exception is returns if the predicate is not valid and thus cannot be evaluated.
    """
    if not self.isValid():
      raise RuntimeError('This predicate is not valid.')
    
    #
    # Some of the tests expect the LHS to be an env var name; others
    # are expecting a filesystem path:
    #
    if self._operator in operators.filesystemOps:
      valFromEnv = self._variableName
    else:
      valFromEnv = os.getenv(self._variableName, None)

    if self._operator == operators.opEnum.isSet:
      return valFromEnv is not None

    elif self._operator == operators.opEnum.isNotSet:
      return valFromEnv is None

    elif self._operator == operators.opEnum.equalTo:
      return valFromEnv is not None and valFromEnv == self._testValue

    elif self._operator == operators.opEnum.notEqualTo:
      return valFromEnv is None or valFromEnv <> self._testValue

    elif self._operator == operators.opEnum.lessThan:
      return valFromEnv is not None and valFromEnv < self._testValue

    elif self._operator == operators.opEnum.lessThanEqualTo:
      return valFromEnv is not None and valFromEnv <= self._testValue

    elif self._operator == operators.opEnum.greaterThan:
      return valFromEnv is not None and valFromEnv > self._testValue

    elif self._operator == operators.opEnum.greaterThanEqualTo:
      return valFromEnv is not None and valFromEnv >= self._testValue

    elif self._operator == operators.opEnum.startsWith:
      return valFromEnv is not None and valFromEnv.startswith(self._testValue)

    elif self._operator == operators.opEnum.notStartsWith:
      return valFromEnv is not None and not valFromEnv.startswith(self._testValue)

    elif self._operator == operators.opEnum.endsWith:
      return valFromEnv is not None and valFromEnv.endswith(self._testValue)

    elif self._operator == operators.opEnum.notEndsWith:
      return valFromEnv is not None and not valFromEnv.endswith(self._testValue)

    elif self._operator == operators.opEnum.contains:
      return valFromEnv is not None and self._testValue in valFromEnv

    elif self._operator == operators.opEnum.contains:
      return valFromEnv is not None and not self._testValue in valFromEnv

    elif self._operator == operators.opEnum.matchesRegex:
      regex = re.compile(self._testValue)
      if regex is not None:
        return valFromEnv is not None and regex.search(valFromEnv) is not None
      return False

    elif self._operator == operators.opEnum.notMatchesRegex:
      regex = re.compile(self._testValue)
      if regex is not None:
        return valFromEnv is not None and regex.search(valFromEnv) is None
      return False

    elif self._operator == operators.opEnum.exists:
      return os.path.exists(sanitizePath(valFromEnv))

    elif self._operator == operators.opEnum.notExists:
      return not os.path.exists(sanitizePath(valFromEnv))

    elif self._operator in (operators.opEnum.isFileType, operators.opEnum.notIsFileType):
      if self._operator == operators.opEnum.notIsFileType:
        rc = False
      else:
        rc = True
      try:
        mode = os.stat(sanitizePath(valFromEnv)).st_mode
        typeMap = {
                      stat.S_IFDIR:   'directory',
                      stat.S_IFREG:   'file',
                      stat.S_IFLNK:   'symlink',
                      stat.S_IFSOCK:  'socket',
                      stat.S_IFIFO:   'fifo'
                  }
        for typeMask, typeStr in typeMap.iteritems():
          if mode & typeMask == typeMask:
            if typeStr == self._testValue.lower():
              return rc
            return not rc
        return not rc
      except:
        return not rc

    elif self._operator in (operators.opEnum.isStrictFileType, operators.opEnum.notIsStrictFileType):
      if self._operator == operators.opEnum.notIsStrictFileType:
        rc = False
      else:
        rc = True
      try:
        mode = os.lstat(sanitizePath(valFromEnv)).st_mode
        typeMap = {
                      stat.S_IFDIR:   'directory',
                      stat.S_IFREG:   'file',
                      stat.S_IFLNK:   'symlink',
                      stat.S_IFSOCK:  'socket',
                      stat.S_IFIFO:   'fifo'
                  }
        for typeMask, typeStr in typeMap.iteritems():
          if mode & typeMask == typeMask:
            if typeStr == self._testValue.lower():
              return rc
            return not rc
        return not rc
      except:
        return not rc

    elif self._operator == operators.opEnum.isReadable or self._operator == operators.opEnum.notIsReadable:
      if self._operator == operators.opEnum.notIsReadable:
        rc = False
      else:
        rc = True
      if os.access(sanitizePath(valFromEnv), os.R_OK):
        return rc
      return not rc

    elif self._operator == operators.opEnum.isWritable or self._operator == operators.opEnum.notIsWritable:
      if self._operator == operators.opEnum.notIsWritable:
        rc = False
      else:
        rc = True
      if os.access(sanitizePath(valFromEnv), os.W_OK):
        return rc
      return not rc

    elif self._operator == operators.opEnum.isExecutable or self._operator == operators.opEnum.notIsExecutable:
      if self._operator == operators.opEnum.notIsExecutable:
        rc = False
      else:
        rc = True
      if os.access(sanitizePath(valFromEnv), os.X_OK):
        return rc
      return not rc
    
    elif self._operator == operators.opEnum.isDescendentPath or self._operator == operators.opEnum.notIsDescendentPath:
      if not valFromEnv:
        raise ValueError('Descendent-path test requires a non-empty path.')
        
      targetPath = sanitizePath(valFromEnv)
      if not os.path.exists(targetPath):
        raise ValueError('Non-existent target path provided to descendent-path test: ' + targetPath)
      targetPath = targetPath + (os.sep if not targetPath.endswith(os.sep) else '')
      
      rootPath = sanitizePath(self._testValue)
      if not os.path.exists(rootPath):
        raise ValueError('Non-existent test path provided to descendent-path test: ' + rootPath)
      rootPath = rootPath + (os.sep if not rootPath.endswith(os.sep) else '')
      
      if self._operator == operators.opEnum.notIsDescendentPath:
        rc = False
      else:
        rc = True
      if targetPath.startswith(rootPath):
        return rc
      return not rc
  
    return False


  def actionStringWithShellHelper(self, shellHelper, isDependency = True, currentContext = contexts.all):
    """Return a configuration fragment string that affects this instance's predicate test within the shell environment.  The test is shell-specific and isn't implemented by the shell helper classes, so it must be done herein."""
    outCmds = ''
    
    # Promote an atomic currentContext to being a set:
    if isinstance(currentContext, basestring):
      currentContext = set(currentContext)
    
    if currentContext in self.contexts:
      #
      # There's no way around it -- this has to be done per shell:
      #
      if shellType.shellTypeEnum.sh in shellHelper.matchShellTypes():
        if isDependency:
          outCmds += 'VALET_ERROR_MSG=`' + os.path.join(config.binDir, 'valet_predicateTest') + ' -d \'' + self.serializeAsString() + '\'`;'
        else:
          outCmds += 'VALET_ERROR_MSG=`' + os.path.join(config.binDir, 'valet_predicateTest') + ' -i \'' + self.serializeAsString() + '\'`;'
        outCmds += 'if [ $? -ne 0 ]; then VALET_EARLY_EXIT=1; break; fi; unset VALET_ERROR_MSG;'
      else:
        raise NotImplementedError('Given shell not implemented in predicate.py!!!')
    return outCmds


if __name__ == '__main__':

  p1 = predicate(variableName = 'PATH', operator = '<>', testValue = '/usr/bin', stage = 'pre-condition')
  print str(p1) + ' = ' + str(p1.evaluate())
  print
  
  p2 = predicate(variableName = '/scratch', operator = '-t', testValue = 'directory', stage = 'pre-condition', ctxts = ['pre', 'all', 'dev'])
  print str(p2) + ' = ' + str(p2.evaluate())
  print
  print p2.serializeAsString()
  print
  
  p3 = predicate(variableName = '/tmp', operator = '-t', testValue = 'directory', stage = 'post-condition')
  print str(p3) + ' = ' + str(p3.evaluate())
  print
  
  print (p1, p2, p3)
  print

  
