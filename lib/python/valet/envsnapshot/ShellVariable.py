#
# valet_envsnapshot
# ShellVariable.py
#
# Class to represent a shell variable.
#
# $Id: ShellVariable.py 531 2014-10-31 16:18:05Z frey $
#

from ShellObject import *

class ShellVariable(ShellObject):
  
  def typeDescription(self):
    return "variable"
  
