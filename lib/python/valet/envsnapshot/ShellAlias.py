#
# valet_envsnapshot
# ShellAlias.py
#
# Class to represent a shell alias.
#
# $Id: ShellAlias.py 531 2014-10-31 16:18:05Z frey $
#

from ShellObject import *

class ShellAlias(ShellObject):
  
  def typeDescription(self):
    return "alias"
