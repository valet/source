#
# valet_envsnapshot
# ShellObject.py
#
# Base class for representing named entities in a shell -- variables,
# functions, aliases.
#
# $Id: ShellObject.py 533 2014-10-31 16:54:20Z frey $
#

class ShellObject(object):

  def __init__(self, name, value, flags=None):
    self._name = name;
    self._value = value;
    self._flags = flags;

  def __str__(self):
    outStr = self.typeDescription()
    if self._flags is not None:
      outStr = outStr + "(flags=%s)" % (self._flags)
    outStr = outStr + "[%s=%s]" % (self._name, self._value)
    return outStr

  #
  # @function __cmp__
  # @discussion
  #   Override of the ordered comparison operator for this
  #   class.  Comparison is done by entity name if the
  #   objects' typeDescription() matches, otherwise the
  #   ordering is based on the typeDescription() strings.
  #
  def __cmp__(self,otherObj):
    if isinstance(otherObj, self.__class__):
      if self._name == otherObj.name():
        return 0
      if self._name < otherObj.name():
        return -1
      return +1

    # Not the same class, sort by type description:
    if self.typeDescription() < otherObj.typeDescription():
      return -1
    return +1

  #
  # @function typeDescription
  # @discussion
  #   Returns a string that identifies the kind of named-entity this
  #   object represents.  Each subclass must override.
  #
  def typeDescription(self):
    return "entity"

  #
  # @function isIdentical
  # @discussion
  #   If the receiver and otherObj are of the came class, have the
  #   same name, value, and flags, then True is returned.
  #
  def isIdentical(self, otherObj):
    if isinstance(otherObj, self.__class__):
      if not self._name == otherObj.name():
        return False
      if not self._value == otherObj.value():
        return False
      if not self._flags == otherObj.flags():
        return False
      return True
    return False

  #
  # @function name
  # @discussion
  #   Returns the receiver's entity name.
  #
  #   The entity name is an immutable property and is set at the
  #   time of instantiation.
  #
  def name(self):
    return self._name

  #
  # @function value
  # @discussion
  #   Returns the receiver's entity value.
  #
  #   The entity value is an immutable property and is set at the
  #   time of instantiation.
  #
  def value(self):
    return self._value

  #
  # @function flags
  # @discussion
  #   Returns the receiver's flag string.
  #
  def flags(self):
    return self._flags

  #
  # @function setFlags
  # @discussion
  #   Sets the receiver's flag string value.
  #
  def setFlags(self, flags=None):
    self._flags = flags

