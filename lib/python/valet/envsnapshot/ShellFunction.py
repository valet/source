#
# valet_envsnapshot
# ShellFunction.py
#
# Class to represent a shell function.
#
# $Id: ShellFunction.py 531 2014-10-31 16:18:05Z frey $
#

from ShellObject import *

class ShellFunction(ShellObject):
  
  def typeDescription(self):
    return "function"
  
