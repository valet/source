#
# valet_envsnapshot
# ShellObjects.py
#
# An array of shell named entities.
#
# $Id: ShellObjects.py 531 2014-10-31 16:18:05Z frey $
#

from ShellVariable import *
from ShellFunction import *
from ShellAlias import *

import os
import pickle

class ShellObjects(object):

  @staticmethod
  def importFromFile(filePath):
    obj = None
    fptr = open(filePath, "rb")
    if fptr is not None:
      try:
        obj = pickle.load(fptr)
        if obj is not None and not isinstance(obj, ShellObjects):
          obj = None
      except Exception as E:
        print str(E)
      fptr.close()
    return obj

  def __init__(self, objects = None):
    self._objects = list()
    if objects is not None:
      for object in objects:
        self._objects.append(object)
  
  def __str__(self):
    outStr = "[{\n"
    for obj in self._objects:
      outStr = outStr + str(obj) + ",\n"
    return outStr + "}]\n"

  def __iter__(self):
    return iter(self._objects)
    
  def objects(self):
    return tuple(self._objects)
  
  def objectMatching(self, shellObject):
    if shellObject in self._objects:
      return self._objects[ self._objects.index(shellObject) ]
    return None

  def addObject(self, shellObject):
    if not shellObject in self._objects:
      self._objects.append(shellObject)
      
  def replaceObject(self, shellObject):
    if shellObject in self._objects:
      idx = self._objects.index(shellObject)
      self._objects[idx] = shellObject
    else:
      self._objects.append(shellObject)

  def removeObject(self, shellObject):
    if shellObject in self._objects:
      self._objects.remove(shellObject)

  #
  # @method objectsNotIn
  # @discussion
  #   Returns a tuple composed of all objects present in this object
  #   list that are NOT present in the other object list.
  #
  def objectsNotIn(self, otherObjects):
    notFound = list()
    
    for object in self.objects():
      if object not in otherObjects:
        notFound.append(object)
    return tuple(notFound)
  
  #
  # @method objectDiff
  # @discussion
  #   Returns a tuple composed of two-value tuples indicating where
  #   differences exist between this object list and another.
  #
  #   A tuple like (None, <object>) represents an object present in
  #   the other object list, but not in this object list.
  #
  #   A tuple like (<object>, None) represents an object present in
  #   this object list, but not in the other object list.
  #
  #   A tuple like (<object>, <object>) represents an object that
  #   exists in both object lists, but with differing value.  The
  #   difference may be in the value associated with the name, or
  #   with the export flag on the object.
  #
  def objectDiff(self, otherObjects):
    diffs = list()
    
    for object in self.objects():
      theirObject = otherObjects.objectMatching(object)
      if theirObject is not None:
        if not theirObject.isIdentical(object):
          diffs.append((object, theirObject))
      else:
        diffs.append((object, None))
    for object in otherObjects.objects():
      if object not in self.objects():
        diffs.append((None, object))
        
    return tuple(diffs)
  
  #
  # @method exportToFile
  # @discussion
  #   Pickles this object to a file at the given path.  Returns
  #   boolean True if successful, False otherwise.
  #
  def exportToFile(self, filePath):
    rc = False
    fptr = open(filePath, "wb")
    if fptr is not None:
      try:
        rc = True
        pickle.dump(self, fptr, pickle.HIGHEST_PROTOCOL)
      except Exception as E:
        pass
      fptr.close()
    return rc
        