# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

from package import *
from standardPaths import *


class packageVersion(basePackage):
  """Version of a package

A version of a package augments the parent package itself; attributes like the prefix, description, flags, and contexts can be set and either replace to alter the value provided by the parent package.
  """

  def __init__(self, pkgVersId, prefix = None, description = None, referenceURL = None, flags = None, incompatibilities = None, dependencies = None, actions = None, parentPackage = None, ctxts = []):
    """Constructor

The packageVersion constructor shares the arguments available to the basePackage constructor, with the addition of the parentPackage argument.

If the pkgVersId is a string, it is turned into a valid versioned package id by prefixing "dummy/" if no slash (/) character is present in the string.  When the instance is added to a package object the packageId will be overwritten anyway.

Raises a ValueError exception if pkgVersId is not an id or string, or doesn't parse properly.
    """
    if isinstance(pkgVersId, id):
      pass
    elif isinstance(pkgVersId, basestring):
      if not '/' in pkgVersId:
        pkgVersId = 'dummy/' + pkgVersId
      pkgVersId = id(pkgVersId)
    else:
      raise ValueError('packageVersion.__init__ expects an id or string.')
    self._parentPackage = None
    super(packageVersion, self).__init__(pkgVersId, prefix, description, referenceURL, flags, incompatibilities, dependencies, actions, ctxts)
    if parentPackage is not None:
      parentPackage.addVersion(self)
    
  
  def __str__(self):
    outStr = super(packageVersion, self).__str__()
    outStr = re.sub(r'^basePackage', r'packageVersion', outStr, 1)
    return outStr
    
    
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(packageVersion, self).__repr__())
    #
    # Add our specific stuff:
    #
    if self._parentPackage:
      s += ', parentPackage=<<pkg "' + str(self._parentPackage.id()) + '">>'
    return s + ')'
  
  
  def isPackageVersion(self):
    """Return True to indicate this is a version of a package."""
    return True
    
    
  def hasFlag(self, flag):
    """Override the inherited method so that we also check the parent."""
    if super(packageVersion,self).hasFlag(flag):
      return True
    if self._parentPackage:
      return self._parentPackage.hasFlag(flag)
    return False
    
    
  def setId(self, pkgVersId):
    """We override the parent method to NOT allow the id to be set if parentPackage has been set."""
    if self._parentPackage is None:
      super(packageVersion, self).setId(pkgVersId)


  def parentPackage(self):
    """Return this instance's parent package (or None if it is not set)."""
    return self._parentPackage
    
    
  def setParentPackage(self, parentPackage):
    """Set this instance's parent package.  Note that this method should NOT be called directly; rather, consumer code should call the package.addVersion() method to embed a packageVersion instance in the package (and addVersion() will call this method to make the connection back to the parent)."""
    if isinstance(parentPackage, basePackage) and parentPackage.isPackage():
      self._parentPackage = parentPackage
    else:
      raise ValueError('packageVersion.setParentPackage expects a basePackage subclass that returns True for the isPackage method.')


  def prefix(self):
    """Return this instance's directory prefix.

If no explicit prefix has been provided, then a relative path is inferred from the version id associated with this instance.  If any features are defined on the id, they are lexigraphically ordered and joined with a plus (+) sign and appended to the version string with a dash (-) delimiter.

An explicit prefix can be either an absolute or relative path.  The latter are relative to the prefix of this instance's parent package.

If the parent package has no prefix, then only if an explicit, absolute prefix was provided for this instance will it be returned.

Note that if hardware architecture tags are enabled those sub-paths are tested FIRST -- the first that exists becomes the prefix path for this version of the package.  If none exist (or they're not enabled) then the auto-generated path or this version's prefix relative to that of its parent is returned (and existence of the directory is _not_ established).
    """
    if self._prefix is not None:
      if os.path.isabs(self._prefix):
        return self._prefix
      else:
        auto = False
    else:
      auto = True
    
    if self._parentPackage is not None:
      prefix = self._parentPackage.prefix()
      if prefix is not None:
        # Check any hardware arch tags first:
        if config.shouldUseHardwareArchTags and config.hwArchTags:
          for archPath in reversed(config.hwArchTags):
            if auto:
              fullArchPath = self.id().pathRelativeToPrefix(os.path.join(prefix, archPath))
            else:
              fullArchPath = os.path.join(prefix, archPath, self._prefix)
            if os.path.isdir(fullArchPath):
              return fullArchPath
        # Nothing?  Then either auto-generate the path or attach the explicit relative
        # bit onto our parent's prefix:
        if auto:
          return self.id().pathRelativeToPrefix(prefix)
        return os.path.join(prefix, self._prefix)
        
    return None
    
  
  def description(self):
    """Return this instance's description string.  If no explicit value has been provided, the parent package's description string is returned."""
    if self._description is not None:
      return self._description
    if self._parentPackage is not None:
      return self._parentPackage.description()
    return None
  
  def referenceURL(self):
    """Return this instance's reference URL.  If no explicit value has been provided, the parent package's reference URL is returned."""
    if self._referenceURL is not None:
      return self._referenceURL
    if self._parentPackage is not None:
      return self._parentPackage.referenceURL()
    return None
  
  
  def coallescedFlags(self):
    """Attempt to merge all flags from the parent package and this instance and return that set."""
    if self._parentPackage is not None:
      allItems = set()
      allItems.update(self._parentPackage._flags);
      allItems.update(self._flags);
      return allItems
    return self._flags
    
    
  def coallescedIncompatibilities(self):
    """Attempt to merge all incompatibilities from the parent package and this instance and return that list."""
    if self._parentPackage is not None:
      allItems = []
      allItems.extend(self._parentPackage._incompatibilities)
      allItems.extend(self._incompatibilities)
      return allItems
    return self._incompatibilities
  
  
  def coallescedDependencies(self):
    """Attempt to merge all dependencies from the parent package and this instance and return that list."""
    if self._parentPackage is not None:
      allItems = []
      allItems.extend(self._parentPackage._dependencies)
      allItems.extend(self._dependencies)
      return allItems
    return self._dependencies
  
  
  def coallescedActions(self):
    """Attempt to merge all actions from the parent package and this instance and return that list."""
    if self._parentPackage is not None:
      allItems = []
      allItems.extend(self._parentPackage._actions)
      allItems.extend(self._actions)
      return allItems
    return self._actions
  
  
  def coallescedAttributes(self):
    """Attempt to merge all attributes from the parent package and this instance and return that dictionary."""
    if self._parentPackage is not None:
      allAttrs = {}
      allAttrs.update(self._parentPackage._attributes)
      allAttrs.update(self._attributes)
      return allAttrs
    return self._attributes
  
  
  def resolvedVersion(self, exactMatch = False, fullDepth = True):
    """Forward-declared for the sake of the packageVersionAlias class; a non-alias instance just returns itself."""
    return self


  def infoString(self):
    """Return a string describing this instance for the sake of vpkg_info."""
    s  = '[' + str(self.id()) + '] {' + os.linesep
    
    s += indentString(super(packageVersion, self).infoString(), '  ')
    
    if self._referenceURL is not None:
      s += '  ' + self._referenceURL + os.linesep
    
    if self._description is not None:
      s += '  ' + self._description + os.linesep

    prefix = self.prefix()
    if prefix is not None:
      s += '  prefix: ' + prefix + os.linesep
      
      #
      # See if there are any standard paths:
      #
      subSectionStarted = False
      for pathKind in standardPaths.pathKind:
        if isinstance(pathKind, basestring):
          foundPaths = standardPaths.extantPathsWithPrefix(pathKind, prefix)
          if foundPaths:
            if not subSectionStarted:
              subSectionStarted = True
              s += '  standard paths: {' + os.linesep
            s += '    ' + pathKind + ': ' + ', '.join(foundPaths) + os.linesep
      if subSectionStarted:
        s += '  }' + os.linesep
    
    s += '}' + os.linesep
    return s



if __name__ == '__main__':
  
  p = package(pkgId = 'openmpi', description = 'Open MPI: Message Passing Interface', prefix = '/opt/shared/openmpi')
  
  v = packageVersion(pkgVersId = '1.8.2')
  p.setFlag('no-crying')
  p.addVersion(v)
  
  print v
  print
  
  v = packageVersion(pkgVersId = 'openmpi/1.10.2-gcc-4.9.3')
  p.setFlag('no-crying')
  p.addVersion(v)
  
  print v
  print
  
  print p
  print
  print p.__repr__()
  print
  print p.infoString()
  print
  
  print p.filterVersions(byId='openmpi/=>=1.4')
  
