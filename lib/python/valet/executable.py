# -*- coding: utf-8 -*-
#
# VALET: VALET Automates Linux Environment Tasks
#
# Copyright © 2014-2016
# Dr. Jeffrey Frey
# Network & Systems Services, University of Delaware
#
# $Id$
#

import os
from config import *
from shellType import *
from action import *
from enum import *


#
# enumerate the possible methods for execution:
#
executableMethod = enum(['source', 'exec'])


#
# enumerate the ways to determine success versus failure of an executable:
#
rcOrder = enum(['succeedThenFail', 'failThenSucceed'])


class executable(action):
  """Wrapper for an external script/program that should be executed when adding a package to the environment

An instance of this class defines one or more filesystem paths that are scripts or generic executables.  These paths are keyed by shell kind (see shellType.shellTypeEnum).

There are two methods of execution:  fork and exec, or source into the current shell.  The latter only pertains to shell scripts.

The return code after source/exec of the script/executable can be analyzed for success.  An instance of this class can have a single integer value that indicates success and a single integer value that indicates failure.  The rcOrder dictates in which order the return code is checked:  against the success code then the failure code, or vice versa.
  """

  def __init__(self, method = executableMethod.source, rcOrder = rcOrder.succeedThenFail, successOnRc = 0, failureOnRc = None, pathsByShell = {}, ctxts = []):
    """Constructor

None of the arguments are mandatory.  By default, the rcOrder and success/failure integer fields are setup to succeed on return code zero and fail otherwise, which is the typical scheme used by Unix programs.

The pathsByShell dict should contain strings (filesystem paths) keyed by strings/integers from the shellType.shellTypeEnum enumeration.
    """
    super(executable, self).__init__(ctxts)
    
    self._pathsByShell = {}
    
    self.setMethod(method)
    self.setRcOrder(rcOrder)
    self.setSuccessOnRc(successOnRc)
    self.setFailureOnRc(failureOnRc)
    if pathsByShell is not None:
      for shell, path in pathsByShell.iteritems():
        if isinstance(path, basestring):
          self.setPathForShell(shell, path)
  
  
  def clone(self):
    return executable(self._method, self._rcOrder, self._successOnRc, self._failureOnRc, self._pathsByShell, self.contexts)


  def __str__(self):
    if self._method is not None:
      s = 'executable(' + executableMethod[self._method]
    else:
      return ''

    if self._rcOrder is not None:
      s += ', ' + rcOrder[self._rcOrder]

    if self._successOnRc is not None:
      s += ', success=' + str(self._successOnRc)
    if self._failureOnRc is not None:
      s += ', fail=' + str(self._failureOnRc)

    s += ') {' + os.linesep
    for shell, path in self._pathsByShell.iteritems():
      s += '  ' + shellType.stringWithShellType(shell) + ' : ' + path + os.linesep

    s += '}'
    contextStr = super(executable, self).__str__()
    if contextStr:
      s = s + ' ' + contextStr
    
    return s
  
  
  def __repr__(self):
    s = re.sub(r'^[^(]+\((.*)\)$', self.__class__.__name__ + r'(\1', super(executable, self).__repr__())
    s += ', method=' + str(self._method) + ', rcOrder=' + str(self._rcOrder)
    if self._successOnRc is not None:
      s += ', successOnRc=' + str(self._successOnRc)
    if self._failureOnRc is not None:
      s += ', failureOnRc=' + str(self._failureOnRc)
    if self._pathsByShell is not None and len(self._pathsByShell):
      s += ', pathsByShell=' + self._pathsByShell.__repr__()
    return s + ')'


  def method(self):
    """Return this instance's execution method."""
    return self._method
    
    
  def setMethod(self, method):
    """Set this instance's execution method."""
    if method is None:
      self._method = None
    else:
      if isinstance(method, basestring):
        method = executableMethod[method]
      if method in executableMethod:
        self._method = method


  def rcOrder(self):
    """Return this instance's order of success-versus-failure evaluation."""
    return self._rcOrder
    
    
  def setRcOrder(self, order):
    """Set this instance's order of success-versus-failure evaluation."""
    if order is None:
      self._rcOrder = None
    else:
      if isinstance(order, basestring):
        order = rcOrder[order]
      if order in rcOrder:
        self._rcOrder = order


  def successOnRc(self):
    """Return this instance's integer code that indicates success."""
    return self._successOnRc
    
    
  def setSuccessOnRc(self, rc):
    """Set this instance's integer code that indicates success to the integer value of rc."""
    self._successOnRc = None if rc is None else int(rc)


  def failureOnRc(self):
    """Return this instance's integer code that indicates failure."""
    return self._failureOnRc
    
    
  def setFailureOnRc(self, rc):
    """Set this instance's integer code that indicates failure to the integer value of rc."""
    self._failureOnRc = None if rc is None else int(rc)


  def pathForShell(self, shell):
    """If shell is a valid shellType and has a path defined in this instance's pathsByShell dictionary, that path is returned.

If shell is not a valid shellType, a ValueError exception is raised.

If shell is a valid shellType but no path exists, None is returned.
    """
    if isinstance(shell, basestring):
      resolvedShell = shellType.shellTypeWithString(shell)
      if not shellType.isValidShellType(resolvedShell):
        raise ValueError('The given shell is not understood by VALET: ' + shell)
      shell = resolvedShell
    if shell in self._pathsByShell:
      return self._pathsByShell[shell]
    return None
    
    
  def setPathForShell(self, shell, executablePath):
    """Add an association between the given shell type and the executablePath.

If shell is not valid, a ValueError exception is raised.

If executablePath is not an absolute path it is implicitly relative to VALET's config.libExecDir parameter.  If the executablePath does not exist, a ValueError exception is raised.

An exception to the behavior above w.r.t. executablePath is if the string starts with "${VALET_PATH_PREFIX}/" -- in which case, the path is assumed to be relative to the prefix of the versioned package containing this executable instance.  In that case, the string is used as-is and will be evaluated only when the package is added to the environment.
    """
    if isinstance(shell, basestring):
      resolvedShell = shellType.shellTypeWithString(shell)
      if not shellType.isValidShellType(resolvedShell):
        raise ValueError('The given shell is not understood by VALET: ' + shell)
      shell = resolvedShell
    if shellType.isValidShellType(shell):
      if executablePath.startswith('${VALET_PATH_PREFIX}/'):
        self._pathsByShell[shell] = executablePath
      else:
        if not os.path.isabs(executablePath):
          executablePath = os.path.join(config.libExecDir, executablePath)
        if os.path.exists(executablePath):
          self._pathsByShell[shell] = executablePath
        else:
          raise ValueError('The given executable path does not exist: ' + executablePath)
    else:
      raise ValueError('The value ' + str(shell) + ' is not one of the enumerated shell types.')    
      

  def actionStringWithShellHelper(self, shellHelper, currentContext = contexts.all):
    """Return a configuration fragment string that affects this instance's invocation of an external script/executable.  The provided shellHelper is used for as much shell-specific work as possible, but this action is complex enough that some shell-specific handling is necessary."""
    outCmds = ''
    
    # Promote an atomic currentContext to being a set:
    if isinstance(currentContext, basestring):
      currentContext = set(currentContext)
    
    if currentContext in self.contexts:
      for aShell in shellHelper.matchShellTypes():
        exePath = self.pathForShell(aShell)
        if exePath:
          break
      if exePath is not None:
        #
        # There's no way around it -- this has to be done per shell:
        #
        if shellType.shellTypeEnum.sh in shellHelper.matchShellTypes():
          if exePath.startswith('${VALET_PATH_PREFIX}/') or os.path.exists(exePath):
            if self.method() == executableMethod.source:
              outCmds += 'source '
            outCmds += exePath + ' < /dev/null >& /dev/null;'
            outCmds += 'VALET_RC=$?;'
            if self.rcOrder() == rcOrder.succeedThenFail:
              rc = self.successOnRc()
              if rc is not None:
                outCmds += 'if [ $VALET_RC -ne ' + str(rc) + ' ]; then VALET_EARLY_EXIT=1; fi;'
              rc = self.failureOnRc()
              if rc is not None:
                outCmds += 'if [ $VALET_RC -eq ' + str(rc) + ' ]; then VALET_EARLY_EXIT=1; fi;'
            elif self.rcOrder() ==rcOrder.failThenSucceed:
              rc = self.failureOnRc()
              if rc is not None:
                outCmds += 'if [ $VALET_RC -eq ' + str(rc) + ' ]; then VALET_EARLY_EXIT=1; fi;'
              rc = self.successOnRc()
              if rc is not None:
                outCmds += 'if [ $VALET_RC -ne ' + str(rc) + ' ]; then VALET_EARLY_EXIT=1; fi;'
            outCmds += 'unset VALET_RC;'
            outCmds += 'if [ $VALET_EARLY_EXIT -ne 0 ]; then break; fi;'
        else:
          raise NotImplementedError('Given shell not implemented in executable.py!!!')
    return outCmds
    


if __name__ == '__main__':
  x = executable(executableMethod['exec'], rcOrder.failThenSucceed, ctxts=['development', 'standard'])
  x.setPathForShell('bash', '/bin/ls')
  x.setPathForShell('sh', '/bin/cp')
  print x
  print
  print x.__repr__()
  print
  
